const std = @import("std");
const sdl = @import("sdl");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const is_release = optimize != .Debug;

    const use_llvm = b.option(bool, "use_llvm", "Whether to use LLVM (default: never in debug)") orelse is_release;

    const use_libc = b.option(bool, "prefer_libc", "Whether to use libc if not required (default: false)") orelse false;

    const zenit_module = b.createModule(.{
        .root_source_file = .{ .path = "src/zenit.zig" },
    });

    try b.modules.put(b.dupe("zenit"), zenit_module);

    const exe = b.addExecutable(.{
        .name = "example",
        .root_source_file = .{ .path = "src/example.zig" },
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
    });
    exe.linkLibC();
    exe.use_llvm, exe.use_lld = if (use_llvm) .{ true, true } else .{ false, false };
    b.installArtifact(exe);

    const sdl_sdk = sdl.init(b, null);
    sdl_sdk.link(exe, .dynamic);
    exe.root_module.addImport("sdl", sdl_sdk.getWrapperModule());

    const exe_simple = b.addExecutable(.{
        .name = "print_meta",
        .root_source_file = .{ .path = "src/example_simple.zig" },
        .target = target,
        .optimize = optimize,
        .single_threaded = true,
    });
    exe_simple.use_llvm, exe_simple.use_lld = if (use_llvm) .{ true, true } else .{ false, false };
    b.installArtifact(exe_simple);

    if (use_libc) exe_simple.linkLibC();

    const lib_unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/zenit.zig" },
        .target = target,
        .optimize = optimize,
    });

    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);
}
