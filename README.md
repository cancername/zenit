# Zenit multimedia toolkit

⚠ Zenit is not yet ready for any serious use. Everything is under development. Nothing is tested. ⚠

For development status, visit [the roadmap](./Roadmap.org). "Zenit should do this" goals right now
are demuxing NUT [0/1] and reaching parity with [zigimg](https://github.com/zigimg/zigimg) [3/9]
[0/6].
## Screenshots
### Y4M demuxing
![akiyo.cif playing](./screenshot.png)

## Examples
### Use bitstream decoders
This example is mostly here to track API ease of use.
```zig
const std = @import("std");
const zenit = @import("zenit");

pub fn main() !void {
    // Open a file as usual.
    const file = std.io.getStdIn();
    defer file.close();

    // Create a `Context`.
    // The context is mainly used for three things:
    // 1. to present a convenient interface to you, the library user
    // 2. to handle memory allocation
    // 3. to handle diagnostics reporting.
    // Note that using the same context from multiple threads is not thread-safe.
    var ctx = zenit.Context.initDefault();
    defer ctx.deinit();

    // Components can issue diagnostics. This prints them to stderr to inform the user.
    defer for (ctx.diagnostics.items) |diagnostic| std.debug.print("{}\n", .{diagnostic});

    // Now, register decoders to the context to allow them to be used. Decoder source files contain
    // a `decoder` function that returns a decoder of the correct type. Some decoders are built-in
    // and can be accessed in the `decoders` namespace, but you can also write your own decoder or
    // wrap an existing one. See `src/decoders/template.zig` for a starter.
    try ctx.registerBuiltin();

    // Create a stream to read the data from. It is naturally buffered, do not add buffering as is
    // common in std.io.Reader. The ability to seek is optional.
    var stream = zenit.InputStream.initFd(file.handle);

    // Find the decoder that best suits this stream.
    const sink = try ctx.probe(stream) orelse return error.UnsupportedFile;

    // We didn't register any demuxers, it must be a bitstream decoder.
    const decoder = sink.bitstream_decoder;

    // The decoder's init function sets up any state the decoder requires, now in `decoder_data`,
    // and may also read some information about the stream, now in `stream_metadata`.
    const decoder_data, const stream_metadata = try decoder.init(&ctx, &stream);
    defer decoder.deinit(&ctx, decoder_data) catch {};
    // We aren't doing anything with the stream metadata.
    defer stream_metadata.deinit(&ctx);

    var frame_count: usize = 0;

    // The decoder's decode function returns an update on the state of the frame stream.
    while (decoder.decode(&ctx, decoder_data, &stream)) |update| switch (update) {
        // When the end of the stream is reached, there is nothing more to do.
        .end => break,
        // A frame is output.
        .frame => |const_frame| {
            // API contrivance, should probably be fixed
            var frame = const_frame;
            defer frame.deinit();
            // `frame` now contains the frame data, which may be an image or audio samples but, in
            // this case, is an image. Here, we just print out the metadata.
            std.debug.print("frame {d}: {}\n", .{frame_count, frame.meta});
            frame_count += 1;
        },
    // The decoder doesn't yet have a frame for us. Do nothing.
        .none => {},
    } else |err| return err;
}
```
