const std = @import("std");
const zenit = @import("zenit.zig");

const CR32 = zenit.image.Interpretation.ColorspaceR32;

pub fn Conversions(comptime Float: type) type {
    return struct {
        const CF = zenit.image.Interpretation.Colorspace(Float);

        pub const Sample = [3]Float;
        pub const Matrix = [3 * 3]Float;
        pub const Primaries = [3 * 2]Float;
        pub const WhitePoint = [2]Float;

        pub fn multiplyMatrix(matrix: Matrix, components: Sample) Sample {
            return .{
                matrix[0] * components[0] + matrix[1] * components[1] + matrix[2] * components[2],
                matrix[3] * components[0] + matrix[4] * components[1] + matrix[5] * components[2],
                matrix[6] * components[0] + matrix[7] * components[1] + matrix[8] * components[2],
            };
        }

        pub fn multiplyMatMat(a: Matrix, b: Matrix) Matrix {
            const n = 3;

            var c = std.mem.zeroes(Matrix);

            for (0..n) |i| {
                for (0..n) |j| {
                    for (0..n) |k| {
                        c[i * n + j] += a[i * n + k] * b[k * n + j];
                    }
                }
            }

            return c;
        }

        pub fn invertMatrix(m: Matrix) Matrix {
            // <https://stackoverflow.com/a/18504573>

            const det = m[0 * 3 + 0] * (m[1 * 3 + 1] * m[2 * 3 + 2] - m[2 * 3 + 1] * m[1 * 3 + 2]) -
                m[0 * 3 + 1] * (m[1 * 3 + 0] * m[2 * 3 + 2] - m[1 * 3 + 2] * m[2 * 3 + 0]) +
                m[0 * 3 + 2] * (m[1 * 3 + 0] * m[2 * 3 + 1] - m[1 * 3 + 1] * m[2 * 3 + 0]);

            return .{
                (m[1 * 3 + 1] * m[2 * 3 + 2] - m[2 * 3 + 1] * m[1 * 3 + 2]) / det,
                (m[0 * 3 + 2] * m[2 * 3 + 1] - m[0 * 3 + 1] * m[2 * 3 + 2]) / det,
                (m[0 * 3 + 1] * m[1 * 3 + 2] - m[0 * 3 + 2] * m[1 * 3 + 1]) / det,
                (m[1 * 3 + 2] * m[2 * 3 + 0] - m[1 * 3 + 0] * m[2 * 3 + 2]) / det,
                (m[0 * 3 + 0] * m[2 * 3 + 2] - m[0 * 3 + 2] * m[2 * 3 + 0]) / det,
                (m[1 * 3 + 0] * m[0 * 3 + 2] - m[0 * 3 + 0] * m[1 * 3 + 2]) / det,
                (m[1 * 3 + 0] * m[2 * 3 + 1] - m[2 * 3 + 0] * m[1 * 3 + 1]) / det,
                (m[2 * 3 + 0] * m[0 * 3 + 1] - m[0 * 3 + 0] * m[2 * 3 + 1]) / det,
                (m[0 * 3 + 0] * m[1 * 3 + 1] - m[1 * 3 + 0] * m[0 * 3 + 1]) / det,
            };
        }

        /// xyY to XYZ
        pub fn xyytoXyz(xyy: Sample) Sample {
            // <http://www.brucelindbloom.com/index.html?Eqn_xyY_to_XYZ.html>

            if (xyy[1] == 0) return .{ 0, 0, 0 };
            return .{
                xyy[0] * xyy[2] / xyy[0],
                xyy[2],
                (1 - xyy[0] - xyy[1]) * xyy[2] / xyy[1],
            };
        }

        pub fn computeRgbToXyzMatrixFromPrimariesAndWhitePoint(primaries: Primaries, white_point: WhitePoint) Matrix {
            // <http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html> (<https://archive.is/9KLKF>)

            const wp_as_xyz = xyytoXyz(.{ white_point[0], white_point[1], 1 });

            const xr = primaries[0] / primaries[1];
            const yr = 1;
            const zr = (1 - primaries[0] - primaries[1]) / primaries[0];

            const xg = primaries[1 * 2 + 0] / primaries[1 * 2 + 1];
            const yg = 1;
            const zg = (1 - primaries[1 * 2 + 0] - primaries[1 * 2 + 1]) / primaries[1 * 2 + 0];

            const xb = primaries[2 * 2 + 0] / primaries[2 * 2 + 1];
            const yb = 1;
            const zb = (1 - primaries[2 * 2 + 0] - primaries[2 * 2 + 1]) / primaries[2 * 2 + 0];

            const sr, const sg, const sb = multiplyMatrix(invertMatrix(.{ xr, xg, xb, yr, yg, yb, zr, zg, zb }), wp_as_xyz);

            return .{
                sr * xr, sg * xg, sb * xb,
                sr * yr, sg * yg, sb * yb,
                sr * zr, sg * zg, sb * zb,
            };
        }

        // Sample values range from 0 to 1.
        pub fn convertSampleColorspace(src: Sample, dst_colorspace: CR32, src_colorspace: CR32) Sample {
            var sample = src;

            const dst_cs_f = dst_colorspace.toFloat(Float);
            const src_cs_f = src_colorspace.toFloat(Float);

            // conversion between identical color spaces is tested, this optimization would make
            // that test pointless
            const testing = @import("builtin").is_test;
            const eql_mat = !testing and src_colorspace.eqlTransformMatrix(dst_colorspace);
            const eql_tf = !testing and src_colorspace.transfer_fn.eql(dst_colorspace.transfer_fn);
            const eql_primaries = !testing and src_colorspace.eqlPrimaries(dst_colorspace);
            const eql_wp = !testing and src_colorspace.eqlWhitePoint(dst_colorspace);

            if (!eql_mat) {
                // src->identity
                sample = multiplyMatrix(src_cs_f.transform_matrix, sample);
            }

            if (!eql_tf) {
                // src->linear
                sample = switch (src_cs_f.transfer_fn) {
                    .fixed => |f| .{
                        f.applyEotf(Float, sample[0]),
                        f.applyEotf(Float, sample[1]),
                        f.applyEotf(Float, sample[2]),
                    },
                    .gamma => |g| .{
                        std.math.pow(Float, sample[0], g),
                        std.math.pow(Float, sample[1], g),
                        std.math.pow(Float, sample[2], g),
                    },
                };
            }

            if (!eql_primaries) {
                sample = multiplyMatrix(computeRgbToXyzMatrixFromPrimariesAndWhitePoint(src_cs_f.primaries, src_cs_f.white_point), sample);
            }

            _ = eql_wp; // autofix
            if (!src_colorspace.eqlWhitePoint(dst_colorspace)) {
                std.log.err("TODO: implement chromatic adaptation, {d} -> {d} requested", .{ src_colorspace.white_point, dst_colorspace.white_point });
            }

            if (!eql_primaries) {
                sample = multiplyMatrix(invertMatrix(computeRgbToXyzMatrixFromPrimariesAndWhitePoint(dst_cs_f.primaries, dst_cs_f.white_point)), sample);
            }

            if (!eql_tf) {
                // linear->src
                sample = switch (src_cs_f.transfer_fn) {
                    .fixed => |f| .{
                        f.applyOetf(Float, sample[0]),
                        f.applyOetf(Float, sample[1]),
                        f.applyOetf(Float, sample[2]),
                    },
                    .gamma => |g| .{
                        std.math.pow(Float, sample[0], 1.0 / g),
                        std.math.pow(Float, sample[1], 1.0 / g),
                        std.math.pow(Float, sample[2], 1.0 / g),
                    },
                };
            }

            if (!eql_mat) {
                // identity->dst
                sample = multiplyMatrix(invertMatrix(dst_cs_f.transform_matrix), sample);
            }

            return sample;
        }

        pub fn convertQuantizedSample(comptime T: type, quantized_sample: [3]T, dst_cs: CR32, src_cs: CR32) [3]T {
            const scaled_sample: Sample = .{
                @floatFromInt(quantized_sample[0]),
                @floatFromInt(quantized_sample[1]),
                @floatFromInt(quantized_sample[2]),
            };
            const unscaled_sample: Sample = .{
                scaled_sample[0] / std.math.maxInt(T),
                scaled_sample[1] / std.math.maxInt(T),
                scaled_sample[2] / std.math.maxInt(T),
            };

            const converted_sample = convertSampleColorspace(unscaled_sample, dst_cs, src_cs);

            const rescaled_sample = .{
                converted_sample[0] * std.math.maxInt(T),
                converted_sample[1] * std.math.maxInt(T),
                converted_sample[2] * std.math.maxInt(T),
            };
            const requantized_sample: [3]T = .{
                @intFromFloat(@round(rescaled_sample[0])),
                @intFromFloat(@round(rescaled_sample[1])),
                @intFromFloat(@round(rescaled_sample[2])),
            };
            return requantized_sample;
        }

        pub fn convertFrame(frame: zenit.Frame, dst_interpretation: zenit.image.Interpretation) void {
            switch (frame.meta.layout.image) {
                .xyz => {
                    const src_cs = frame.meta.format.image.interpretation.three;
                    const dst_cs = dst_interpretation.three;

                    var cur_pix = frame.data;

                    for (0..frame.meta.medium_data.image.rows * frame.meta.medium_data.image.columns) |_| {
                        cur_pix[0..3].* = convertQuantizedSample(u8, cur_pix[0..3].*, dst_cs, src_cs);

                        cur_pix = cur_pix[3..];
                    }
                },
                .xyzp_444 => {
                    const src_cs = frame.meta.format.image.interpretation.three;
                    const dst_cs = dst_interpretation.three;

                    const plane_len = frame.meta.medium_data.image.rows * frame.meta.medium_data.image.columns;
                    const x_plane = frame.data[0..plane_len];
                    const y_plane = frame.data[plane_len..][0..plane_len];
                    const z_plane = frame.data[plane_len * 2 ..][0..plane_len];

                    for (x_plane, y_plane, z_plane) |*x_comp, *y_comp, *z_comp| {
                        x_comp.*, y_comp.*, z_comp.* = convertQuantizedSample(u8, .{ x_comp.*, y_comp.*, z_comp.* }, dst_cs, src_cs);
                    }
                },
                else => @panic("TODO: missing layout in convertFrame"),
            }
        }
    };
}

test "convertSampleColorspace f32 same colorspace is exact" {
    const c = Conversions(f32);
    const src_sample: c.Sample = .{ 0.5, 0.2, 0.73 };

    const src_colorspace = .{
        .transform_matrix = zenit.image.Interpretation.transform_matrices.bt_709,
        .primaries = zenit.image.Interpretation.primaries.bt_709,
        .white_point = zenit.image.Interpretation.white_points.d_65,
        .transfer_fn = .{ .fixed = .bt_601 },
    };

    const dst_colorspace = src_colorspace;

    const converted_sample = c.convertSampleColorspace(src_sample, dst_colorspace, src_colorspace);

    for (src_sample, converted_sample) |ss, cs| {
        try std.testing.expectApproxEqAbs(ss, cs, 1.0 / @as(comptime_float, std.math.maxInt(u16)) / 2.0);
    }
}

test "convertSampleColorspace f32 to ycbcr with same primaries" {
    const c = Conversions(f32);
    const src_sample: c.Sample = .{ 0.5, 0.5, 0.5 };

    const src_colorspace = zenit.image.Interpretation.bt_709_rgb.three;

    const dst_colorspace = zenit.image.Interpretation.bt_709_444.three;

    const converted_sample = c.convertSampleColorspace(src_sample, dst_colorspace, src_colorspace);

    const expected_sample: c.Sample = .{ 0.5, 0, 0 };

    for (expected_sample, converted_sample) |ss, cs| {
        try std.testing.expectApproxEqAbs(ss, cs, 1.0 / @as(comptime_float, std.math.maxInt(u16)) / 2.0);
    }
}

test "convertSampleColorspace f32 to ycbcr with different primaries" {
    const c = Conversions(f32);
    const src_sample: c.Sample = .{ 0.5, 0.5, 0.5 };

    const src_colorspace = zenit.image.Interpretation.bt_709_rgb.three;

    const dst_colorspace = .{
        .primaries = zenit.image.Interpretation.primaries.bt_2020,
        .white_point = src_colorspace.white_point,
        .transform_matrix = zenit.image.Interpretation.transform_matrices.bt_709,
        .transfer_fn = src_colorspace.transfer_fn,
    };

    const converted_sample = c.convertSampleColorspace(src_sample, dst_colorspace, src_colorspace);
    const expected_sample: c.Sample = .{ 0.5, 0, 0 };

    for (expected_sample, converted_sample) |ss, cs| {
        try std.testing.expectApproxEqAbs(ss, cs, 1.0 / @as(comptime_float, std.math.maxInt(u16)) / 2.0);
    }
}

test "convertSampleColorspace f32 to ycbcr with different primaries random" {
    const c = Conversions(f32);
    const src_sample: c.Sample = .{ 0.8823529481887817, 0.7019608020782471, 0.8627451062202454 };

    const src_colorspace = zenit.image.Interpretation.bt_709_rgb.three;

    const dst_colorspace = .{
        .primaries = zenit.image.Interpretation.primaries.bt_2020,
        .white_point = src_colorspace.white_point,
        .transform_matrix = zenit.image.Interpretation.transform_matrices.bt_709,
        .transfer_fn = src_colorspace.transfer_fn,
    };

    const converted_sample = c.convertSampleColorspace(src_sample, dst_colorspace, src_colorspace);
    // TODO: update with real values
    const expected_sample: c.Sample = .{ 0.745775580406189, 0.053430646657943726, 0.04313521087169647 };
    for (expected_sample, converted_sample) |ss, cs| {
        try std.testing.expectApproxEqAbs(ss, cs, 1.0 / @as(comptime_float, std.math.maxInt(u16)) / 2.0);
    }
}
