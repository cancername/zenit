const std = @import("std");
const zenit = @import("zenit.zig");

pub const std_options = .{ .fmt_max_depth = 16 };

pub fn main() !void {
    var ctx = zenit.Context.initDefault();
    defer ctx.deinit();
    defer for (ctx.diagnostics.items) |diagnostic| std.debug.print("{}\n", .{diagnostic});

    try ctx.registerBuiltin();

    var stream = zenit.InputStream.initFd((try std.fs.cwd().openFile(std.mem.span(std.os.argv[1]), .{})).handle);
    defer stream.deinit();

    const bd_i = b: {
        const sel = ctx.probe(&stream) orelse return error.NoFormatFound;
        if (sel != .bitstream_decoder) return error.WrongSelection;
        break :b sel.bitstream_decoder;
    };

    const bd = ctx.bitstream_decoders.items[bd_i];

    const decoder_data, const stream_metadata = try bd.init(&ctx, &stream);
    defer bd.deinit(&ctx, decoder_data) catch {};
    defer stream_metadata.deinit(&ctx.alloc);

    while (true) {
        var update = try bd.decode(&ctx, decoder_data, &stream);

        switch (update) {
            .end => break,
            .none => {},
            .frame => |*f| {
                defer f.free(&ctx.alloc);

                std.debug.assert(f.meta.media_type == .image);

                std.debug.print("{} {} {} {}\n", .{
                    f.meta.flags,
                    if (f.meta.flags.is_pts_accurate) f.meta.pts else zenit.Duration{ .nsecs = 0 },
                    f.meta.layout.image,
                    f.meta.interpretation.image.toTagged(f.meta.layout.image),
                });
            },
        }
    }
}
