pub const Score = u8;
/// Might be the format, but probably not.
pub const low = 1;
/// Magic matches. Unlikely to be something else.
pub const high = 100;
/// Checksum matches. Bascially impossible to be something else.
pub const definite = 127;
