const std = @import("std");
const zenit = @import("zenit.zig");
const assert = std.debug.assert;
const simd = @import("simd.zig");

pub fn mae(a_frame: zenit.Frame, b_frame: zenit.Frame) [4]zenit.FastFloat {
    assert(a_frame.meta.format == .image);
    assert(b_frame.meta.format == .image);

    const a_fmt = a_frame.meta.format.image;
    const b_fmt = b_frame.meta.format.image;

    assert(a_fmt.eql(b_fmt));

    const fmt = a_fmt;

    const a_med = a_frame.meta.medium_data.image;
    const b_med = b_frame.meta.medium_data.image;
    assert(a_med.rows == b_med.rows and a_med.columns == b_med.columns);

    const med = a_med;

    var res: [4]zenit.FastFloat = undefined;

    switch (fmt.layout) {
        .xyzp_444,
        .xyzp_422,
        .xyzp_420,
        .xyzp_411,
        => {
            var ah = zenit.PlanarFrameHelper(u8).init(a_frame) catch unreachable;
            var bh = zenit.PlanarFrameHelper(u8).init(b_frame) catch unreachable;

            for (&res, ah.planes[0..ah.nb_planes], bh.planes[0..ah.nb_planes]) |*d, ap, bp| {
                d.* = maePlane(zenit.FastFloat, u8, ap.ptr, bp.ptr, ap.len);
            }
        },

        .xyzp_10_444,
        .xyzp_12_444,
        .xyzp_16_444,

        .xyzp_10_422,
        .xyzp_12_422,
        .xyzp_16_422,

        .xyzp_10_420,
        .xyzp_12_420,
        .xyzp_16_420,

        .xyzp_10_411,
        .xyzp_12_411,
        .xyzp_16_411,
        => {
            var ah = zenit.PlanarFrameHelper(u16).init(a_frame) catch unreachable;
            var bh = zenit.PlanarFrameHelper(u16).init(b_frame) catch unreachable;
            for (&res, ah.planes[0..ah.nb_planes], bh.planes[0..ah.nb_planes]) |*d, ap, bp| {
                d.* = maePlane(zenit.FastFloat, u16, ap.ptr, bp.ptr, ap.len);
            }
        },

        .x1 => {
            const abp = a_frame.data.ptr;
            const bbp = b_frame.data.ptr;
            const bit_len = med.rows * med.columns;

            var accum: usize = 0;

            const aup: [*]align(zenit.Frame.alignment) usize = @ptrCast(abp);
            const bup: [*]align(zenit.Frame.alignment) usize = @ptrCast(bbp);

            const usize_len = @divFloor(bit_len, @bitSizeOf(usize));
            const done_byte_len = usize_len * @bitSizeOf(usize) / 8;

            for (aup[0..usize_len], bup[0..usize_len]) |au, bu| {
                accum += @popCount(au ^ bu);
            }

            const byte_len = @divFloor(bit_len, 8) + @intFromBool(bit_len % 8 != 0);

            {
                var a_tmp: usize = 0;
                var b_tmp: usize = 0;

                const left = byte_len - done_byte_len;
                @memcpy(std.mem.asBytes(&a_tmp)[0..left], abp[done_byte_len..byte_len]);
                @memcpy(std.mem.asBytes(&b_tmp)[0..left], bbp[done_byte_len..byte_len]);

                accum += @popCount(a_tmp ^ b_tmp);
            }

            res[0] = @as(f64, @floatFromInt(accum)) / @as(f64, @floatFromInt(bit_len));
        },
        else => @panic("TODO"),
    }

    return res;
}

pub fn maePlane(
    comptime F: type,
    comptime T: type,
    a: [*]const T,
    b: [*]const T,
    len: usize,
) F {
    const vl = std.simd.suggestVectorLength(T) orelse @compileError("tf");
    const V = @Vector(vl, T);

    var r: u64 = 0;

    const al_off = std.mem.alignPointerOffset(a, @alignOf(V)) orelse unreachable;

    if (al_off != 0) {
        assert(al_off < @alignOf(V));
        for (a[0..al_off], b[0..al_off]) |ae, be| {
            r += @max(ae, be) - @min(ae, be);
        }
    }

    const va: [*]const V = @ptrCast(@alignCast(a + al_off));
    const vb: [*]const V = @ptrCast(@alignCast(b + al_off));
    const len_v = @divFloor(len, vl);

    for (va[0..len_v], vb[0..len_v]) |av, bv| {
        r += simd.sadNonNative(V, av, bv);
    }

    return maePlaneFinish(F, T, a, b, len_v * vl, len, r);
}

fn maePlaneFinish(
    comptime F: type,
    comptime T: type,
    a: [*]const T,
    b: [*]const T,
    done: usize,
    len: usize,
    prev_r: u64,
) F {
    var r = prev_r;

    for (a[done..len], b[done..len]) |ae, be| {
        r += @max(ae, be) - @min(ae, be);
    }

    return @as(F, @floatFromInt(r)) / @as(F, @floatFromInt(len));
}

export fn maef(d: *[4]zenit.FastFloat, a_p: *anyopaque, b_p: *anyopaque) void {
    const a: *zenit.Frame = @ptrCast(@alignCast(a_p));
    const b: *zenit.Frame = @ptrCast(@alignCast(b_p));
    d.* = mae(a.*, b.*);
}
