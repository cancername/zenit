const std = @import("std");
const zenit = @import("zenit.zig");

pub const image = @import("image.zig");
pub const video = @import("video.zig");
pub const audio = @import("audio.zig");

const MediaType = enum {
    audio,
    image,
};

pub const Layout = union {
    audio: audio.Layout,
    image: image.Layout,
};

pub const Interpretation = union {
    audio: audio.Interpretation,
    image: image.Interpretation,
};

pub const FrameStreamMetadata = struct {
    stream: StreamMetadata = .{},
    frame_hint: ?FrameMetadata = null,

    pub fn deinit(fsm: *FrameStreamMetadata, allocs: *zenit.AllocStrat) void {
        fsm.stream.extra_data.deinit(allocs);
        if (fsm.frame_hint) |*fh| fh.deinit(allocs);
    }
};

pub const StreamMetadata = struct {
    is_vfr: bool = false,
    duration: ?zenit.Duration = null,
    extra_data: zenit.KVOptions = .{},

    pub fn deinit(sm: *StreamMetadata, allocs: *zenit.AllocStrat) void {
        sm.extra_data.deinit(allocs);
    }
};

pub const ContainerMetadata = struct {
    pub const ContainerStreamMetadata = struct {
        stream_metadata: StreamMetadata,
        recommended_decoders: []const []const u8,
        kind: MediaType,
    };

    streams: std.ArrayListUnmanaged(ContainerStreamMetadata) = .{},

    pub fn addStream(cm: *ContainerMetadata, allocs: *zenit.AllocStrat, csm: ContainerStreamMetadata) std.mem.Allocator.Error!void {
        return cm.streams.append(allocs.general, csm);
    }

    pub fn deinit(cm: *ContainerMetadata, allocs: *zenit.AllocStrat) void {
        for (cm.streams.items) |*stream| {
            stream.stream_metadata.deinit(allocs);
        }
    }
};

pub const Packet = struct {
    data: []u8,
    stream_idx: u16,
    pts: zenit.Duration,
    dts: zenit.Duration,
};

pub const FrameFlags = packed struct {
    /// Whether the frame was decoded stand-alone.
    is_key_frame: bool = false,

    /// Whether the frame should be scrapped and never presented.
    dont_present: bool = false,

    /// Whether the presentation timestamp is correct.
    is_pts_accurate: bool = false,

    /// Whether the decode timestamp is correct.
    is_dts_accurate: bool = false,

    /// Whether the frame is likely corrupted.
    is_corrupt: bool = false,
};

pub const MediumData = union {
    image: struct {
        rows: usize,
        columns: usize,
        /// undefined if the stream is not vfr
        /// 0 if unknown
        vfr_frame_duration: zenit.Duration = .{ .nsecs = 0 },
        sar: zenit.Rational64 = zenit.Rational64.fromInt(1),
    },
    audio: struct {
        nb_samples: usize,
    },
};

pub const FrameMetadata = struct {
    media_type: MediaType,

    medium_data: MediumData,

    interpretation: Interpretation,
    layout: Layout,

    extra_data: zenit.KVOptions = .{},
    pts: zenit.Duration,
    dts: zenit.Duration,
    flags: FrameFlags = .{},

    pub fn getLen(meta: FrameMetadata) error{Overflow}!usize {
        const mul = std.math.mul;

        // TODO: replace with infos when done
        const factor: usize = switch (meta.media_type) {
            .audio => switch (meta.layout.audio.kind) {
                .u_8 => @as(usize, 1) * meta.layout.audio.nb_channels,
                .s_16 => @as(usize, 2) * meta.layout.audio.nb_channels,
                .s_32, .f_32 => @as(usize, 4) * meta.layout.audio.nb_channels,
            },
            .image => switch (meta.layout.image) {
                // bit-packed
                .x1,
                .x1b,
                .x,
                => 1,
                .x10, .x12, .x16 => 2,
                .xy => 2,
                .xy10, .xy12, .xy16 => 4,
                .xyz => 3,
                .xyz10, .xyz12, .xyz16 => 6,
                .xyzw => 4,
                .xyzw10, .xyzw12, .xyzw16 => 8,
                .xyzp_444 => 3,
                .xyzp_10_444, .xyzp_12_444, .xyzp_16_444 => 6,
                .xyzp_422 => 2,
                .xyzp_10_422, .xyzp_12_422, .xyzp_16_422 => 4,
                .xyzp_420 => 3,
                .xyzp_10_420, .xyzp_12_420, .xyzp_16_420 => 6,
                .xyzp_411 => 3,
                .xyzp_10_411, .xyzp_12_411, .xyzp_16_411 => 6,
                .xyzwp_4444 => 4,
                .xyzwp_4204 => 3,
                .xyz565 => 2,
            },
        };

        const divisor: usize = switch (meta.media_type) {
            .image => switch (meta.layout.image) {
                .x1, .x1b => 8,
                .xyzp_420, .xyzp_10_420, .xyzp_12_420, .xyzp_16_420 => 2,
                .xyzp_411, .xyzp_10_411, .xyzp_12_411, .xyzp_16_411 => 2,
                else => 1,
            },
            else => 1,
        };

        if (meta.media_type == .audio) {
            return std.math.divCeil(usize, try mul(usize, meta.medium_data.audio.nb_samples, factor), divisor) catch unreachable;
        } else {
            const r = meta.medium_data.image.rows;
            const c = meta.medium_data.image.columns;
            const rc = try mul(usize, r, c);

            switch (meta.media_type) {
                .image => if (meta.layout.image == .xyzwp_4204) {
                    return std.math.add(usize, try mul(usize, rc, 3) / 2, rc);
                },
                else => unreachable,
            }

            return std.math.divCeil(usize, try mul(usize, rc, factor), divisor) catch unreachable;
        }
    }

    pub fn deinit(meta: *FrameMetadata, alloc: *zenit.AllocStrat) void {
        meta.extra_data.deinit(alloc);
    }

    pub fn deinitWithAllocator(meta: *FrameMetadata, ally: std.mem.Allocator) void {
        meta.extra_data.deinitWithAllocator(ally);
    }
};

pub const Frame = struct {
    pub const alignment = std.simd.suggestVectorLength(u8) orelse @alignOf(std.c.max_align_t);

    data: []align(alignment) u8,
    meta: FrameMetadata,

    pub inline fn alloc(allocs: *zenit.AllocStrat, meta: FrameMetadata) std.mem.Allocator.Error!Frame {
        return allocWithAllocator(allocs.buf, meta);
    }

    pub fn allocWithAllocator(ally: std.mem.Allocator, meta: FrameMetadata) std.mem.Allocator.Error!Frame {
        const len = meta.getLen() catch return error.OutOfMemory;
        return .{
            .meta = meta,
            .data = try ally.alignedAlloc(u8, alignment, len),
        };
    }

    pub fn allocDiag(z: *zenit.Context, meta: FrameMetadata) std.mem.Allocator.Error!Frame {
        const len = meta.getLen() catch switch (meta.media_type) {
            .image => try z.diag(
                .err,
                .glue,
                error.OutOfMemory,
                "Integer overflow when trying to allocate an image frame of size {d}x{d} (layout {}).",
                .{ meta.medium_data.image.columns, meta.medium_data.image.rows, meta.layout.image },
            ),
            .audio => try z.diag(
                .err,
                .glue,
                error.OutOfMemory,
                "Integer overflow when trying to allocate an audio frame of size {d} (layout {}).",
                .{ meta.medium_data.audio.nb_samples, meta.layout.audio },
            ),
        };

        return .{
            .meta = meta,
            .data = z.alloc.buf.alignedAlloc(u8, alignment, len) catch switch (meta.media_type) {
                .image => try z.diag(
                    .err,
                    .glue,
                    error.OutOfMemory,
                    "Out of memory when trying to allocate an image frame of size {d}x{d} (layout {}).",
                    .{ meta.medium_data.image.columns, meta.medium_data.image.rows, meta.layout.image },
                ),
                .audio => try z.diag(
                    .err,
                    .glue,
                    error.OutOfMemory,
                    "Out of memory when trying to allocate an audio frame of size {d} (layout {}).",
                    .{ meta.medium_data.audio.nb_samples, meta.layout.audio },
                ),
            },
        };
    }

    pub fn free(frame: *Frame, allocs: *zenit.AllocStrat) void {
        allocs.buf.free(frame.data);
        frame.meta.deinit(allocs);
    }

    pub fn freeWithAllocator(frame: *Frame, ally: std.mem.Allocator) void {
        ally.free(frame.data);
        frame.meta.deinitWithAllocator(ally);
    }

    pub fn dupeWithAllocator(frame: *Frame, ally: std.mem.Allocator) std.mem.Allocator.Error!Frame {
        const f = try Frame.allocWithAllocator(ally, frame.meta);
        @memcpy(f.data, frame.data);
        return f;
    }
};
