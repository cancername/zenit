const std = @import("std");
const zenit = @import("zenit.zig");

// Recommended reading:
// - https://hg2dc.com/ (<https://archive.is/gdg14>)
// - https://ninedegreesbelow.com/photography/xyz-rgb.html (<https://archive.is/dwuRY>)
// - https://ninedegreesbelow.com/photography/srgb-luminance.html (<https://archive.is/bLZHu>)
// - https://ninedegreesbelow.com/photography/srgb-history.html (<https://archive.is/mBITR>)
// - http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html (<https://archive.is/9KLKF>)
// - https://encoding.bluefalcon.cc/colors.html (<https://archive.is/UhCkp>)

// ugh I hate colorimetry
/// How samples are to be interpreted.
pub const Interpretation = union {
    pub const Transparency = enum {
        /// The transparency channel should be disregarded entirely.
        ignored,

        /// The transparency channel represents the occlusion of the sample ("premultiplied"), a
        /// foreground sample is composited with a background sample as follows: `foreground + ((1 -
        /// foreground_transparency) * background)`.
        associated,

        /// The transparency channel represents the occlusion of the sample *and its contribution to the
        /// composited sample* ("straight", "unpremultiplied"), a foreground sample is composited with a
        /// background sample as follows: `foreground * foreground_transparency + ((1 -
        /// foreground_transparency) * background).
        unassociated,
    };

    // conversion from the expressed colorspace to CIE XYZ goes something like this:
    //
    // X, Y, Z values to be converted to CIE XYZ: `input`
    // CIE XYZ values converted from `input`: `output`
    // - `transformed` is the result of the matrix multiplication of `transform_matrix` and `input`.
    // - `optical` is the result of the application of `transfer_fn` onto each `transformed`:
    //   - if `transfer_fn` is `gamma`, the result of each `input` taken to the power of one divided by `gamma`.
    //   - if `transfer_fn` is `fixed`, just use the corresponding transfer function
    // - `output` is the result of the application of algorithm in <http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html> (<https://archive.is/9KLKF>) to `optical` using `primaries` and `white_point`.
    pub fn Colorspace(comptime T: type) type {
        return struct {
            pub const TransferFn = union(enum) {
                gamma: T,
                fixed: zenit.TransferFn,

                pub fn eql(a: TransferFn, b: TransferFn) bool {
                    return std.meta.activeTag(a) == std.meta.activeTag(b) and switch (a) {
                        .gamma => |g| g.eql(b.gamma),
                        .fixed => |f| f == b.fixed,
                    };
                }
            };

            // XYZ XYZ XYZ
            transform_matrix: [3 * 3]T,
            // xy xy xy
            primaries: [3 * 2]T,
            // xy
            white_point: [2]T,
            transfer_fn: TransferFn,

            const Self = @This();

            pub fn toFloat(src: Self, comptime Float: type) if (Float == T) @compileError("loop") else Colorspace(Float) {
                var d: Colorspace(Float) = undefined;
                for (&d.transform_matrix, src.transform_matrix) |*di, i| {
                    di.* = i.toFloat(Float);
                }
                for (&d.primaries, src.primaries) |*di, i| {
                    di.* = i.toFloat(Float);
                }
                for (&d.white_point, src.white_point) |*di, i| {
                    di.* = i.toFloat(Float);
                }
                d.transfer_fn = if (src.transfer_fn == .gamma) .{ .gamma = src.transfer_fn.gamma.toFloat(Float) } else .{ .fixed = src.transfer_fn.fixed };
                return d;
            }

            pub fn eqlTransformMatrix(a: Self, b: Self) bool {
                return std.mem.eql(u8, std.mem.asBytes(&a.transform_matrix), std.mem.asBytes(&b.transform_matrix));
            }

            pub fn eqlPrimaries(a: Self, b: Self) bool {
                return std.mem.eql(u8, std.mem.asBytes(&a.primaries), std.mem.asBytes(&b.primaries));
            }

            pub fn eqlWhitePoint(a: Self, b: Self) bool {
                return std.mem.eql(u8, std.mem.asBytes(&a.white_point), std.mem.asBytes(&b.white_point));
            }

            pub fn eql(a: Self, b: Self) bool {
                return eqlTransformMatrix(a, b) and eqlPrimaries(a, b) and eqlWhitePoint(a, b) and a.transfer_fn.eql(b.transfer_fn);
            }
        };
    }

    pub const R32 = zenit.Rational(i32);
    pub const ColorspaceR32 = Colorspace(R32);

    /// AKA "chroma siting", "chroma location"
    ///
    /// `X` samples:
    ///
    /// ```
    /// X _ X
    /// _ _ _
    /// X _ X
    /// ```
    ///
    /// `Y`, `Z` Samples:
    ///
    /// ```
    /// 1 2 X
    /// 4 5 _
    /// 6 7 X
    /// ```
    /// Source: FFmpeg AVChromaLocation, given how FFmpeg is, revise this soon
    pub const Subsampling = enum(u4) {
        unknown = 0,
        top_left = 1,
        top_center = 2,
        center_left = 3,
        center = 4,
        bottom_left = 6,
        bottom_center = 7,
    };

    /// Whether one or zero is white.
    pub const Bit = enum {
        one_is_white,
        one_is_black,
    };

    /// The transfer function for the X component.
    // TODO: Does any colorspace (lightspace?) information make sense here?
    pub const One = ColorspaceR32.TransferFn;

    /// Transfer function for the X component and Y as transparency.
    pub const Two = struct {
        x_transfer: ColorspaceR32.TransferFn,
        transparency: Transparency,

        pub fn eql(a: Two, b: Two) bool {
            return a.x_transfer.eql(b.x_transfer) and a.transparency == b.transparency;
        }
    };

    /// Colorspace for the X, Y, Z components.
    pub const Three = ColorspaceR32;

    /// Colorspace for the X, Y, Z components, and additional data about how to interpret
    /// subsampling.
    pub const ThreeSubsampled = struct {
        xyz_color_space: ColorspaceR32,
        subsampling: Subsampling,

        pub fn eql(a: ThreeSubsampled, b: ThreeSubsampled) bool {
            return a.xyz_color_space.eql(b.xyz_color_space) and a.subsampling == b.subsampling;
        }
    };

    /// Colorspace for the X, Y, Z components, plus W as transparency.
    pub const Four = struct {
        xyz_color_space: ColorspaceR32,
        transparency: Transparency,

        pub fn eql(a: Four, b: Four) bool {
            return a.xyz_color_space.eql(b.xyz_color_space) and a.transparency == b.transparency;
        }
    };

    /// Colorspace for the X, Y, Z components, and additional data about how to interpret
    /// subsampling, plus W as transparency.
    pub const FourSubsampled = struct {
        xyz_color_space: ColorspaceR32,
        subsampling: Subsampling,
        transparency: Transparency,

        pub fn eql(a: FourSubsampled, b: FourSubsampled) bool {
            return a.xyz_color_space.eql(b.xyz_color_space) and a.transparency == b.transparency and a.subsampling == b.subsampling;
        }
    };

    bit: Bit,
    one: One,
    two: Two,
    three: Three,
    three_subsampled: ThreeSubsampled,
    four: Four,
    four_subsampled: FourSubsampled,

    pub fn toTagged(i: Interpretation, l: Layout) zenit.util.Tagged(Interpretation) {
        return switch (l) {
            .x1, .x1b => .{ .bit = i.bit },

            .x,
            .x10,
            .x12,
            .x16,
            => .{ .one = i.one },

            .xy,
            .xy10,
            .xy12,
            .xy16,
            => .{ .two = i.two },

            .xyz,
            .xyz10,
            .xyz12,
            .xyz16,
            .xyz565,
            => .{ .three = i.three },

            .xyzw,
            .xyzw10,
            .xyzw12,
            .xyzw16,
            => .{ .four = i.four },

            .xyzp_444,
            .xyzp_10_444,
            .xyzp_12_444,
            .xyzp_16_444,
            => .{ .three = i.three },

            .xyzp_422,
            .xyzp_10_422,
            .xyzp_12_422,
            .xyzp_16_422,
            .xyzp_420,
            .xyzp_10_420,
            .xyzp_12_420,
            .xyzp_16_420,

            .xyzp_411,
            .xyzp_10_411,
            .xyzp_12_411,
            .xyzp_16_411,
            => .{ .three_subsampled = i.three_subsampled },

            .xyzwp_4444 => .{ .four = i.four },
            .xyzwp_4204 => .{ .four_subsampled = i.four_subsampled },
        };
    }

    pub fn eql(a: Interpretation, b: Interpretation, l: Layout) bool {
        return switch (a.toTagged(l)) {
            .bit => |bit| bit == b.bit,
            .one => |x| x.eql(b.one),
            .two => |x| x.eql(b.two),
            .three => |x| x.eql(b.three),
            .three_subsampled => |x| x.eql(b.three_subsampled),
            .four => |x| x.eql(b.four),
            .four_subsampled => |x| x.eql(b.four_subsampled),
        };
    }

    const rS = zenit.rationalI32FromString;

    pub const primaries = struct {
        // Source: <https://en.wikipedia.org/w/index.php?title=Rec._709&oldid=1193725571>
        pub const bt_709 = .{
            // x         y
            rS("0.64"), rS("0.33"),
            rS("0.3"),  rS("0.6"),
            rS("0.15"), rS("0.06"),
        };

        // Source: <https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.601-7-201103-I!!PDF-E.pdf> (<https://archive.ph/nkAsO>)
        pub const bt_601_625 = .{
            // x         y
            rS("0.64"), rS("0.33"),
            rS("0.29"), rS("0.6"),
            rS("0.15"), rS("0.06"),
        };

        // Source: <https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.601-7-201103-I!!PDF-E.pdf> (<https://archive.ph/nkAsO>)
        pub const bt_601_525 = .{
            // x          y
            rS("0.63"),  rS("0.34"),
            rS("0.31"),  rS("0.595"),
            rS("0.155"), rS("0.07"),
        };

        // Source: <https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.2020-2-201510-I!!PDF-E.pdf>
        pub const bt_2020 = .{
            // x          y
            rS("0.708"), rS("0.292"),
            rS("0.170"), rS("0.797"),
            rS("0.131"), rS("0.046"),
        };
    };

    pub const transform_matrices = struct {
        // Source: duh
        pub const identity = .{
            // X       Y        Z
            rS("1"), rS("0"), rS("0"),
            rS("0"), rS("1"), rS("0"),
            rS("0"), rS("0"), rS("1"),
        };

        // Source: <https://en.wikipedia.org/w/index.php?title=YCbCr&oldid=1203379996>
        pub const bt_709 = .{
            // X       Y              Z
            rS("1"), rS("0"),       rS("1.5748"),
            rS("1"), rS("-0.1873"), rS("-0.4681"),
            rS("1"), rS("1.8556"),  rS("0"),
        };
    };

    // Source: <https://en.wikipedia.org/w/index.php?title=Standard_illuminant&oldid=1207711650>
    pub const white_points = struct {
        pub const d_65 = .{ rS("0.31272"), rS("0.32903") };
        pub const d_50 = .{ rS("0.34567"), rS("0.3585") };
        pub const e = .{.{ .num = 1, .den = 3 }} ** 3;
    };

    pub const s_rgb = .{ .three = .{
        .primaries = primaries.bt_709,
        .white_point = white_points.d_65,
        .transform_matrix = transform_matrices.identity,
        .transfer_fn = .{ .fixed = .s_rgb },
    } };

    const bt_709_ycbcr_cs = .{
        .primaries = primaries.bt_709,
        .white_point = white_points.d_65,
        .transform_matrix = transform_matrices.bt_709,
        .transfer_fn = .{ .fixed = .bt_601 },
    };

    pub const bt_709_rgb = .{ .three = .{
        .primaries = primaries.bt_709,
        .white_point = white_points.d_65,
        .transform_matrix = transform_matrices.identity,
        .transfer_fn = .{ .fixed = .bt_601 },
    } };

    pub const bt_709_rgba_a = .{ .four = .{
        .xyz_color_space = bt_709_rgb.three,
        .transparency = .associated,
    } };

    pub const bt_709_rgba_ua = .{ .four = .{
        .xyz_color_space = bt_709_rgb.three,
        .transparency = .unassociated,
    } };

    pub const bt_709_444 = .{ .three = bt_709_ycbcr_cs };

    pub const s_rgb_linear = .{ .three = .{
        .primaries = primaries.bt_709,
        .white_point = white_points.d_65,
        .transform_matrix = transform_matrices.identity,
        .transfer_fn = .{ .gamma = rS("1") },
    } };

    pub const s_rgba_ua_linear = .{ .four = .{
        .xyz_color_space = .{
            .primaries = primaries.bt_709,
            .white_point = white_points.d_65,
            .transform_matrix = transform_matrices.identity,
            .transfer_fn = .{ .gamma = rS("1") },
        },
        .transparency = .unassociated,
    } };

    pub const bt_709_420_tl = .{ .three_subsampled = .{
        .xyz_color_space = bt_709_ycbcr_cs,
        .subsampling = .top_left,
    } };

    pub const bt_709_420_cl = .{ .three_subsampled = .{
        .xyz_color_space = bt_709_ycbcr_cs,
        .subsampling = .center_left,
    } };

    // TODO
    pub const bt_709_422 = .{ .three = bt_709_ycbcr_cs };
    // TODO
    pub const bt_709_411 = .{ .three_subsampled = .{
        .xyz_color_space = bt_709_ycbcr_cs,
        .subsampling = .top_left,
    } };

    pub const bt_709_gray = .{ .one = .{ .fixed = .bt_601 } };
    pub const bt_709_graya_ua = .{ .two = .{
        .x_transfer = .{ .fixed = .bt_601 },
        .transparency = .unassociated,
    } };
    pub const s_rgb_gray = .{ .one = .{ .fixed = .bt_601 } };

    pub const one_is_white = .{ .bit = .one_is_white };
    pub const one_is_black = .{ .bit = .one_is_black };
};

pub const Format = struct {
    pub const Range = enum {
        /// The entire range is used.
        full,

        /// The range of the components is limited to 16-235.
        /// FIXME: what about non-8bit?
        limited,
    };

    layout: Layout,
    range: Range,
    interpretation: Interpretation,

    pub fn eql(a: Format, b: Format) bool {
        return a.layout == b.layout and a.range == b.range and a.interpretation.eql(b.interpretation, a.layout);
    }

    pub const s_rgb_8 = .{
        .layout = .xyz,
        .range = .full,
        .interpretation = Interpretation.s_rgb,
    };
    pub const s_rgb_10 = .{
        .layout = .xyz10,
        .range = .full,
        .interpretation = Interpretation.s_rgb,
    };
    pub const s_rgb_12 = .{
        .layout = .xyz12,
        .range = .full,
        .interpretation = Interpretation.s_rgb,
    };
    pub const s_rgb_16 = .{
        .layout = .xyz16,
        .range = .full,
        .interpretation = Interpretation.s_rgb,
    };

    pub const bt_709_gray_8 = .{
        .layout = .x,
        .range = .full,
        .interpretation = Interpretation.bt_709_gray,
    };
    pub const bt_709_gray_10 = .{
        .layout = .x10,
        .range = .full,
        .interpretation = Interpretation.bt_709_gray,
    };
    pub const bt_709_gray_12 = .{
        .layout = .x12,
        .range = .full,
        .interpretation = Interpretation.bt_709_gray,
    };
    pub const bt_709_gray_16 = .{
        .layout = .x16,
        .range = .full,
        .interpretation = Interpretation.bt_709_gray,
    };

    pub const bt_709_graya_ua_8 = .{
        .layout = .xy,
        .range = .full,
        .interpretation = Interpretation.bt_709_graya_ua,
    };
    pub const bt_709_graya_ua_10 = .{
        .layout = .xy10,
        .range = .full,
        .interpretation = Interpretation.bt_709_graya_ua,
    };
    pub const bt_709_graya_ua_12 = .{
        .layout = .xy12,
        .range = .full,
        .interpretation = Interpretation.bt_709_graya_ua,
    };
    pub const bt_709_graya_ua_16 = .{
        .layout = .xy16,
        .range = .full,
        .interpretation = Interpretation.bt_709_graya_ua,
    };

    pub const bt_709_rgb_8 = .{
        .layout = .xyz,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgb,
    };
    pub const bt_709_rgb_10 = .{
        .layout = .xyz10,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgb,
    };
    pub const bt_709_rgb_12 = .{
        .layout = .xyz12,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgb,
    };
    pub const bt_709_rgb_16 = .{
        .layout = .xyz16,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgb,
    };

    pub const bt_709_rgba_ua_8 = .{
        .layout = .xyz,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgba_ua,
    };
    pub const bt_709_rgba_ua_10 = .{
        .layout = .xyz10,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgba_ua,
    };
    pub const bt_709_rgba_ua_12 = .{
        .layout = .xyz12,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgba_ua,
    };
    pub const bt_709_rgba_ua_16 = .{
        .layout = .xyz16,
        .range = .full,
        .interpretation = Interpretation.bt_709_rgba_ua,
    };

    pub const bt_709_444_8 = .{
        .layout = .xyzp_444,
        .range = .limited,
        .interpretation = Interpretation.bt_709_444,
    };
    pub const bt_709_444_10 = .{
        .layout = .xyzp_10_444,
        .range = .limited,
        .interpretation = Interpretation.bt_709_444,
    };
    pub const bt_709_444_12 = .{
        .layout = .xyzp_12_444,
        .range = .limited,
        .interpretation = Interpretation.bt_709_444,
    };
    pub const bt_709_444_16 = .{
        .layout = .xyzp_16_444,
        .range = .limited,
        .interpretation = Interpretation.bt_709_444,
    };

    pub const bt_709_420_8 = .{
        .layout = .xyzp_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_tl,
    };
    pub const bt_709_420_10 = .{
        .layout = .xyzp_10_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_tl,
    };
    pub const bt_709_420_12 = .{
        .layout = .xyzp_12_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_tl,
    };
    pub const bt_709_420_16 = .{
        .layout = .xyzp_16_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_tl,
    };

    pub const bt_709_420_jpeg_8 = .{
        .layout = .xyzp_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_cl,
    };
    pub const bt_709_420_jpeg_10 = .{
        .layout = .xyzp_10_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_cl,
    };
    pub const bt_709_420_jpeg_12 = .{
        .layout = .xyzp_12_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_cl,
    };
    pub const bt_709_420_jpeg_16 = .{
        .layout = .xyzp_16_420,
        .range = .limited,
        .interpretation = Interpretation.bt_709_420_cl,
    };

    pub const bt_709_411_8 = .{
        .layout = .xyzp_411,
        .range = .limited,
        .interpretation = Interpretation.bt_709_411,
    };
    pub const bt_709_411_10 = .{
        .layout = .xyzp_10_411,
        .range = .limited,
        .interpretation = Interpretation.bt_709_411,
    };
    pub const bt_709_411_12 = .{
        .layout = .xyzp_12_411,
        .range = .limited,
        .interpretation = Interpretation.bt_709_411,
    };
    pub const bt_709_411_16 = .{
        .layout = .xyzp_16_411,
        .range = .limited,
        .interpretation = Interpretation.bt_709_411,
    };

    pub const mono_light_on = .{
        .layout = .x1,
        .range = .full,
        .interpretation = Interpretation.one_is_white,
    };
    pub const mono_ink_on = .{
        .layout = .x1,
        .range = .full,
        .interpretation = Interpretation.one_is_black,
    };

    pub const mono_light_on_bytes = .{
        .layout = .x1b,
        .range = .full,
        .interpretation = Interpretation.one_is_white,
    };
    pub const mono_ink_on_bytes = .{
        .layout = .x1b,
        .range = .full,
        .interpretation = Interpretation.one_is_black,
    };
};

// TODO: interlacing

pub const Layout = enum {
    /// 1 component, 1 bit per component
    x1,
    /// 1 component, 1 byte per component, only the low bit is used
    x1b,
    /// 1 component, 1 byte per component
    x,
    /// 1 component, 2 bytes per component, only the low 10 bits are used
    x10,
    /// 1 component, 2 bytes per component, only the low 12 bits are used
    x12,
    /// 1 component, 2 bytes per component, only the low 16 bits are used
    x16,

    /// Two components, 1 byte per component, packed
    xy,
    /// Two components, 2 bytes per component, only the low 10 bits are used, packed
    xy10,
    /// Two components, 2 bytes per component, only the low 12 bits are used, packed
    xy12,
    /// Two components, 2 bytes per component, packed
    xy16,

    /// Three components, 1 byte per component, packed
    xyz,
    /// Three components, 2 bytes per component, only the low 10 bits are used, packed
    xyz10,
    /// Three components, 2 bytes per component, only the low 12 bits are used, packed
    xyz12,
    /// Three components, 2 bytes per component, packed
    xyz16,
    /// Three components, 2 bytes per sample, 5 samples for X and Y, 6 samples for Z, packed
    xyz565,

    /// Four components, 1 byte per component, packed
    xyzw,
    /// Four components, 2 bytes per component, only the low 10 bits are used, packed
    xyzw10,
    /// Four components, 2 bytes per component, only the low 12 bits are used, packed
    xyzw12,
    /// Four components, 2 bytes per component, packed
    xyzw16,

    /// Three components, 1 byte per component, Y and Z planes fully sampled, planar
    xyzp_444,
    /// Three components, 2 bytes per component, only the low 10 bits are used, Y and Z planes fully sampled, planar
    xyzp_10_444,
    /// Three components, 2 bytes per component, only the low 12 bits are used, Y and Z planes fully sampled, planar
    xyzp_12_444,
    /// Three components, 2 bytes per component, Y and Z planes fully sampled, planar
    xyzp_16_444,

    /// Three components, 1 byte per component, Y and Z planes sampled at half the rate, planar
    xyzp_422,
    /// Three components, 2 bytes per component, only the low 10 bits are used, Y and Z planes sampled at half the rate, planar
    xyzp_10_422,
    /// Three components, 2 bytes per component, only the low 12 bits are used, Y and Z planes sampled at half the rate, planar
    xyzp_12_422,
    /// Three components, 2 bytes per component, Y and Z planes sampled at half the rate, planar
    xyzp_16_422,

    /// Three components, 1 byte per component, Y and Z planes sampled at quarter the rate, planar
    xyzp_420,
    /// Three components, 2 bytes per component, only the low 12 bits are used, Y and Z planes sampled at quarter the rate, planar
    xyzp_10_420,
    /// Three components, 2 bytes per component, only the low 10 bits are used, Y and Z planes sampled at quarter the rate, planar
    xyzp_12_420,
    /// Three components, 2 bytes per component, Y and Z planes sampled at quarter the rate, planar
    xyzp_16_420,

    /// Three components, 1 byte per component, Y and Z planes sampled at quarter the rate, planar
    xyzp_411,
    /// Three components, 2 bytes per component, only the low 10 bits are used, Y and Z planes sampled at quarter the rate, planar
    xyzp_10_411,
    /// Three components, 2 bytes per component, only the low 12 bits are used, Y and Z planes sampled at quarter the rate, planar
    xyzp_12_411,
    /// Three components, 2 bytes per component, Y and Z planes sampled at quarter the rate, planar
    xyzp_16_411,

    /// Four components, 1 byte per component, Y and Z planes fully sampled, W plane fully sampled, planar
    xyzwp_4444,
    /// Four components, 1 byte per component, Y and Z planes sampled at quarter the rate, W plane fully sampled, planar
    xyzwp_4204,
};

// pub const infos = std.enums.directEnumArray(SampleFormat, SampleFormatInfo, 0, @compileError("TODO"));
