const std = @import("std");
const zenit = @import("zenit.zig");

pub fn Nonexhaustive(comptime Enum: type) type {
    comptime var i = @typeInfo(Enum);
    i.Enum.is_exhaustive = false;
    return @Type(i);
}

pub fn TagEnum(comptime Union: type) type {
    const i = @typeInfo(Union);
    var df: [i.Union.fields.len]std.builtin.Type.EnumField = undefined;
    for (i.Union.fields, 0..) |field, idx| {
        df[idx] = .{ .name = field.name, .value = idx };
    }

    return @Type(.{ .Enum = .{
        .fields = &df,
        .tag_type = std.math.IntFittingRange(0, i.Union.fields.len - 1),
        .decls = &.{},
        .is_exhaustive = true,
    } });
}

pub fn Tagged(comptime Union: type) type {
    var i = @typeInfo(Union);
    i.Union.tag_type = TagEnum(Union);
    i.Union.decls = &.{};
    return @Type(i);
}

pub fn parseUnsignedDiag(comptime T: type, buf: []const u8, base: u8, z: *zenit.Context) std.fmt.ParseIntError!T {
    return std.fmt.parseUnsigned(T, buf, base) catch |err| {
        const displayed_buf = buf[0..@min(64, buf.len)];
        return switch (err) {
            error.Overflow => |e| try z.diag(
                .err,
                .glue,
                e,
                "Overflow occurred while trying to parse \"{s}\" (possibly truncated) as {s}.",
                .{ std.fmt.fmtSliceEscapeLower(displayed_buf), @typeName(T) },
            ),
            error.InvalidCharacter => |e| try z.diag(
                .err,
                .glue,
                e,
                "Invalid character encountered while trying to parse \"{s}\" (possibly truncated) as {s}.",
                .{ std.fmt.fmtSliceEscapeLower(displayed_buf), @typeName(T) },
            ),
        };
    };
}

pub fn parseIntDiag(comptime T: type, buf: []const u8, base: u8, z: *zenit.Context) std.fmt.ParseIntError!T {
    return std.fmt.parseInt(T, buf, base) catch |err| {
        const displayed_buf = buf[0..@min(64, buf.len)];
        return switch (err) {
            error.Overflow => |e| try z.diag(
                .err,
                .glue,
                e,
                "Overflow occurred while trying to parse \"{s}\" (possibly truncated) as {s}.",
                .{ std.fmt.fmtSliceEscapeLower(displayed_buf), @typeName(T) },
            ),
            error.InvalidCharacter => |e| try z.diag(
                .err,
                .glue,
                e,
                "Invalid character encountered while trying to parse \"{s}\" (possibly truncated) as {s}.",
                .{ std.fmt.fmtSliceEscapeLower(displayed_buf), @typeName(T) },
            ),
        };
    };
}

pub fn isElseValue(v: anytype) bool {
    const ti = @typeInfo(@TypeOf(v));
    if (ti != .Enum) @compileError("isElseValue takes an enum.");
    if (ti.Enum.is_exhaustive) return false;

    const v_int: ti.Enum.tag_type = @intFromEnum(v);

    // TODO optimization: detect sequential fields and check accordingly.

    inline for (ti.Enum.fields) |field| {
        if (v_int == field.value) return false;
    }

    return true;
}

pub fn hasPadding(comptime T: type) bool {
    const ti = @typeInfo(T);

    if (ti != .Struct) @compileError("structHasPadding takes a struct.");

    // packed structs have no padding
    if (ti.Struct.layout == .Packed) return true;

    const struct_size = @sizeOf(T);
    comptime var size_sum = 0;

    inline for (ti.Struct.fields) |field| {
        size_sum += @sizeOf(field.type);
    }

    return struct_size == size_sum;
}

pub fn Slicer(comptime T: type) type {
    return struct {
        const Self = @This();
        const E = std.meta.Elem(T);

        start: T,
        idx: usize = 0,

        pub fn next(slicer: *Self) error{Overflow}!E {
            return (try slicer.get(1))[0];
        }

        pub fn nextA(slicer: *Self) E {
            return (slicer.getA(1))[0];
        }

        pub fn rest(slicer: *Self) []const E {
            return slicer.start[slicer.idx..];
        }

        pub fn get(slicer: *Self, len: usize) error{Overflow}![]const E {
            const end = try std.math.add(slicer.idx, len);

            if (slicer.start.len < end) return error.Overflow;

            defer slicer.idx += len;
            return slicer.start[slicer.idx..end];
        }

        pub fn getA(slicer: *Self, len: usize) []const E {
            const end = slicer.idx + len;
            defer slicer.idx = end;
            return slicer.start[slicer.idx..end];
        }

        pub inline fn getArrayP(slicer: *Self, comptime len: usize) error{Overflow}!*const [len]E {
            return (try slicer.get(len))[0..len];
        }

        pub inline fn getArrayPA(slicer: *Self, comptime len: usize) *const [len]E {
            return (slicer.getA(len))[0..len];
        }
    };
}
