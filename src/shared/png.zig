const std = @import("std");
const assert = std.debug.assert;
const zenit = @import("../zenit.zig");

pub fn validatePngInt(z: *zenit.Context, png_int: u32, part: []const u8, name: []const u8) !void {
    if (png_int >> 31 != 0) try z.diag(
        .warn,
        .demuxer,
        error.InvalidPngInt,
        "{s} {s} {d} is larger than the PNG spec allows.",
        .{ part, name, png_int },
    );
}

// inline to prevent monomorphization bloat
pub inline fn validateExhaustiveValue(z: *zenit.Context, value: anytype, part: []const u8, name: []const u8) !void {
    if (zenit.util.isElseValue(value)) try z.diag(
        .warn,
        .demuxer,
        error.WasExhaustiveValue,
        "Unknown {s} {s} {d}.",
        .{ part, name, @intFromEnum(value) },
    );
}

// PNG specification: https://www.w3.org/TR/png-3/

const native_endian = @import("builtin").cpu.arch.endian();

pub const Crc = std.hash.crc.Crc32WithPoly(.IEEE);

pub const ChunkType = enum(u32) {
    fn getType(s: *const [@sizeOf(u32)]u8) u32 {
        return std.mem.readInt(u32, s, native_endian);
    }

    IHDR = getType("IHDR"),
    PLTE = getType("PLTE"),
    IDAT = getType("IDAT"),
    IEND = getType("IEND"),

    tRNS = getType("tRNS"),
    cHRM = getType("cHRM"),
    gAMA = getType("gAMA"),
    iCCP = getType("iCCP"),
    sBIT = getType("sBIT"),
    sRGB = getType("sRGB"),
    cICP = getType("cICP"),
    mDCv = getType("mDCv"),

    zTXt = getType("zTXt"),
    tEXt = getType("tEXt"),
    iTXt = getType("iTXt"),
    bKGD = getType("bKGD"),
    hIST = getType("hIST"),
    pHYs = getType("pHYs"),
    sPLT = getType("sPLT"),
    eXIf = getType("eXIf"),
    tIME = getType("tIME"),
    acTL = getType("acTL"),
    fcTL = getType("fcTL"),
    fdAT = getType("fdAT"),
    cLLi = getType("cLLi"),

    // https://www.hackerfactor.com/blog/index.php?/archives/895-Connecting-the-iDOTs.html
    iDOT = getType("iDOT"),
    // ImageMagick, apparently
    vpAg = getType("vpAg"),
    // ImageMagick, yet again: https://png-mng-misc.narkive.com/BPJE3zfe/typical-to-from-exif-conversions
    orNT = getType("orNT"),

    _,

    pub inline fn bytes(self: *const ChunkType) *const [@sizeOf(ChunkType)]u8 {
        return @ptrCast(self);
    }

    pub const Flags = packed struct {
        /// Critical chunks are necessary for successful display of the contents of the datastream.
        is_ancillary: bool,

        /// Public chunks are reserved for definition by the W3C.
        is_reserved: bool,

        /// The significance of the case of the third letter of the chunk name is reserved for
        /// possible future extension.
        is_private: bool,

        /// This property bit is not of interest to pure decoders, but it is needed by PNG editors.
        is_copyable: bool,
    };

    pub inline fn flags(self: ChunkType) Flags {
        const b = self.bytes();
        return .{
            .is_ancillary = b[0] & ' ' != 0,
            .is_reserved = b[1] & ' ' != 0,
            .is_private = b[2] & ' ' != 0,
            .is_copyable = b[3] & ' ' != 0,
        };
    }
};

pub const WriteChunk = struct {
    kind: ChunkType,
    bytes: []const u8,
    crc: u32,

    pub fn init(kind: ChunkType, data: []const u8) WriteChunk {
        return .{
            .kind = kind,
            .bytes = data,
            .crc = compute_crc: {
                const len_bytes: [@sizeOf(u32)]u8 = @bitCast(std.mem.nativeToBig(u32, @as(u31, @intCast(data.len))));

                var c: Crc = .{};
                c.hash(kind.bytes());
                c.hash(&len_bytes);
                c.hash(data);
                break :compute_crc c.final();
            },
        };
    }

    pub fn write(wc: WriteChunk, out: zenit.OutputStream) void {
        const len_bytes: [@sizeOf(u32)]u8 = @bitCast(std.mem.nativeToBig(u32, @as(u31, @intCast(wc.data.len))));

        var iovecs = [_]std.os.iovec_const{
            .{ .iov_base = &len_bytes, .iov_len = len_bytes.len },
            .{ .iov_base = wc.kind.bytes().ptr, .iov_len = @sizeOf(ChunkType) },
            .{ .iov_base = wc.bytes.ptr, .iov_len = wc.bytes.len },
            .{ .iov_base = std.mem.toBytes(std.mem.nativeToBig(u32, wc.crc)), .iov_len = @sizeOf(u32) },
        };

        try out.writevAll(&iovecs);
    }
};

pub const ReadChunk = struct {
    bytes: []const u8,

    pub inline fn chunkType(chunk: ReadChunk) ChunkType {
        return @enumFromInt(@as(u32, @bitCast(chunk.bytes[0..@sizeOf(ChunkType)].*)));
    }

    pub inline fn data(chunk: ReadChunk) []const u8 {
        return chunk.bytes[@sizeOf(ChunkType) .. chunk.bytes.len - @sizeOf(u32)];
    }

    pub inline fn crc(chunk: ReadChunk) u32 {
        return std.mem.readInt(u32, chunk.bytes[chunk.bytes.len - @sizeOf(u32) ..][0..@sizeOf(u32)], .big);
    }

    pub inline fn computeCrc(chunk: ReadChunk) u32 {
        return Crc.hash(chunk.bytes[0 .. chunk.bytes.len - @sizeOf(u32)]);
    }

    pub fn dealloc(chunk: ReadChunk, ally: std.mem.Allocator) void {
        ally.free(chunk.bytes);
    }

    pub fn validate(chunk: ReadChunk, z: *zenit.Context) !void {
        const stored_crc = chunk.crc();
        const actual_crc = chunk.computeCrc();

        if (stored_crc != actual_crc) try z.diag(
            .warn,
            .decoder,
            error.InvalidCrc,
            "CRC mismatch in {d}-byte {s} chunk: stored CRC was {x}, actual CRC was {x}.",
            .{ chunk.data().len, chunk.chunkType().bytes(), stored_crc, actual_crc },
        );
    }

    pub fn read(z: *zenit.Context, ally: std.mem.Allocator, in: *zenit.InputStream, max_size: usize) !ReadChunk {
        var alu = std.ArrayListUnmanaged(u8){};

        const len = try in.readInt(u32, .big);

        try validatePngInt(z, len, "chunk", "length");
        if (len > max_size) try z.diag(.err, .demuxer, error.TooLong, "A chunk was too long ({d} > {d})", .{ len, max_size });

        const len_to_read: usize = @sizeOf(ChunkType) + len + @sizeOf(u32);

        try alu.ensureTotalCapacityPrecise(ally, len_to_read);

        const chunk_type_bytes = alu.addManyAsArrayAssumeCapacity(@sizeOf(ChunkType));
        try in.readNoEof(chunk_type_bytes);
        const chunk_type: ChunkType = @enumFromInt(@as(u32, @bitCast(chunk_type_bytes.*)));
        validateExhaustiveValue(z, chunk_type, "chunk", "type") catch |e| {
            if (!chunk_type.flags().is_ancillary) return e;
        };

        try in.readNoEof(alu.addManyAsSliceAssumeCapacity(len_to_read - @sizeOf(u32)));

        return .{ .bytes = alu.toOwnedSlice(ally) catch unreachable };
    }

    pub fn peekChunkType(in: *zenit.InputStream) ?ChunkType {
        const peeked = in.peek(@sizeOf(ChunkType) + @sizeOf(u32)) catch return null;
        if (peeked.len < 8) return null;
        return @bitCast(peeked[@sizeOf(u32)..][0..@sizeOf(ChunkType)].*);
    }

    pub fn peek(in: *zenit.InputStream) ?struct { u32, ChunkType } {
        const peeked = in.peek(@sizeOf(ChunkType) + @sizeOf(u32)) catch return null;
        if (peeked.len < 8) return null;
        return .{
            std.mem.bigToNative(u32, @bitCast(peeked[0..@sizeOf(u32)].*)),
            @bitCast(peeked[@sizeOf(u32)..][0..@sizeOf(ChunkType)].*),
        };
    }

    pub fn skipWithCrc(in: *zenit.InputStream) !u32 {

        return try in.readInt(u32, .big);
    }
};

/// "Image header"
pub const Ihdr = extern struct {
    pub const BitDepth = enum(u8) {
        one = 1,
        two = 2,
        four = 4,
        eight = 8,
        sixteen = 16,
        _,
    };

    pub const ColorType = enum(u8) {
        gray = 0,
        true_color = 2,
        indexed = 3,
        gray_alpha = 4,
        true_color_alpha = 6,
        _,
    };

    pub const CompressionMethod = enum(u8) {
        zlib = 0,
        _,
    };

    pub const FilterMethod = enum(u8) {
        adaptive = 0,
        _,
    };

    pub const InterlaceMethod = enum(u8) {
        none = 0,
        adam7 = 1,
        _,
    };

    width: u32,
    height: u32,
    depth: BitDepth,
    color_type: ColorType,
    compression_method: CompressionMethod,
    filter_method: FilterMethod,
    interlace_method: InterlaceMethod,

    pub const ValidationError = error{
        InvalidHeight,
        InvalidWidth,
        InvalidDepth,
        InvalidColorType,
        InvalidCompressionMethod,
        InvalidFilterMethod,
        InvalidInterlaceMethod,
    };

    pub fn fromReadChunk(chunk: ReadChunk) !Ihdr {
        if (chunk.data().len != 13) return error.BadLength;
        var sl = zenit.util.Slicer([]const u8){ .start = chunk.data() };
        return .{
            .width = std.mem.readInt(u32, sl.getArrayPA(@sizeOf(u32)), .big),
            .height = std.mem.readInt(u32, sl.getArrayPA(@sizeOf(u32)), .big),
            .depth = @enumFromInt(sl.nextA()),
            .color_type = @enumFromInt(sl.nextA()),
            .compression_method = @enumFromInt(sl.nextA()),
            .filter_method = @enumFromInt(sl.nextA()),
            .interlace_method = @enumFromInt(sl.nextA()),
        };
    }

    pub fn asWriteChunk(ihdr: *Ihdr) WriteChunk {
        return WriteChunk.init(.IHDR, std.mem.asBytes(ihdr));
    }

    pub fn validate(ihdr: Ihdr, z: *zenit.Context) ValidationError!void {
        validatePngInt(z, ihdr.width, "IHDR", "width") catch return error.InvalidWidth;
        validatePngInt(z, ihdr.height, "IHDR", "height") catch return error.InvalidHeight;
        validateExhaustiveValue(z, ihdr.depth, "IHDR", "bit depth") catch return error.InvalidDepth;
        validateExhaustiveValue(z, ihdr.color_type, "IHDR", "color type") catch return error.InvalidColorType;
        validateExhaustiveValue(z, ihdr.compression_method, "IHDR", "compression method") catch return error.InvalidCompressionMethod;
        validateExhaustiveValue(z, ihdr.filter_method, "IHDR", "filter method") catch return error.InvalidFilterMethod;
        validateExhaustiveValue(z, ihdr.interlace_method, "IHDR", "interlace method") catch return error.InvalidInterlaceMethod;
    }
};

/// "Significant bits"
pub const Sbit = struct {
    depths: [4]u8,

    /// It is illegal to pass an unnamed value for `ct`.
    pub fn len(ct: Ihdr.ColorType) u32 {
        return switch (ct) {
            .gray => 1,
            .true_color, .indexed => 3,
            .gray_alpha => 2,
            .true_color_alpha => 4,
            else => unreachable,
        };
    }

    pub fn slice(self: *Sbit, ct: Ihdr.ColorType) []u8 {
        return self.depths[0..len(ct)];
    }

    pub fn fromReadChunk(rc: ReadChunk, ct: Ihdr.ColorType) error{InvalidLength}!Sbit {
        var sbit: Sbit = undefined;
        const sl = sbit.slice(ct);
        if (sl.len != rc.data.len()) return error.InvalidLength;
        @memcpy(sl, rc.data());
    }

    const ValidationError = error{TooManyBits};
    pub fn validate(sbit: Sbit, ct: Ihdr.ColorType, z: *zenit.Context) ValidationError!void {
        _ = z; // autofix
        const sl = sbit.slice(ct);
        _ = sl; // autofix
        @compileError("TODO");
    }

    pub fn asWriteChunk(self: *Sbit, ct: Ihdr.ColorType) WriteChunk {
        return WriteChunk.init(.sBIT, self.slice(ct));
    }
};

/// "Standard RGB colour space"
pub const Srgb = struct {
    pub const RenderingIntent = enum(u8) {
        /// "for images preferring good adaptation to the output device gamut at the expense of
        /// colorimetric accuracy, such as photographs."
        perceptual = 0,
        /// "for images requiring colour appearance matching (relative to the output device white
        /// point), such as logos."
        relative_colorimetric = 1,
        /// "for images preferring preservation of saturation at the expense of hue and lightness,
        /// such as charts and graphs."
        saturation = 2,
        /// "for images requiring preservation of absolute colorimetry, such as previews of images
        /// destined for a different output device (proofs)."
        absolute_colorimetric = 3,
        _,
    };

    rendering_intent: RenderingIntent,

    pub fn fromReadChunk(rc: ReadChunk) error{InvalidLength}!Srgb {
        const sl = rc.data();
        if (sl.len != 1) return error.InvalidLength;

        return .{ .rendering_intent = @enumFromInt(sl[0]) };
    }

    pub fn asWriteChunk(srgb: *Srgb) WriteChunk {
        return WriteChunk.init(.sRGB, std.mem.asBytes(&srgb.rendering_intent));
    }

    const ValidationError = error{InvalidRenderingIntent};
    pub fn validate(srgb: Srgb, z: *zenit.Context) ValidationError!void {
        validateExhaustiveValue(z, srgb.rendering_intent, "sRGB", "rendering intent") catch return error.InvalidRenderingIntent;
    }
};

/// "Image gamma"
pub const Gama = struct {
    gamma: u32,

    pub fn fromReadChunk(rc: ReadChunk) error{InvalidLength}!Gama {
        const sl = rc.data();
        if (sl.len != @sizeOf(u32)) return error.InvalidLength;

        return .{ .gamma = std.mem.readInt(u32, sl[0..@sizeOf(u32)], .big) };
    }

    pub fn asWriteChunk(gama: *Gama, buf: *[@sizeOf(u32)]u8) WriteChunk {
        std.mem.writeInt(u32, buf, gama.gamma, .big);
        return WriteChunk.init(.gAMA, buf);
    }

    const ValidationError = error{GammaGreaterOne};
    pub fn validate(gama: Gama, z: *zenit.Context) ValidationError!void {
        if (gama.gamma > 100000) return try z.diag(
            .warn,
            .decoder,
            error.GammaGreaterOne,
            "Gamma value {d}:100000 greater 1.",
            .{gama.gamma},
        );
    }

    pub fn toRational(gama: Gama, comptime Rational: type) Rational {
        if (comptime std.math.maxInt(Rational.T) < 100000) @compileError("gama values do not fit into type `" ++ @typeName(Rational.T) ++ "'");
        return .{ .num = @intCast(gama.gamma), .den = 100000 };
    }
};

/// "Coding-independent code points for video signal type identification"
// T-REC H.273
pub const Cicp = extern struct {
    pub const Primaries = enum(u8) {
        bt_709 = 1,
        unspecified = 2,
        bt_470_m = 4,
        bt_470_bg = 5,
        bt_601_525 = 6,
        smpte_240_m = 7,
        generic_film = 8,
        bt_2020 = 9,
        cie_xyz = 10,
        // https://pub.smpte.org/latest/rp431-2/rp0431-2-2011.pdf
        // TODO
        smpte_rp_431 = 11,
        smpte_eg_432 = 12,
        ebu_t_3213_e = 22,
        _,
    };

    pub const TransferFn = enum(u8) {
        bt_709 = 1,
        unspecified = 2,
        gamma_2_2 = 4,
        gamma_2_8 = 5,
        bt_601 = 6,
        smpte_240_m = 7,
        linear = 8,
        log_10 = 9,
        log_10_2 = 10,
        iec_61966_2_4 = 11,
        bt_1361 = 12,
        s_rgb = 13,
        bt_2020_10bit = 14,
        bt_2020_12bit = 15,
        smpte_st_2084 = 16,
        smpte_st_428_1 = 17,
        arib_std_b67 = 18,
        _,
    };

    pub const Range = enum(u8) {
        limited = 0,
        full = 1,
        _,
    };

    pub const MatrixCoefficients = enum(u8) {
        identity = 0,
        bt_709 = 1,
        unspecified = 2,
        title_47 = 4,
        bt_470_bg = 5,
        smpte_240_m = 6,
        y_cg_co = 8,
        bt_2020 = 9,
        bt_2020_cl = 10,
        y_dz_dx = 11,
        // TODO
        what = 12,
        // TODO
        wtf = 13,
        i_ct_cp = 14,
        _,
    };

    primaries: Primaries,
    transfer_fn: TransferFn,
    matrix_coeffs: MatrixCoefficients,
    range: Range,

    pub fn fromReadChunk(rc: ReadChunk) error{InvalidLength}!Cicp {
        const sl = rc.data();
        if (sl.len != 4) return error.InvalidLength;

        return @bitCast(rc[0..4].*);
    }

    pub fn asWriteChunk(cicp: *Cicp) WriteChunk {
        return WriteChunk.init(.cICP, std.mem.asBytes(cicp));
    }

    const ValidationError = error{ InvalidPrimaries, InvalidTransferFn, InvalidMatrixCoeffs, InvalidRange, MatrixCoeffsNotApplicable };
    pub fn validate(cicp: Cicp, z: *zenit.Context) ValidationError!void {
        validateExhaustiveValue(z, cicp.primaries, "cICP", "primaries") catch return error.InvalidPrimaries;
        validateExhaustiveValue(z, cicp.transfer_fn, "cICP", "transfer fn") catch return error.InvalidTransferFn;
        validateExhaustiveValue(z, cicp.matrix_coeffs, "cICP", "matrix coeffs") catch return error.InvalidMatrixCoeffs;
        validateExhaustiveValue(z, cicp.matrix_coeffs, "cICP", "range") catch return error.InvalidRange;
        if (cicp.matrix_coeffs != .identity) try z.diag(
            .warn,
            .decoder,
            error.MatrixCoeffsNotApplicable,
            "cICP matrix coefficients {} are not applicable to PNG.",
            .{cicp.matrix_coeffs},
        );
    }

    pub fn toInterpretation(cicp: Cicp) zenit.image.Interpretation {
        _ = cicp; // autofix
        @compileError("TODO");
    }

    pub fn range(cicp: Cicp) zenit.image.Format.Range {
        return switch (cicp.range) {
            .limited => .limited,
            .full => .full,
            else => unreachable,
        };
    }
};

pub const Actl = extern struct {
    nb_frames: u32,
    nb_plays: u32,

    pub fn fromReadChunk(rc: ReadChunk) error{InvalidLength}!Actl {
        const sl = rc.data();
        if (sl.len != @sizeOf(u32) * 2) return error.InvalidLength;

        return .{
            .nb_frames = std.mem.readInt(u32, sl[0..@sizeOf(u32)], .big),
            .nb_plays = std.mem.readInt(u32, sl[@sizeOf(u32)..][0..@sizeOf(u32)], .big),
        };
    }

    pub fn asWriteChunk(cicp: *Cicp) WriteChunk {
        return WriteChunk.init(.cICP, std.mem.asBytes(cicp));
    }
};
