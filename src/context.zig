const std = @import("std");
const zenit = @import("zenit.zig");

pub const Diagnostic = struct {
    pub const Component = enum {
        demuxer,
        decoder,
        muxer,
        encoder,
        filter,
        io,
        glue,
        user,
    };

    /// The severity of the failure.
    level: std.log.Level,
    /// The component this failure occurred in.
    component: Component,
    /// A human-readable description of the failure.
    message: std.BoundedArray(u8, 512),
    /// The machine-readable Zig error for this failure.
    err: ?anyerror,

    pub inline fn diagFmt(d: *Diagnostic, level: std.log.Level, component: Component, err: ?anyerror, comptime fmt: []const u8, args: anytype) void {
        @setCold(true);
        d.level = level;
        d.component = component;
        d.err = err;
        d.message.len = @intCast((std.fmt.bufPrint(&d.message.buffer, fmt, args) catch "").len);
    }

    pub fn format(d: Diagnostic, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
        return writer.print("[{s}] {s}: {s} ({?})", .{ @tagName(d.level), @tagName(d.component), d.message.constSlice(), d.err });
    }
};

/// Various allocators used in different situations. Using the same AllocStrat from different
/// threads is thread-safe iff all inner allocators are thread-safe.
pub const AllocStrat = struct {
    const nb_cached_temps = 32;

    /// An allocator for many large objects.
    buf: std.mem.Allocator,

    /// An allocator for everything else.
    general: std.mem.Allocator,

    /// Backing for temporary allocators.
    backing: std.mem.Allocator,

    /// Pool of temporary allocators.
    temps: std.BoundedArray(std.heap.ArenaAllocator.State, nb_cached_temps) = .{},

    temps_mutex: std.Thread.Mutex = .{},

    const optimize_for: enum {
        speed,
        memory,
    } = .speed;

    /// Uses the same allocator for everything except alloc.backing. This is not optimal.
    pub fn initSingleAlly(ally: std.mem.Allocator) AllocStrat {
        return .{
            .buf = ally,
            .general = ally,
            .backing = std.heap.page_allocator,
        };
    }

    pub fn acquireTemp(self: *AllocStrat) std.heap.ArenaAllocator {
        self.temps_mutex.lock();
        defer self.temps_mutex.unlock();

        if (self.temps.len != 0) {
            return self.temps.pop().promote(self.backing);
        }
        return std.heap.ArenaAllocator.init(self.backing);
    }

    pub fn releaseTemp(self: *AllocStrat, const_temp: std.heap.ArenaAllocator) void {
        var temp = const_temp;
        _ = temp.reset(.{ .retain_capacity = {} });
        if (temp.state.buffer_list.first == null) return;

        self.temps_mutex.lock();
        defer self.temps_mutex.unlock();

        if (self.temps.len < nb_cached_temps) {
            self.temps.append(temp.state) catch unreachable;
        } else {
            // evict a temp
            const idx = switch (optimize_for) {
                .speed => nb_cached_temps - 1,
                .memory => get_chosen: {
                    var max_cap: usize = 0;
                    var chosen: usize = 0;

                    for (self.temps.constSlice(), 0..) |candidate, i| {
                        const cap = candidate.promote(undefined).queryCapacity();
                        if (cap >= max_cap) {
                            max_cap = cap;
                            chosen = i;
                        }
                    }

                    break :get_chosen chosen;
                },
            };
            self.temps.buffer[idx].promote(self.backing).deinit();
            self.temps.buffer[idx] = temp.state;
        }
    }

    pub fn deinit(self: *AllocStrat) void {
        for (self.temps.constSlice()) |temp| {
            temp.promote(self.backing).deinit();
        }
        self.* = undefined;
    }
};

const SystemAllocatorState = struct {
    fn allocator(_: SystemAllocatorState) std.mem.Allocator {
        return if (@import("builtin").link_libc) std.heap.c_allocator else std.heap.c_allocator;
    }
};
const system_allocator = (SystemAllocatorState{}).allocator();

var default_general_allocator_state = if (@import("builtin").link_libc) SystemAllocatorState{} else @import("binned_allocator.zig").BinnedAllocator(.{}){};
var default_buf_allocator_state = @import("free_list_allocator.zig").FreeListAllocator{ .inner = std.heap.page_allocator };
var default_buf_allocator_state_thread_safe = if (@import("builtin").link_libc) SystemAllocatorState{} else std.heap.ThreadSafeAllocator{ .child_allocator = default_buf_allocator_state.allocator() };

const default_alloc_strat: AllocStrat = .{
    .buf = default_buf_allocator_state_thread_safe.allocator(),
    .backing = std.heap.page_allocator,
    .general = default_general_allocator_state.allocator(),
};

// Mainly used for three things:
// 1. to present a convenient interface to the library user.
// 2. to handle memory allocation.
// 3. to handle diagnostics reporting.
// Note that using the same context from multiple threads is not thread-safe.
pub const Context = struct {
    alloc: AllocStrat,

    diagnostics: std.ArrayListUnmanaged(Diagnostic) = .{},

    bitstream_decoders: std.ArrayListUnmanaged(zenit.BitstreamDecoder) = .{},

    demuxers: std.ArrayListUnmanaged(zenit.Demuxer) = .{},
    packet_decoders: std.ArrayListUnmanaged(zenit.PacketDecoder) = .{},

    // packet_encoders: std.ArrayListUnmanaged(zenit.PacketCoder),
    // muxers: std.ArrayListUnmanaged(zenit.Demuxer),
    // bitstream_encoders: std.ArrayListUnmanaged(zenit.BitstreamCoder),

    pub const BitstreamSinkSelection = union(enum) {
        bitstream_decoder: usize,
        demuxer: usize,
    };

    pub fn initDefault() Context {
        return .{
            .alloc = default_alloc_strat,
        };
    }

    pub fn probe(z: *const Context, in: *zenit.InputStream) ?BitstreamSinkSelection {
        const buffer = in.peek(zenit.buffer_size) catch return null;

        // TODO: replace with more sophisticated probing

        var max_score_bd: zenit.probe.Score = 0;
        var max_score_idx_bd: usize = undefined;

        for (z.bitstream_decoders.items, 0..) |bd, i| {
            const score = bd.v_table.probeFn(buffer);
            if (score > max_score_bd) {
                max_score_bd = score;
                max_score_idx_bd = i;
            }
        }

        var max_score_dx: zenit.probe.Score = 0;
        var max_score_idx_dx: usize = undefined;

        for (z.demuxers.items, 0..) |dx, i| {
            const score = dx.v_table.probeFn(buffer);
            if (score > max_score_dx) {
                max_score_dx = score;
                max_score_idx_dx = i;
            }
        }

        if (max_score_bd | max_score_dx == 0 or max_score_bd == max_score_dx) return null;

        return if (max_score_bd > max_score_dx) .{ .bitstream_decoder = max_score_idx_bd } else .{ .demuxer = max_score_idx_dx };
    }

    pub fn deinit(z: *Context) void {
        z.bitstream_decoders.deinit(z.alloc.general);
        z.demuxers.deinit(z.alloc.general);
        z.packet_decoders.deinit(z.alloc.general);
        z.diagnostics.deinit(z.alloc.general);
        z.alloc.deinit();
        z.* = undefined;
    }

    pub fn registerBitstreamDecoder(z: *Context, dec: zenit.BitstreamDecoder) error{OutOfMemory}!void {
        return z.bitstream_decoders.append(z.alloc.general, dec);
    }

    pub fn registerBuiltin(z: *Context) error{OutOfMemory}!void {
        const start_bd_len = z.bitstream_decoders.items.len;
        const start_dx_len = z.demuxers.items.len;

        errdefer z.bitstream_decoders.items.len = start_bd_len;
        errdefer z.demuxers.items.len = start_dx_len;

        inline for (@typeInfo(zenit.decoders).Struct.decls) |f| {
            const dec = @field(zenit.decoders, f.name).decoder();
            switch (@TypeOf(dec)) {
                zenit.BitstreamDecoder => try z.bitstream_decoders.append(z.alloc.general, dec),
                zenit.Demuxer => try z.demuxers.append(z.alloc.general, dec),
                else => @compileError("bug"),
            }
        }
    }

    fn DiagRet(comptime Err: type) type {
        return if (@typeInfo(Err) == .ErrorSet) Err!noreturn else void;
    }

    pub inline fn diag(
        z: *Context,
        level: std.log.Level,
        component: Diagnostic.Component,
        err: anytype,
        comptime fmt: []const u8,
        args: anytype,
    ) DiagRet(@TypeOf(err)) {
        @setCold(true);
        if (comptime @hasDecl(@import("root"), "zenit_no_diagnostics") and @import("root").zenit_no_diagnostics) return err;

        const p = z.diagnostics.addOne(z.alloc.general) catch return err;
        p.diagFmt(level, component, if (@typeInfo(@TypeOf(err)) == .ErrorSet) err else null, fmt, args);
        return err;
    }
};
