const std = @import("std");
const types = @import("types.zig");

pub const TransferFn = enum {
    bt_601,
    s_rgb,
    s_log,

    // optical -> electric
    pub fn applyOetf(tf: TransferFn, comptime Float: type, sample: Float) Float {
        const tfs = TransferFns(Float);

        return switch (tf) {
            .bt_601 => tfs.oetf.bt601(sample),
            .s_rgb => tfs.oetf.sRgb(sample),
            .s_log => tfs.oetf.sLog(sample),
        };
    }

    // electric -> optical
    pub fn applyEotf(tf: TransferFn, comptime Float: type, sample: Float) Float {
        const tfs = TransferFns(Float);

        return switch (tf) {
            .bt_601 => tfs.eotf.bt601(sample),
            .s_rgb => tfs.eotf.sRgb(sample),
            .s_log => tfs.eotf.sLog(sample),
        };
    }
};

pub fn TransferFns(comptime Float: type) type {
    return struct {
        const pow = std.math.pow;
        const log10 = std.math.log10;

        pub const oetf = struct {
            pub inline fn bt601(optical: Float) Float {
                return if (optical < 0.018) optical * 4.5 else 1.099 * pow(Float, optical, 0.45) - 0.099;
            }

            pub inline fn sRgb(optical: Float) Float {
                return if (optical <= 0.0031308) optical * 12.92 else 1.055 * pow(Float, optical, 1.0 / 2.4) - 0.055;
            }

            pub inline fn sLog(optical: Float) Float {
                return (0.432699 * log10(optical + 0.037584) + 0.616596) + 0.03;
            }
        };

        pub const eotf = struct {
            pub inline fn bt601(electric: Float) Float {
                return if (electric < 0.081247944035) electric / 4.5 else pow(Float, (electric + 0.099) / 1.099, 2.22222222222);
            }

            pub inline fn sRgb(electric: Float) Float {
                return if (electric < 0.04045) electric / 12.92 else pow(Float, (electric + 0.055) / 1.055, 2.4);
            }

            pub inline fn sLog(optic: Float) Float {
                return pow(Float, 10.0, ((optic - 0.616596 - 0.03) / 0.432699)) - 0.037584;
            }
        };
    };
}

fn inexact(comptime name: []const u8, f: anytype, x: anytype, ff: anytype) void {
    if (@inComptime()) {
        @compileLog("transfer function " ++ name ++ " produces incorrect results", f, x, ff);
    } else {
        std.debug.print("transfer function " ++ name ++ " produces incorrect results {d} -> {d} -> {d}\n", .{ f, x, ff });
    }
}

test "transfer fns are exact" {
    const highest_desired_bit_depth = 16;
    const nb_vals_at_depth = @as(comptime_float, 1 << highest_desired_bit_depth);
    // TODO: add f16 and f128 once those are implemented
    inline for (.{ f64, f32 }) |Float| {
        const tf = TransferFns(Float);
        inline for (@typeInfo(tf.oetf).Struct.decls) |field| {
            const oetf = @field(tf.oetf, field.name);
            const eotf = @field(tf.eotf, field.name);
            for (0..nb_vals_at_depth) |v| {
                const f = @as(Float, @floatFromInt(v)) / nb_vals_at_depth;
                const ff = eotf(oetf(f));
                if (!std.math.approxEqAbs(Float, f, ff, 1.0 / nb_vals_at_depth)) {
                    inexact(field.name, f, eotf(f), ff);
                    break;
                }
            }
        }
    }
}
