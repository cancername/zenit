const std = @import("std");
const types = @import("types.zig");
const image = @import("image.zig");

pub const TemporalResolution = union(enum) {
    variable,
    fixed: types.Rational64,
};

pub const StreamFlags = packed struct {
    is_vfr: bool,
};
