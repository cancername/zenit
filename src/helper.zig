const std = @import("std");
const assert = std.debug.assert;
const zenit = @import("zenit.zig");

pub fn PlanarFrameHelper(comptime T: type) type {
    return struct {
        planes: [4][]T,
        widths: [4]usize,
        nb_planes: u2,
        rows: usize,
        columns: usize,
        pf: zenit.image.Format,

        const Self = @This();

        pub fn init(frame: zenit.Frame) !Self {
            assert(frame.meta.format == .image);
            const img = frame.meta.medium_data.image;
            const layout = frame.meta.format.image.layout;

            const data: [*]T = @ptrCast(frame.data.ptr);

            var widths: [4]usize = undefined;
            var planes: [4][]T = undefined;

            var nb_planes: u2 = undefined;

            // TODO: test all this

            {
                @setRuntimeSafety(true);

                const ylen = img.rows * img.columns;

                switch (layout) {
                    .xyzp_444,
                    .xyzp_10_444,
                    .xyzp_12_444,
                    .xyzp_16_444,
                    => {
                        const ylen2 = ylen * 2;
                        nb_planes = 3;
                        planes[0..3].* = .{ data[0..ylen], data[ylen..][0..ylen], data[ylen2..][0..ylen] };
                        widths[0..3].* = .{ img.columns, img.columns, img.columns };
                    },

                    .xyzp_422,
                    .xyzp_10_422,
                    .xyzp_12_422,
                    .xyzp_16_422,
                    => {
                        const ylenh = @divExact(ylen, 2);
                        const colh = @divExact(img.columns, 2);
                        nb_planes = 3;
                        planes[0..3].* = .{ data[0..ylen], data[ylen..][0..ylenh], data[ylen + ylenh ..][0..ylenh] };
                        widths[0..3].* = .{ img.columns, colh, colh };
                    },

                    .xyzp_420, .xyzp_10_420, .xyzp_12_420, .xyzp_16_420 => {
                        const ylenq = @divExact(ylen, 4);
                        const colh = @divExact(img.columns, 2);
                        nb_planes = 3;
                        planes[0..3].* = .{ data[0..ylen], data[ylen..][0..ylenq], data[ylen + ylenq ..][0..ylenq] };
                        widths[0..3].* = .{ img.columns, colh, colh };
                    },

                    .xyzp_411, .xyzp_10_411, .xyzp_12_411, .xyzp_16_411 => {
                        const ylenq = @divExact(ylen, 4);
                        const colq = @divExact(img.columns, 4);
                        nb_planes = 3;
                        planes[0..3].* = .{ data[0..ylen], data[ylen..][0..ylenq], data[ylen + ylenq ..][0..ylenq] };
                        widths[0..3].* = .{ img.columns, colq, colq };
                    },

                    .xyzwp_4444 => {
                        const ylen2 = ylen * 2;
                        nb_planes = 3;
                        planes = .{ data[0..ylen], data[ylen..][0..ylen], data[ylen2..][0..ylen], data[ylen2 + ylen ..][0..ylen] };
                        widths = .{ img.columns, img.columns, img.columns, img.columns };
                    },
                    .xyzwp_4204 => {
                        const ylenq = @divExact(ylen, 4);
                        const colh = @divExact(img.columns, 2);
                        nb_planes = 3;
                        planes = .{ data[0..ylen], data[ylen..][0..ylenq], data[ylen + ylenq ..][0..ylenq], data[ylen + ylenq * 2 ..][0..ylen] };
                        widths = .{ img.columns, colh, colh, img.columns };
                    },
                    else => return error.Packed,
                }
            }

            return .{
                .widths = widths,
                .planes = planes,
                .rows = img.rows,
                .columns = img.columns,
                .pf = frame.meta.format.image,
                .nb_planes = nb_planes,
            };
        }
    };
}
