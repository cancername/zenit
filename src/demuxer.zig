const std = @import("std");
const zenit = @import("zenit.zig");

pub const Demuxer = struct {
    name: []const u8,
    v_table: *const VTable,

    pub const VTable = struct {
        probeFn: *const fn (bytes: []const u8) zenit.probe.Score,
        initFn: *const fn (z: *zenit.Context, context: *?*anyopaque, in: zenit.InputStream) anyerror!zenit.ContainerMetadata,
        demuxFn: *const fn (z: *zenit.Context, context: ?*anyopaque, in: zenit.InputStream) anyerror!zenit.Packet,
        seekFn: *const fn (z: *zenit.Context, context: ?*anyopaque, pts: zenit.Duration) anyerror!zenit.Packet,
        deinitFn: *const fn (z: *zenit.Context, context: ?*anyopaque) anyerror!void,
    };
};
