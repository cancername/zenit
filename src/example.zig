const std = @import("std");
const zenit = @import("zenit.zig");
const sdl = @import("sdl");

pub fn main() !void {
    try sdl.init(.{
        .video = true,
        .events = true,
    });
    defer sdl.quit();

    var prev_w: usize = 640;
    var prev_h: usize = 480;

    var ctx = zenit.Context.initDefault();
    defer ctx.deinit();

    try ctx.registerBuiltin();

    var in = zenit.InputStream.initFd(0);

    const bd = ctx.bitstream_decoders.items[(ctx.probe(&in) orelse return error.NoFormat).bitstream_decoder];

    const dec_data = get_data: {
        const decoder_data: ?*anyopaque, var stream_data: zenit.FrameStreamMetadata = try bd.init(&ctx, &in);
        defer stream_data.deinit(&ctx.alloc);

        if (stream_data.frame_hint) |hint| {
            if (hint.media_type != .image) return error.NotImage;
            prev_w = hint.medium_data.image.columns;
            prev_h = hint.medium_data.image.rows;
        }

        break :get_data decoder_data;
    };
    defer bd.deinit(&ctx, dec_data) catch {};

    var window = try sdl.createWindow(
        "Example",
        .{ .centered = {} },
        .{ .centered = {} },
        prev_w,
        prev_h,
        .{ .vis = .shown, .resizable = true },
    );
    defer window.destroy();

    var renderer = try sdl.createRenderer(window, null, .{ .accelerated = true });
    defer renderer.destroy();

    var texture = try sdl.createTexture(renderer, .iyuv, .static, prev_w, prev_h);
    defer texture.destroy();

    var timer = std.time.Timer.start() catch @panic("tick tock");
    var first_frame = true;
    var eos = false;

    mainLoop: while (true) {
        while (if (eos) try sdl.waitEvent() else sdl.pollEvent()) |ev| {
            switch (ev) {
                .quit => break :mainLoop,
                else => {},
            }
        }

        if (eos) continue :mainLoop;

        if (!first_frame and timer.read() < std.time.ns_per_s / 30) {
            continue :mainLoop;
        }

        var update = try bd.decode(&ctx, dec_data, &in);

        switch (update) {
            .end => eos = true,
            .none => continue :mainLoop,
            .frame => |*f| {
                // TODO: color space conversion
                // TODO: anything that isn't yuv

                first_frame = false;
                defer f.free(&ctx.alloc);

                if (f.meta.media_type != .image) return error.NotImage;

                const layout = f.meta.layout.image;

                if (layout != .xyzp_420) return error.UnsupportedLayout;

                const med = f.meta.medium_data.image;

                if (med.rows != prev_h or med.columns != prev_w) {
                    prev_h = med.rows;
                    prev_w = med.columns;

                    texture.destroy();
                    texture = try sdl.createTexture(renderer, .iyuv, .static, prev_w, prev_h);
                    {
                        @setRuntimeSafety(true);
                        window.setMinimumSize(@intCast(prev_w), @intCast(prev_h));
                    }
                }

                try texture.update(f.data, prev_w, null);
                timer.reset();
            },
        }

        try renderer.copy(texture, null, null);
        renderer.present();
    }

    std.debug.print("All done!", .{});
}
