const std = @import("std");
const zenit = @import("zenit.zig");
const mem = std.mem;
const assert = std.debug.assert;

pub const Rational64 = Rational(i64);

pub fn ComptimeRational(comptime num: comptime_int, comptime den: comptime_int) type {
    return struct {
        comptime num: comptime_int = num,
        comptime den: comptime_int = den,
    };
}

pub fn parseComptimeInt(comptime buf: []const u8, comptime base: u8) comptime_int {
    // HACK
    return std.fmt.parseInt(i65535, buf, base) catch |e| @compileError("failed to parse \"" ++ buf ++ "\" as comptime_int: " ++ @errorName(e));
}

fn ComptimeRationalFromString(comptime xstr: []const u8) type {
    const sign, const str = if (std.mem.startsWith(u8, xstr, "-")) .{ -1, xstr[1..] } else .{ 1, xstr };

    if (str.len == 0) return ComptimeRational(0, 1);

    const dec_idx = (std.mem.indexOfScalar(u8, str, '.') orelse str.len - 1) + 1;

    if (dec_idx == str.len) return struct {
        comptime num: comptime_int = parseComptimeInt(str, 10),
        comptime den: comptime_int = 1,
    };

    const num = parseComptimeInt(str[0 .. dec_idx - 1], 10);
    // I sure hope this doesn't cause issues <trollface.png>
    const scale = std.math.powi(i65535, 10, str.len - dec_idx) catch |e| @compileError(@errorName(e) ++ ", what?");
    const den = parseComptimeInt(str[dec_idx..], 10);

    return ComptimeRational(num * scale + den, scale * sign);
}

pub inline fn rationalI32FromString(comptime str: []const u8) Rational(i32) {
    const crs: ComptimeRationalFromString(str) = .{};
    return .{ .num = crs.num, .den = crs.den };
}

pub fn Rational(comptime _T: type) type {
    const signedness: std.builtin.Signedness = switch (comptime @typeInfo(_T)) {
        .Int => |i| i.signedness,
        .ComptimeInt => @compileError("tried to use comptime_int with Rational, use comptimeRational instead"),
        else => @compileError("invalid type `" ++ @typeName(_T) ++ "' passed to `" ++ @typeName(@This()) ++ ".Rational'"),
    };
    _ = signedness; // autofix

    return extern struct {
        pub const T = _T;
        const Self = @This();

        num: T,
        den: T,

        pub inline fn fromInt(int: T) Self {
            return .{ .num = int, .den = 1 };
        }

        // /// Parse a string with a decimal point, base 10.
        // pub fn fromDecStr(xstr: []const u8) !Self {
        //     const sign: i2, const str = if (std.mem.startsWith(u8, xstr, "-")) .{ -1, xstr[1..] } else .{ 1, xstr };

        //     if (signedness == .unsigned and sign == -1) return error.Signed;

        //     if (str.len == 0) return zero;

        //     const dec_idx = (std.mem.indexOfScalar(u8, str, '.') orelse str.len - 1) + 1;

        //     if (dec_idx == str.len) return fromInt(try std.fmt.parseInt(T, str, 10));

        //     const num = try std.fmt.parseInt(T, str[0..dec_idx], 10);
        //     const scale = try std.math.powi(T, 10, str.len - dec_idx);
        //     const den = try std.fmt.parseInt(T, str[dec_idx..], 10);

        //     const den_scaled = try std.math.mul(T, den, scale * sign);

        //     return .{ .num = num, .den = den_scaled };
        // }

        pub inline fn eql(a: Self, b: Self) bool {
            return a.num == b.num and a.den == b.den;
        }

        pub inline fn toInt(rational: Self, comptime I: type) I {
            return @as(I, @intCast(rational.num)) / @as(I, @intCast(rational.den));
        }

        pub inline fn toFloat(rational: Self, comptime F: type) F {
            return @as(F, @floatFromInt(rational.num)) / @as(F, @floatFromInt(rational.den));
        }

        pub inline fn to(rational: Self, comptime D: type) D {
            return switch (comptime std.meta.activeTag(@typeInfo(D))) {
                .Int, .ComptimeInt => rational.toInt(D),
                .Float, .ComptimeFloat => rational.toFloat(D),
                else => @compileError("invalid type `" ++ @typeName(D) ++ "' passed to `" ++ @typeName(Self) ++ ".to'"),
            };
        }

        pub fn format(v: Self, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            return writer.print("{d}:{d}", .{ v.num, v.den });
        }

        pub fn parse(field: []const u8, sep: u8) !Self {
            const idx = mem.indexOfScalar(u8, field, sep) orelse return error.MissingSep;
            return Self{
                .num = try std.fmt.parseInt(T, field[0..idx], 10),
                .den = try std.fmt.parseInt(T, field[idx + 1 ..], 10),
            };
        }

        pub fn parseDiag(field: []const u8, sep: u8, z: *zenit.Context) !Self {
            const idx = mem.indexOfScalar(u8, field, sep) orelse {
                const displayed_buf = field[0..@min(64, field.len)];
                try z.diag(
                    .warn,
                    .glue,
                    error.MissingSep,
                    "The rational number string \"{s}\" (possibly truncated) is missing the expected separator '{s}'.",
                    .{ std.fmt.fmtSliceEscapeLower(displayed_buf), std.fmt.fmtSliceEscapeLower(@as([]const u8, &.{sep})) },
                );
            };
            return Self{
                .num = try zenit.util.parseIntDiag(T, field[0..idx], 10, z),
                .den = try zenit.util.parseIntDiag(T, field[idx + 1 ..], 10, z),
            };
        }

        pub const one = @This(){ .num = 1, .den = 1 };
        pub const zero = @This(){ .num = 0, .den = 1 };
    };
}

pub const BitsInterpretation = enum {
    float,
    unsigned,
    signed,
};

pub const Duration = struct {
    nsecs: u64,

    const time = std.time;
    const fmt = std.fmt;

    /// Units of time.
    const Unit = enum(u64) {
        nanoseconds = 1,
        microseconds = time.ns_per_us,
        milliseconds = time.ns_per_ms,
        seconds = time.ns_per_s,
        minutes = time.ns_per_min,
        hours = time.ns_per_hour,

        pub fn fromByte(c: u8) ?Unit {
            return switch (c) {
                'n' => .nanoseconds,
                'u' => .microseconds,
                'm' => .milliseconds,
                's' => .seconds,
                'M' => .minutes,
                'h' => .hours,
                else => null,
            };
        }
    };

    pub fn formatDecimalTrunc(data: Duration, comptime fmtstr: []const u8, options: fmt.FormatOptions, writer: anytype) !void {
        comptime var rest: []const u8 = undefined;
        const unit: Unit = comptime if (fmtstr.len == 0) get_unit: {
            rest = "";
            break :get_unit .seconds;
        } else get_unit: {
            rest = fmtstr[1..];
            break :get_unit Unit.fromByte(fmtstr[0]) orelse {
                rest = fmtstr;
                break :get_unit .seconds;
            };
        };

        try fmt.formatIntValue(data.nsecs / @intFromEnum(unit), rest, options, writer);
    }

    pub fn formatDecimal(data: Duration, comptime fmtstr: []const u8, options: fmt.FormatOptions, writer: anytype) !void {
        comptime var rest: []const u8 = undefined;
        const unit: Unit = comptime if (fmtstr.len == 0) get_unit: {
            rest = "";
            break :get_unit .seconds;
        } else get_unit: {
            rest = fmtstr[1..];
            break :get_unit Unit.fromByte(fmtstr[0]) orelse {
                rest = fmtstr;
                break :get_unit .seconds;
            };
        };

        try fmt.formatIntValue(data.nsecs / @intFromEnum(unit), rest, options, writer);
        try writer.writeByte('.');
        try fmt.formatIntValue(data.nsecs % @intFromEnum(unit), rest, options, writer);
    }

    // TODO: There is a bug in this function, causing it to print less decimals than desired.
    pub fn formatSexagesimal(data: Duration, comptime fmtstr: []const u8, _: fmt.FormatOptions, writer: anytype) !void {
        _ = fmtstr;
        const hms = .{ .alignment = .left, .fill = '0', .width = 2 };
        const us = .{ .alignment = .left, .fill = '0', .width = 3 };

        try fmt.formatIntValue(data.nsecs / time.ns_per_hour, "d", hms, writer);
        try writer.writeByte(':');

        try fmt.formatIntValue(data.nsecs % time.ns_per_hour / time.ns_per_min, "d", hms, writer);
        try writer.writeByte(':');

        try fmt.formatIntValue(data.nsecs % time.ns_per_hour % time.ns_per_min / time.ns_per_s, "d", hms, writer);
        try writer.writeByte('.');

        try fmt.formatIntValue(data.nsecs % time.ns_per_hour % time.ns_per_min % time.ns_per_s / time.ns_per_us, "d", us, writer);
    }

    pub const format = formatDecimal;
};

pub const KVOptions = struct {
    pub const Entry = union(enum) {
        string: []u8,
        signed: i64,
        unsigned: u64,
        float: f64,
        rational: Rational64,
        array: []Entry,
        frame: zenit.Frame,

        pub fn dupe(e: Entry, ally: mem.Allocator) mem.Allocator.Error!Entry {
            return switch (e) {
                .string => |s| .{ .string = try ally.dupe(u8, s) },
                .array => |a| b: {
                    const new_a = try ally.alloc(Entry, a.len);

                    errdefer ally.free(new_a);

                    var last_i: usize = 0;
                    errdefer for (new_a[0..last_i]) |ent| ent.free(ally);

                    for (new_a, a) |*d, ent| {
                        d.* = try ent.dupe(ally);
                        last_i += 1;
                    }
                    break :b .{ .array = new_a };
                },
                .frame => |*f| .{ .frame = try @constCast(f).dupeWithAllocator(ally) },
                else => e,
            };
        }

        pub fn free(e: Entry, ally: mem.Allocator) void {
            switch (e) {
                .string => |s| ally.free(s),
                .array => |a| {
                    for (a) |ent| ent.free(ally);
                    ally.free(a);
                },
                .frame => |f| {
                    var t = f;
                    t.freeWithAllocator(ally);
                },
                else => {},
            }
        }
    };

    hash_map: std.StringHashMapUnmanaged(Entry) = .{},

    pub fn deinit(opts: *KVOptions, alloc: *zenit.AllocStrat) void {
        return deinitWithAllocator(opts, alloc.general);
    }

    pub fn deinitWithAllocator(opts: *KVOptions, ally: std.mem.Allocator) void {
        var i = opts.hash_map.iterator();
        while (i.next()) |kv| {
            ally.free(kv.key_ptr.*);
            kv.value_ptr.free(ally);
            kv.key_ptr.* = undefined;
            kv.value_ptr.* = undefined;
        }
        opts.hash_map.deinit(ally);
    }

    /// Takes ownership of `name` and `entry`.
    pub fn putOwned(opts: *KVOptions, name: []const u8, ent: Entry, z: *zenit.Context) mem.Allocator.Error!void {
        return opts.hash_map.put(z.alloc.general, name, ent);
    }

    /// Does not take ownership of `name` and `entry`.
    pub fn putDupe(opts: *KVOptions, name: []const u8, ent: Entry, z: *zenit.Context) mem.Allocator.Error!void {
        const a = z.alloc.general;
        return opts.hash_map.put(a, try a.dupe(u8, name), try ent.dupe(a));
    }

    /// Does not take ownership of `name`, takes ownership of `entry`.
    pub fn putDupeKey(opts: *KVOptions, name: []const u8, ent: Entry, z: *zenit.Context) mem.Allocator.Error!void {
        const a = z.alloc.general;
        return opts.hash_map.put(a, try a.dupe(u8, name), ent);
    }

    pub fn delete(opts: *KVOptions, name: []const u8, z: *zenit.Context) !void {
        const kv = opts.hash_map.fetchRemove(name) orelse return;
        z.alloc.general.free(kv.key);
        kv.value.free(z.alloc.general);
    }
};

pub const FastFloat = if (@bitSizeOf(usize) > 32) f64 else f32;
