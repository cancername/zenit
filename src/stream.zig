const std = @import("std");
const zenit = @import("zenit.zig");
const assert = std.debug.assert;
const root = @import("root");

// TODO: change this to match reality
pub const have_pread: bool = true;
pub const want_pread: bool = have_pread and if (@hasDecl(root, "zenit_want_pread")) root.zenit_want_pread else true;

pub const buffer_size: comptime_int = if (@hasDecl(root, "zenit_buffer_size")) root.zenit_buffer_size else 8 << 10;

pub const AnySeeker = struct {
    context: ?*anyopaque,
    seekToFn: *const fn (self: ?*anyopaque, pos: u64) anyerror!void,
    seekByFn: *const fn (self: ?*anyopaque, amt: i64) anyerror!void,
    getEndPosFn: ?*const fn (self: ?*anyopaque) anyerror!u64,
    getPosFn: *const fn (self: ?*anyopaque) anyerror!u64,

    pub inline fn seekTo(self: AnySeeker, pos: u64) anyerror!void {
        return self.seekToFn(self.context, pos);
    }

    pub inline fn seekBy(self: AnySeeker, off: i64) anyerror!void {
        return self.seekByFn(self.context, off);
    }

    pub inline fn getEndPos(self: AnySeeker) anyerror!u64 {
        return (self.getEndPosFn orelse return error.Unsupported)(self.context);
    }

    pub inline fn getPos(self: AnySeeker) anyerror!u64 {
        return self.getPosFn(self.context);
    }
};

/// A seek operation.
pub const Seek = union(enum) {
    /// Offset from the start of the stream.
    set: u64,
    /// Offset from the current position.
    cur: i64,
    /// Offset from the end of the stream.
    end: u64,
};

/// Provides buffering and seeking.
pub const InputStream = struct {
    const Buffer = std.ArrayListUnmanaged(u8);

    pub const Backing = union(enum) {
        fd: std.os.fd_t,
        pread_fd: std.os.fd_t,
        bytes: [*]const u8,
        mapped_file: [*]align(std.mem.page_size) const u8,
        custom: struct {
            reader: std.io.AnyReader,
            seeker: ?AnySeeker,
        },
    };

    pos: u64 = 0,
    buffer: std.BoundedArray(u8, buffer_size) = .{},
    backing: Backing,
    size: ?u64 = null,

    /// Does not take ownership of `fd`. **`fd` must not be used by other code until the returned
    /// `InputStream` is invalidated.**
    pub fn initFd(fd: std.os.fd_t) InputStream {
        return if (std.os.lseek_CUR_get(fd)) |pos| .{
            .backing = if (want_pread) .{ .pread_fd = fd } else .{ .fd = fd },
            .pos = pos,
        } else |_| b: {
            //                 F_SETPIPE_SZ  lol, this is so cringe
            _ = std.os.linux.fcntl(fd, 1031, 1048576);
            break :b .{
                .backing = .{ .fd = fd },
            };
        };
    }

    /// Does not take ownership of `buf`.
    pub fn initBytes(buf: []const u8) InputStream {
        return .{
            .backing = .{ .bytes = buf.ptr },
            .size = buf.len,
        };
    }

    /// Does not take ownership of `fd`. `fd` may be used by other code.
    pub fn initMap(fd: std.os.fd_t, len: usize, off: usize) !InputStream {
        const data = try std.os.mmap(null, len, std.os.PROT.READ, .{ .TYPE = .PRIVATE }, fd, off);
        assert(data.len >= len);
        return .{
            .backing = .{ .mapped_file = data.ptr },
            .size = len,
        };
    }

    /// **`reader` and `seeker` must not be used by other code until the returned `InputStream` is
    /// invalidated.**
    pub inline fn initReader(reader: std.io.AnyReader, seeker: AnySeeker) InputStream {
        return .{
            .backing = .{ .custom = .{ .reader = reader, .seeker = seeker } },
            .pos = if (seeker) |s| s.getPos() catch 0 else 0,
        };
    }

    /// If possible (`.fd`, `.mapped_file`), closes the underlying resource.
    pub fn close(self: *InputStream) error{Unclosable}!void {
        return switch (self.backing) {
            .bytes, .custom => error.Unclosable,
            .fd, .pread_fd => |fd| std.os.close(fd),
            .mapped_file => |m| {
                std.os.munmap(m[0..self.size.?]);
            },
        };
    }

    /// Invalidates `self`.
    pub fn deinit(self: *InputStream) void {
        self.* = undefined;
    }

    pub fn anyReader(self: *InputStream) std.io.AnyReader {
        return switch (self.backing) {
            else => .{
                .context = @ptrCast(self),
                .readFn = readFnForReaders,
            },
        };
    }

    // HACK: API contrivance requires const on context here.
    fn readFnForReaders(context: *const anyopaque, buffer: []u8) anyerror!usize {
        return @as(*InputStream, @ptrCast(@alignCast(@constCast(context)))).read(buffer);
    }

    pub fn genericReader(self: *InputStream) std.io.GenericReader(*anyopaque, anyerror, readFnForReaders) {
        return .{ .context = @ptrCast(self) };
    }

    const DupeError = error{ Undupeable, ProcessFdQuotaExceeded, Unexpected } || std.os.MMapError;

    pub fn dupe(self: *const InputStream) !InputStream {
        return switch (self.backing) {
            .custom, .bytes => error.Undupeable,
            .fd, .pread_fd => |fd| .{
                .backing = .{ .fd = try std.os.dup(fd) },
                .pos = self.pos,
                .size = self.size,
                .buffer = self.buffer,
            },
            .mapped_file => |m| r: {
                const new_map = try std.os.mmap(
                    null,
                    self.size.?,
                    std.os.PROT.READ,
                    .{ .TYPE = .PRIVATE, .ANONYMOUS = true },
                    -1,
                    0,
                );

                @memcpy(new_map[0..self.size.?], m[0..self.size.?]);

                break :r .{
                    .backing = .{ .mapped_file = new_map.ptr },
                    .pos = self.pos,
                    .size = self.size,
                    .buffer = self.buffer,
                };
            },
        };
    }

    pub fn isBuffered(self: *const InputStream) bool {
        return switch (self.backing) {
            .fd, .pread_fd, .custom => true,
            .bytes, .mapped_file => false,
        };
    }

    pub fn isDupeable(self: *const InputStream) bool {
        return switch (self.backing) {
            .fd, .pread_fd, .mapped_file => true,
            .bytes, .custom => false,
        };
    }

    fn advancePos(self: *InputStream, len: u64) void {
        self.pos +|= len;
        if (self.size) |s| {
            if (self.pos > s) self.pos = s;
        }
    }

    fn advancePosInBuffer(self: *InputStream, len: u64) void {
        // TODO: replace this with a buffer that has a start for 🚀
        self.buffer.replaceRange(0, len, &.{}) catch unreachable;
        self.advancePos(len);
    }

    pub fn read(self: *InputStream, buf: []u8) anyerror!usize {
        if (!self.isBuffered() or (self.buffer.len == 0 and buf.len >= buffer_size)) {
            const nb_read = try self.readUnbuffered(buf);

            self.advancePos(nb_read);

            return nb_read;
        }

        try self.ensureBuffer();

        const len = @min(buf.len, self.buffer.len);
        @memcpy(buf[0..len], self.buffer.buffer[0..len]);
        self.advancePosInBuffer(len);

        return len;
    }

    /// Whether the underlying positions of `.fd` and `.custom` are synchronized with self.pos.
    pub fn isPositionInSync(self: *InputStream) bool {
        return switch (self.backing) {
            .fd,
            => |fd| (std.os.lseek_CUR_get(fd) catch self.pos) == self.pos,
            .custom => |custom| (if (custom.seeker) |s| s.getPos() catch self.pos else self.pos) == self.pos,
            // These just use `self.pos.
            .mapped_file, .bytes, .pread_fd => true,
        };
    }

    // marked inline because small and basically just a wrapper
    inline fn ensureBuffer(self: *InputStream) anyerror!void {
        return if (self.isBuffered() and self.buffer.len == 0) self.fillBuffer();
    }

    // marked inline because small and basically just a wrapper
    inline fn fillBuffer(self: *InputStream) anyerror!void {
        assert(self.isBuffered() and self.buffer.len == 0);
        const nb_read = try self.readUnbuffered(self.buffer.buffer[0..]);
        self.buffer.len = @intCast(nb_read);
    }

    fn readUnbuffered(self: *InputStream, buf: []u8) anyerror!usize {
        // expensive
        if (@import("builtin").mode == .Debug) assert(self.isPositionInSync());
        return switch (self.backing) {
            .pread_fd => |fd| std.os.pread(fd, buf, self.pos),
            .fd => |fd| std.os.read(fd, buf),
            .custom => |custom| custom.reader.read(buf),
            .mapped_file, .bytes => |m| copy: {
                const len: usize = @intCast(@min(buf.len, self.size.? - self.pos));
                @memcpy(buf[0..len], m[self.pos..][0..len]);
                break :copy len;
            },
        };
    }

    /// Returns up to len bytes of whatever data is available. The lifetime of the contents of the
    /// returned slice ends with the next call to any member function of `self`.
    pub fn peek(self: *InputStream, len: usize) anyerror![]const u8 {
        switch (self.backing) {
            .fd, .custom, .pread_fd => {
                try self.ensureBuffer();
                const sl = self.buffer.constSlice();
                return sl[0..@min(len, sl.len)];
            },
            .bytes => |b| {
                const sl = b[self.pos..self.size.?];
                return sl[0..@min(len, sl.len)];
            },
            .mapped_file => |m| {
                const b = m;
                const sl = b[self.pos..self.size.?];
                return sl[0..@min(len, sl.len)];
            },
        }
    }

    fn skipBytes(self: *InputStream, nb_bytes: u64) anyerror!void {
        // TODO
        var i: u64 = 0;
        while (i < nb_bytes) : (i += 1) _ = try self.readByte();
    }

    pub fn seek(self: *InputStream, op: Seek) anyerror!void {
        if (self.isBuffered() and (op == .cur and op.cur > 0 and op.cur < self.buffer.len)) {
            // relative seek within buffer
            try self.buffer.replaceRange(0, @intCast(op.cur), &.{});
            self.pos +|= @intCast(op.cur);
            return;
        }

        // TODO: absolute seeks within buffer

        switch (self.backing) {
            .fd => |fd| switch (op) {
                .set => |pos| {
                    try std.os.lseek_SET(fd, pos);
                    self.pos = pos;
                },
                .cur => |off| {
                    std.os.lseek_CUR(fd, off) catch |err| switch (err) {
                        error.Unseekable => |e| return if (off > 0) self.skipBytes(@intCast(off)) else e,
                        else => |e| return e,
                    };
                    self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off));
                },
                .end => |eof_off| {
                    try std.os.lseek_END(fd, @intCast(eof_off));
                    if (self.size == null) {
                        self.size = try std.os.lseek_CUR_get(fd);
                    }
                    self.pos = if (eof_off < 0) self.size.? -| @abs(eof_off) else self.size.? +| @as(u64, @intCast(eof_off));
                },
            },
            .bytes, .mapped_file, .pread_fd => return self.seekSetPos(op),
            .custom => |custom| {
                if (custom.seeker == null) return error.Unseekable;
                switch (op) {
                    .set => |pos| {
                        try custom.seeker.?.seekTo(pos);
                        self.pos = pos;
                    },
                    .cur => |off| {
                        try custom.seeker.?.seekBy(off);
                        self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off));
                    },
                    .end => |eof_off| {
                        if (self.size == null) {
                            self.size = try custom.seeker.?.getEndPos();
                        }
                        self.pos = if (eof_off < 0) self.size.? -| @abs(eof_off) else self.size.? +| @as(u64, @intCast(eof_off));
                        try custom.seeker.?.seekTo(self.pos);
                    },
                }
            },
        }
    }

    fn seekSetPos(self: *InputStream, op: Seek) anyerror!void {
        switch (op) {
            .set => |pos| self.pos = pos,
            .cur => |off| self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off)),
            .end => |end_off| self.pos = if (end_off < 0) self.size.? -| @abs(end_off) else self.size.? +| end_off,
        }
        if (self.pos > self.size.?) self.pos = self.size.?;
    }

    pub fn readAtLeast(self: *InputStream, buf: []u8, len: usize) anyerror!usize {
        std.debug.assert(len <= buf.len);

        var index: usize = 0;
        while (index < len) {
            const amt = try self.read(buf[index..]);
            if (amt == 0) break;
            index += amt;
        }
        return index;
    }

    pub fn readAll(self: *InputStream, buf: []u8) anyerror!usize {
        return self.readAtLeast(buf, buf.len);
    }

    pub fn readNoEof(self: *InputStream, buf: []u8) anyerror!void {
        if (try self.readAll(buf) != buf.len) return error.EndOfStream;
    }

    // marked inline to prevent bloat from monomorphization based on len parameter and copy on return
    pub inline fn readBytesNoEof(self: *InputStream, comptime len: usize) anyerror![len]u8 {
        var buf: [len]u8 = undefined;
        try self.readNoEof(&buf);
        return buf;
    }

    pub fn readByte(self: *InputStream) anyerror!u8 {
        var b: [1]u8 = undefined;
        if (try self.read(&b) != 1) return error.EndOfStream;
        return b[0];
    }

    pub fn readUntilDelimiter(self: *InputStream, buf: []u8, delim: u8) anyerror!usize {
        switch (self.backing) {
            .bytes, .mapped_file => |b| {
                const remaining = b[self.pos..self.size.?];
                const sl = remaining[0..@min(buf.len, remaining.len)];
                const delim_idx = std.mem.indexOfScalar(u8, sl, delim) orelse return error.NoDelimiterFound;
                const delim_slice = sl[0 .. delim_idx + 1];
                @memcpy(buf[0..delim_slice.len], delim_slice);
                self.advancePos(delim_slice.len);
                return delim_slice.len;
            },
            .fd, .pread_fd, .custom => {
                const index = std.mem.indexOfScalar(u8, self.buffer.constSlice(), delim) orelse {
                    // TODO: optimize
                    var i: usize = 0;
                    while (i < buf.len) : (i += 1) {
                        const byte = try self.readByte();
                        buf[i] = byte;
                        if (byte == delim) break;
                    } else {
                        return error.NoDelimiterFound;
                    }
                    return i + 1;
                };
                if (index > buf.len) return error.NoDelimiterFound;
                _ = self.readAll(buf[0 .. index + 1]) catch unreachable;
                return index + 1;
            },
        }
    }

    pub fn readUntilDelimiters(self: *InputStream, buf: []u8, delims: []const u8) anyerror!usize {
        switch (self.backing) {
            .bytes, .mapped_file => |b| {
                const remaining = b[self.pos..self.size.?];
                const sl = remaining[0..@min(buf.len, remaining.len)];
                const delim_idx = std.mem.indexOfAny(u8, sl, delims) orelse return error.NoDelimiterFound;
                const delim_slice = sl[0 .. delim_idx + 1];
                @memcpy(buf[0..delim_slice.len], delim_slice);
                self.advancePos(delim_slice.len);
                return delim_slice.len;
            },
            .fd, .pread_fd, .custom => {
                // TODO: replace with fast search in buffer
                var i: usize = 0;
                while (i < buf.len) : (i += 1) {
                    const byte = try self.readByte();
                    buf[i] = byte;
                    for (delims) |delim| if (byte == delim) break;
                } else {
                    return error.NoDelimiterFound;
                }
                return i + 1;
            },
        }
    }

    pub fn readExpected(self: *InputStream, buf: []const u8) anyerror!bool {
        for (buf) |c| if (try self.readByte() != c) return false;
        return true;
    }

    pub fn readInt(self: *InputStream, comptime T: type, comptime endian: std.builtin.Endian) anyerror!T {
        const native_int = @as(T, @bitCast(try self.readBytesNoEof((@bitSizeOf(T) + 7) / 8)));
        return if (@import("builtin").cpu.arch.endian() != endian) @byteSwap(native_int) else native_int;
    }
};

pub const OutputStream = struct {
    pub const Backing = union(enum) {
        fd: std.os.fd_t,
        pread_fd: std.os.fd_t,
        bytes: [*]u8,
        mapped_file: [*]align(std.mem.page_size) u8,
        custom: struct {
            writer: std.io.AnyWriter,
            seeker: ?AnySeeker,
        },
    };

    pos: u64 = 0,
    // buffer: std.BoundedArray(u8, buffer_size) = .{},
    backing: Backing,
    size: ?u64 = null,

    /// Does not take ownership of `fd`. **`fd` must not be used by other code until the returned
    /// `OutputStream` is invalidated.**
    pub fn initFd(fd: std.os.fd_t) OutputStream {
        return if (std.os.lseek_CUR_get(fd)) |pos| .{
            .backing = if (want_pread) .{ .pread_fd = fd } else .{ .fd = fd },
            .pos = pos,
        } else |_| b: {
            //                 F_SETPIPE_SZ  lol, this is so cringe
            _ = std.os.linux.fcntl(fd, 1031, 1048576);
            break :b .{
                .backing = .{ .fd = fd },
            };
        };
    }

    /// Does not take ownership of `buf`.
    pub fn initBytes(buf: []u8) OutputStream {
        return .{
            .backing = .{ .bytes = buf.ptr },
            .size = buf.len,
        };
    }

    /// Does not take ownership of `fd`. `fd` may be used by other code.
    pub fn initMap(fd: std.os.fd_t, len: usize, off: usize) !OutputStream {
        const data = try std.os.mmap(null, len, std.os.PROT.WRITE, .{ .TYPE = .PRIVATE }, fd, off);
        assert(data.len >= len);
        return .{
            .backing = .{ .mapped_file = data.ptr },
            .size = len,
        };
    }

    /// **`writer` and `seeker` must not be used by other code until the returned `OutputStream` is
    /// invalidated.**
    pub inline fn initWriter(writer: std.io.AnyWriter, seeker: AnySeeker) OutputStream {
        return .{
            .backing = .{ .custom = .{ .writer = writer, .seeker = seeker } },
            .pos = if (seeker) |s| s.getPos() catch 0 else 0,
        };
    }

    /// If possible (`.fd`, `.mapped_file`), closes the underlying resource.
    pub fn close(self: *OutputStream) error{Unclosable}!void {
        return switch (self.backing) {
            .bytes, .custom => error.Unclosable,
            .fd, .pread_fd => |fd| std.os.close(fd),
            .mapped_file => |m| {
                std.os.munmap(m[0..self.size.?]);
            },
        };
    }

    /// Invalidates `self`.
    pub fn deinit(self: *OutputStream) void {
        self.* = undefined;
    }

    // massive TODO

    // this code from above
    pub fn seek(self: *OutputStream, op: Seek) anyerror!void {
        switch (self.backing) {
            .fd => |fd| switch (op) {
                .set => |pos| {
                    try std.os.lseek_SET(fd, pos);
                    self.pos = pos;
                },
                .cur => |off| {
                    std.os.lseek_CUR(fd, off) catch |err| switch (err) {
                        else => |e| return e,
                    };
                    self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off));
                },
                .end => |eof_off| {
                    try std.os.lseek_END(fd, @intCast(eof_off));
                    if (self.size == null) {
                        self.size = try std.os.lseek_CUR_get(fd);
                    }
                    self.pos = if (eof_off < 0) self.size.? -| @abs(eof_off) else self.size.? +| @as(u64, @intCast(eof_off));
                },
            },
            .bytes, .mapped_file, .pread_fd => return self.seekSetPos(op),
            .custom => |custom| {
                if (custom.seeker == null) return error.Unseekable;
                switch (op) {
                    .set => |pos| {
                        try custom.seeker.?.seekTo(pos);
                        self.pos = pos;
                    },
                    .cur => |off| {
                        try custom.seeker.?.seekBy(off);
                        self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off));
                    },
                    .end => |eof_off| {
                        if (self.size == null) {
                            self.size = try custom.seeker.?.getEndPos();
                        }
                        self.pos = if (eof_off < 0) self.size.? -| @abs(eof_off) else self.size.? +| @as(u64, @intCast(eof_off));
                        try custom.seeker.?.seekTo(self.pos);
                    },
                }
            },
        }
    }

    fn seekSetPos(self: *OutputStream, op: Seek) anyerror!void {
        switch (op) {
            .set => |pos| self.pos = pos,
            .cur => |off| self.pos = if (off < 0) self.pos -| @abs(off) else self.pos +| @as(u64, @intCast(off)),
            .end => |end_off| self.pos = if (end_off < 0) self.size.? -| @abs(end_off) else self.size.? +| end_off,
        }
        if (self.pos > self.size.?) self.pos = self.size.?;
    }

    pub fn writeUnbuffered(self: *OutputStream, buf: []const u8) anyerror!usize {
        return switch (self.backing) {
            .pread_fd => |fd| std.os.pwrite(fd, buf, self.pos),
            .fd => |fd| std.os.write(fd, buf),
            .custom => |custom| custom.writer.write(buf),
            .mapped_file, .bytes => |m| copy: {
                const len: usize = @intCast(@min(buf.len, self.size.? - self.pos));
                @memcpy(m[self.pos..][0..len], buf[0..len]);
                break :copy len;
            },
        };
    }

    fn advancePos(self: *OutputStream, len: u64) void {
        self.pos +|= len;
        if (self.size) |s| {
            if (self.pos > s) self.pos = s;
        }
    }

    pub fn write(self: *OutputStream, buf: []const u8) anyerror!usize {
        // TODO
        const len = try self.writeUnbuffered(buf);
        self.advancePos(len);
        return len;
    }

    pub fn writeAll(self: *OutputStream, buf: []const u8) anyerror!void {
        var i: usize = 0;
        while (i != buf.len) {
            i += try self.write(buf[i..]);
        }
    }

    pub fn writev(self: *OutputStream, iovecs: []const std.os.iovec_const) anyerror!usize {
        // TODO
        var i: usize = 0;

        for (iovecs) |iovec| {
            const len = try self.write(iovec.iov_base[0..iovec.iov_len]);
            // TODO: OVERFLOW?
            i += len;
            if (len != iovec.iov_len) return i;
        }

        return i;
    }

    pub fn writevAll(self: *OutputStream, iovecs: []std.os.iovec_const) anyerror!void {
        for (iovecs) |iovec| {
            try self.writeAll(iovec.iov_base[0..iovec.iov_len]);
        }
    }
};
