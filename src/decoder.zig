const std = @import("std");
const zenit = @import("zenit.zig");

pub const DecodeUpdate = union(enum) {
    none,
    frame: zenit.Frame,
    end,
};

pub const BitstreamDecoder = struct {
    name: []const u8,
    v_table: *const VTable,

    pub const VTable = struct {
        probeFn: *const fn (bytes: []const u8) zenit.probe.Score,
        initFn: *const fn (z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata,
        deinitFn: *const fn (z: *zenit.Context, data: ?*anyopaque) anyerror!void,
        decodeFn: *const fn (z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!DecodeUpdate,
    };

    pub fn probe(bd: BitstreamDecoder, bytes: []const u8) zenit.probe.Score {
        return bd.v_table.probeFn(bytes);
    }

    pub fn init(bd: BitstreamDecoder, z: *zenit.Context, in: *zenit.InputStream) anyerror!struct { ?*anyopaque, zenit.FrameStreamMetadata } {
        var d: ?*anyopaque = undefined;
        const sm = try bd.v_table.initFn(z, &d, in);
        return .{ d, sm };
    }

    pub fn deinit(bd: BitstreamDecoder, z: *zenit.Context, data: ?*anyopaque) anyerror!void {
        return bd.v_table.deinitFn(z, data);
    }

    pub fn decode(bd: BitstreamDecoder, z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!DecodeUpdate {
        return bd.v_table.decodeFn(z, data, in);
    }
};

pub const PacketDecoder = struct {
    name: []const u8,
    v_table: *const VTable,

    pub const VTable = struct {
        initFn: *const fn (z: *zenit.Context, data: *?*anyopaque) anyerror!zenit.FrameStreamMetadata,
        deinitFn: *const fn (z: *zenit.Context, data: ?*anyopaque) anyerror!void,
        decodeFn: *const fn (z: *zenit.Context, data: ?*anyopaque, in: zenit.InputStream) anyerror!DecodeUpdate,
    };
};
