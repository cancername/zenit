//! A bitstream decoder for the YUV4MPEG2 format, a lossless uncompressed intermediate format for
//! Y'CbCr. Format documentation can be found at <https://wiki.multimedia.cx/index.php/YUV4MPEG2>
const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "y4m",
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    return if (std.mem.startsWith(u8, bytes, "YUV4MPEG2 ")) zenit.probe.high else 0;
}

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    if ((try in.peek(1)).len == 0) return error.EndOfStream;

    var tmp_buf: [512]u8 = undefined;
    const line_len = try in.readUntilDelimiter(&tmp_buf, '\n');

    const header_p = try z.alloc.general.create(Header);
    errdefer z.alloc.general.destroy(header_p);

    header_p.* = try Header.parse(tmp_buf[0..line_len], z);
    errdefer header_p.deinit(z.alloc.general);

    // TODO: XCOLORRANGE

    header_p.meta = .{
        .dts = .{ .nsecs = 0 },
        .pts = .{ .nsecs = 0 },
        .extra_data = .{},
        .flags = .{ .is_key_frame = true, .is_pts_accurate = true },
        .medium_data = .{ .image = .{ .rows = header_p.height, .columns = header_p.width, .vfr_frame_duration = undefined } },
        .interpretation = .{ .image = header_p.format.interpretation },
        .layout = .{ .image = header_p.format.layout },
        .media_type = .image,
    };

    data.* = @ptrCast(header_p);
    return .{
        .frame_hint = header_p.meta,
        .stream = .{ .is_vfr = false },
    };
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    const p: *Header = @ptrCast(@alignCast(data));
    p.deinit(z.alloc.general);
    z.alloc.general.destroy(p);
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    if ((try in.peek(1)).len == 0) return .{ .end = {} };

    const p: *Header = @ptrCast(@alignCast(data));

    // Verify the frame header is there (and, importantly, doesn't have weird interlacing bullshit)

    const buf = try in.readBytesNoEof(6);
    if (!std.mem.eql(u8, buf[0..5], "FRAME")) try z.diag(
        .err,
        .decoder,
        error.NotAtStartOfFrame,
        "\"{s}\" is not  \"FRAME\", indicating that the input stream is not positioned at the start of a frame. The file may be corrupted, or a bug may have ocurred.",
        .{std.fmt.fmtSliceEscapeLower(buf[0..5])},
    );
    // TODO: handle frame headers
    if (buf[5] != '\n') try z.diag(
        .err,
        .decoder,
        error.UnsupportedFrameHeader,
        "There is additional data in the frame header, which is not supported by this implementation.",
        .{},
    );

    // Set presentation timestamp to correct value according to frame rate.

    var meta = p.meta;
    {
        // TODO
        @setRuntimeSafety(true);
        meta.pts = .{ .nsecs = @intCast(@divTrunc((p.framerate.num *% p.index), p.framerate.den)) };
    }

    p.index +%= 1;

    var frame = try zenit.Frame.alloc(&z.alloc, meta);
    errdefer frame.free(&z.alloc);

    try in.readNoEof(frame.data);

    // Convert to host endianness if required.

    if (comptime @import("builtin").cpu.arch.endian() != .little) {
        switch (frame.meta.medium_data) {
            .yuv444p, .yuv420p, .yuv422p, .yuv411p => {},
            .yuv444p10,
            .yuv420p10,
            .yuv422p10,
            .yuv411p10,
            .yuv444p12,
            .yuv420p12,
            .yuv422p12,
            .yuv411p12,
            .yuv444p16,
            .yuv420p16,
            .yuv422p16,
            .yuv411p16,
            => {
                // TODO: replace with a simple ptrCast when it is implemented.
                const data_int_p: [*]align(zenit.Frame.alignment) u16 = @ptrCast(frame.data.ptr);
                const data_len = @divExact(frame.data.len, @sizeOf(u16));
                const data_ints = data_int_p[0..data_len];
                for (data_ints) |*x| x.* = @byteSwap(x.*);
            },
            else => unreachable,
        }
    }

    return .{ .frame = frame };
}

// TODO: diagnostics
// TODO: do something with the extendeds

const Header = struct {
    const Interlacing = enum(u8) {
        progressive = 'p',
        top = 't',
        bottom = 'b',
        mixed = 'm',

        pub fn parse(interlacing: []const u8) !Interlacing {
            if (interlacing.len != 1) return error.BadInterlacingLength;
            return std.meta.intToEnum(Interlacing, interlacing[0]);
        }
    };

    const Param = union(enum) {
        width: u64,
        height: u64,
        interlacing: Interlacing,
        framerate: zenit.Rational64,
        aspect: zenit.Rational64,
        pixel_format: zenit.image.Format,
        extended: []u8,

        fn parse(param: []const u8, z: *zenit.Context) !Param {
            const allocator = z.alloc.general;

            if (param.len < 2) try z.diag(.err, .decoder, error.ParamTooShort, "The {d}-byte parameter passed was too short.", .{param.len});

            const v = param[1..];

            const IF = zenit.image.Format;

            const SampleFormatMap = std.ComptimeStringMap(IF, .{
                // for a discussion of these, see <https://wiki.multimedia.cx/index.php/YUV4MPEG2>
                .{ "420jpeg", IF.bt_709_420_jpeg_8 },
                .{ "411", IF.bt_709_411_8 },
                .{ "420", IF.bt_709_420_8 },
                // TODO
                .{ "420mpeg2", IF.bt_709_420_8 },
                // .{ "422", IF.bt_709_42 },
                .{ "444", IF.bt_709_444_8 },
                // .{ "411p10", .yuv411p10 },
                // .{ "420p10", .yuv420p10 },
                // .{ "422p10", .yuv422p10 },
                // .{ "444p10", .yuv444p10 },
                // .{ "411p12", .yuv411p12 },
                // .{ "420p12", .yuv420p12 },
                // .{ "422p12", .yuv422p12 },
                // .{ "444p12", .yuv444p12 },
                // .{ "411p16", .yuv411p16 },
                // .{ "420p16", .yuv420p16 },
                // .{ "422p16", .yuv422p16 },
                // .{ "444p16", .yuv444p16 },
                // .{ "444alpha", .yuva444p },
                // .{ "420alpha", .yuva420p },
                // .{ "mono", .gray },
                // .{ "mono10", .gray10 },
                // .{ "mono12", .gray12 },
                // .{ "mono16", .gray16 },
            });

            return switch (param[0]) {
                'W' => .{ .width = try zenit.util.parseUnsignedDiag(u64, v, 10, z) },
                'H' => .{ .height = try zenit.util.parseUnsignedDiag(u64, v, 10, z) },
                'I' => .{ .interlacing = Interlacing.parse(v) catch |e| try z.diag(.err, .decoder, e, "The interlacing field is invalid.", .{}) },
                'F' => .{ .framerate = try zenit.Rational64.parseDiag(v, ':', z) },
                'A' => .{ .aspect = try zenit.Rational64.parseDiag(v, ':', z) },
                'C' => .{ .pixel_format = SampleFormatMap.get(v) orelse {
                    const displayed_v = v[0..@min(64, v.len)];
                    try z.diag(
                        .err,
                        .decoder,
                        error.UnknownSampleFormat,
                        "The sample format \"{s}\" is unknown.",
                        .{std.fmt.fmtSliceEscapeLower(displayed_v)},
                    );
                } },
                'X' => .{ .extended = try allocator.dupe(u8, v) },
                else => try z.diag(
                    .err,
                    .decoder,
                    error.UnknownParam,
                    "The parameter byte \'{s}\' is unknown.",
                    .{std.fmt.fmtSliceEscapeLower(&.{param[0]})},
                ),
            };
        }
    };

    pub fn deinit(h: *Header, allocator: std.mem.Allocator) void {
        for (h.extended) |extended| allocator.free(extended);
        if (h.extended.len != 0) allocator.free(h.extended);
    }

    pub fn parse(buffer: []const u8, z: *zenit.Context) !Header {
        const allocator = z.alloc.general;

        if (buffer.len < 10) try z.diag(
            .err,
            .decoder,
            error.HeaderTooSmall,
            "The header \"{s}\" is too small.",
            .{std.fmt.fmtSliceEscapeLower(buffer)},
        );

        var reader_state = std.io.fixedBufferStream(buffer);
        const reader = reader_state.reader();

        var header: Header = .{
            .width = undefined,
            .height = undefined,
            .framerate = undefined,
            .interlacing = .progressive,
            .aspect = null,
            .extended = &.{},
        };

        var extendeds: std.ArrayListUnmanaged([]u8) = .{};
        defer extendeds.deinit(allocator);
        errdefer for (extendeds.items) |extended| allocator.free(extended);

        var header_has_field: packed struct {
            width: bool = false,
            height: bool = false,
            framerate: bool = false,
        } = .{};

        var state: enum {
            magic,
            param,
            end,
        } = .magic;

        while (true) {
            state = switch (state) {
                .magic => next: {
                    const expected_magic = "YUV4MPEG2 ";
                    var actual_magic: [10]u8 = undefined;
                    try reader.readNoEof(&actual_magic);
                    if (!std.mem.eql(u8, expected_magic, &actual_magic))
                        return error.BadMagic;
                    break :next .param;
                },
                .param => next: {
                    var buf: [64]u8 = undefined;
                    var len: usize = 0;

                    const last_byte = parse_bytes: while (reader.readByte()) |cur_byte| {
                        switch (cur_byte) {
                            'A'...'Z', 'a'...'z', '0'...'9', ':', '=' => {
                                const new_len = len + 1;
                                if (new_len > buf.len) return error.Overflow;
                                buf[len] = cur_byte;
                                len = new_len;
                            },
                            else => break :parse_bytes cur_byte,
                        }
                    } else |e| return e;

                    switch (try Param.parse(buf[0..len], z)) {
                        .width => |v| {
                            header.width = v;
                            if (header_has_field.width) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"width"});
                            header_has_field.width = true;
                        },
                        .height => |v| {
                            header.height = v;
                            if (header_has_field.height) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"height"});
                            header_has_field.height = true;
                        },
                        .framerate => |v| {
                            header.framerate = v;
                            if (header_has_field.framerate) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"framerate"});
                            header_has_field.framerate = true;
                        },
                        .interlacing => |v| header.interlacing = v,
                        .aspect => |v| header.aspect = v,
                        .pixel_format => |v| header.format = v,
                        .extended => |v| {
                            errdefer allocator.free(v);
                            try extendeds.append(allocator, v);
                        },
                    }

                    break :next switch (last_byte) {
                        ' ' => .param,
                        '\n' => .end,
                        else => return error.InvalidFile,
                    };
                },
                .end => break,
            };
        }

        if (!(header_has_field.width and header_has_field.height and header_has_field.framerate)) {
            try z.diag(.err, .decoder, error.IncompleteHeader, "One of width, height, or framerate is missing.", .{});
        }

        if (header.interlacing == .mixed) {
            try z.diag(.err, .decoder, error.IncompleteHeader, "The mixed interlacing mode is unsupported.", .{});
        }

        if (header.interlacing != null and header.interlacing.? != .progressive) {
            try z.diag(.err, .decoder, error.IncompleteHeader, "Interlacing is not yet supported.", .{});
        }

        header.extended = try extendeds.toOwnedSlice(allocator);

        return header;
    }

    width: u64,
    height: u64,
    framerate: zenit.Rational64,
    interlacing: ?Interlacing,
    aspect: ?zenit.Rational64,
    format: zenit.image.Format = zenit.image.Format.bt_709_420_8,
    extended: [][]u8 = &.{},
    index: i64 = 0,
    meta: zenit.FrameMetadata = undefined,
};
