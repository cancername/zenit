//! A bitstream decoder for the Quite OK Image format, a losslessly compressed format for
//! RGB and RGBA. Format documentation can be found at <https://qoiformat.org/qoi-specification.pdf>.
const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "qoi",
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    return if (std.mem.startsWith(u8, bytes, "qoif")) zenit.probe.high else 0;
}

const QoiState = struct {
    dec: PixelDecoder = .{},
    info: ImageInfo = undefined,
};

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    const d = try z.alloc.general.create(QoiState);
    errdefer z.alloc.general.destroy(d);
    d.* = .{};

    const info: ImageInfo = get_info: while (in.readByte()) |byte| {
        if (d.dec.pushForInfo(byte) catch |e| try z.diag(.err, .decoder, e, "Decoding header failed at position {d} (byte {x}).", .{ in.pos - 1, byte })) |info| {
            break :get_info info;
        }
    } else |err| switch (err) {
        else => |e| return e,
    };
    d.info = info;

    data.* = @ptrCast(d);

    return .{ .frame_hint = info.getMeta() };
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    const p: *QoiState = @ptrCast(@alignCast(data));
    z.alloc.general.destroy(p);
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    const p: *QoiState = @ptrCast(@alignCast(data));

    if (p.dec.pixels_left == 0) return .{ .end = {} };
    if ((try in.peek(1)).len == 0) return .{ .end = {} };

    const layout: zenit.image.Layout = switch (p.info.pix_fmt) {
        .rgb => .xyz,
        .rgba => .xyzw,
    };

    const interpretation: zenit.image.Interpretation = switch (p.info.colorspace) {
        .linear => if (layout == .xyz) zenit.image.Interpretation.s_rgb_linear else zenit.image.Interpretation.s_rgb_linear,
        .srgb => if (layout == .xyz) zenit.image.Interpretation.s_rgb else zenit.image.Interpretation.s_rgb,
    };

    var frame = try zenit.Frame.allocDiag(z, .{
        .pts = undefined,
        .dts = undefined,
        .layout = .{ .image = layout },
        .interpretation = .{ .image = interpretation },
        .media_type = .image,
        .flags = .{ .is_key_frame = true },
        .medium_data = .{ .image = .{ .rows = p.info.rows, .columns = p.info.cols, .vfr_frame_duration = .{ .nsecs = 0 } } },
    });
    errdefer frame.free(&z.alloc);

    var i: usize = 0;
    var buf: [zenit.buffer_size]u8 = undefined;
    outer: while (in.read(&buf)) |len| {
        if (len == 0) break;
        for (buf[0..len]) |byte| {
            if (try p.dec.push(byte)) {
                while (p.dec.pop()) |pr| {
                    if (i >= frame.data.len) break :outer;
                    const is_rgba = layout == .xyzw;
                    pr.p.store(frame.data[i..].ptr, is_rgba);
                    i += if (is_rgba) 4 else 3;
                }
            }
        }
    } else |err| return err;

    switch (p.info.colorspace) {
        .srgb => {
            z.diag(.warn, .decoder, {}, "Conversion from sRGB gamma to BT.709 gamma not yet implemented, frame corrupt.", .{});
            frame.meta.flags.is_corrupt = true;
        },
        .linear => {
            z.diag(.warn, .decoder, {}, "Conversion from linear to BT.709 gamma not yet implemented, frame corrupt.", .{});
            frame.meta.flags.is_corrupt = true;
        },
    }

    return .{ .frame = frame };
}

// old, slow, but cute, implementation below by me

/// Adds its second operand to first operand and returns the result,
/// wrapping values which would be out of range.
inline fn addSigned(x: anytype, y: anytype) @TypeOf(x) {
    return if (y < 0) x -% @as(@TypeOf(x), @intCast(-y)) else x +% @as(@TypeOf(x), @intCast(y));
}

test "addSigned" {
    try std.testing.expectEqual(42, addSigned(30, 12));
    try std.testing.expectEqual(42, addSigned(50, -8));
    try std.testing.expectEqual(42, addSigned(50, -8));
}

/// Represents a pixel with red, green, blue, and transparency components,
/// all ranging from 0 to 255.
const Pixel = PixelArray;

const PixelArray = struct {
    rgba: [4]u8,

    /// Stores the red, green, and blue components of `p`, in this order, in
    /// `buf`.
    inline fn storeRgb(p: Pixel, buf: *[3]u8) void {
        @memcpy(buf, p.rgba[0..3]);
    }

    /// Stores the red, green, blue, and transparency components of `p`, in
    /// this order, in `buf`.
    inline fn storeRgba(p: Pixel, buf: *[4]u8) void {
        @memcpy(buf, &p.rgba);
    }

    /// Performs either the functionality of `storeRgb` or `storeRgba`,
    /// depending on `want_alpha`.
    inline fn store(p: Pixel, buf: [*]u8, want_alpha: bool) void {
        if (want_alpha) p.storeRgba(buf[0..4]) else p.storeRgb(buf[0..3]);
    }

    /// Sets the red, green, blue values of `p` to the respective elements
    /// of `buf`.
    inline fn setRgb(p: *Pixel, buf: *const [3]u8) void {
        p.rgba = .{ buf[0], buf[1], buf[2], p.getA() };
    }

    /// Sets the red, green, blue, transparency values of `p` to the
    /// respective elements of `buf`.
    inline fn setRgba(p: *Pixel, buf: *const [4]u8) void {
        p.rgba = .{ buf[0], buf[1], buf[2], buf[3] };
    }

    /// Returns a `Pixel`, initialized with `r`, `g, `b`, `a` as its red,
    /// green, blue, and transparency components respectively.
    inline fn init(r: u8, g: u8, b: u8, a: u8) Pixel {
        return .{ .rgba = .{ r, g, b, a } };
    }

    /// Initializes the `Pixel` pointed to by `p` with `r`, `g`, `b`, `a` as
    /// its red, green, blue, and transparency components respectively.
    inline fn set(p: *Pixel, r: u8, g: u8, b: u8, a: u8) void {
        p.* = init(r, g, b, a);
    }

    /// Returns the red component of `p`.
    inline fn getR(p: Pixel) u8 {
        return p.rgba[0];
    }

    /// Returns the green component of `p`.
    inline fn getG(p: Pixel) u8 {
        return p.rgba[1];
    }

    /// Returns the blue component of `p`.
    inline fn getB(p: Pixel) u8 {
        return p.rgba[2];
    }

    /// Returns the transparency component of `p`.
    inline fn getA(p: Pixel) u8 {
        return p.rgba[3];
    }

    /// Returns the hash of `p`, ranging from 0 to 63. See the QOI specification.
    fn hash(p: Pixel) u6 {
        const r = p.getR();
        const g = p.getG();
        const b = p.getB();
        const a = p.getA();
        return @as(u6, @truncate(((r << 1) +% r) +%
            ((g << 2) +% g) +%
            ((b << 3) -% b) +%
            ((a << 3) +% (a << 1) +% a)));
    }

    /// Performs the "diff" operation on `p`, as defined in the QOI
    /// specification.
    inline fn diff(p: *Pixel, dr: i7, dg: i7, db: i7) void {
        p.rgba = .{
            addSigned(p.rgba[0], dr),
            addSigned(p.rgba[1], dg),
            addSigned(p.rgba[2], db),
            p.rgba[3],
        };
    }

    /// Returns whether `p1` and `p2` are equal.
    inline fn eql(p1: Pixel, p2: Pixel) bool {
        // oof
        return @as(u32, @bitCast(p1.rgba)) == @as(u32, @bitCast(p2.rgba));
    }
};

const PixelVector = struct {
    rgba: @Vector(4, u8),

    /// Stores the red, green, and blue components of `p`, in this order, in
    /// `buf`.
    inline fn storeRgb(p: Pixel, buf: *[3]u8) void {
        @memcpy(buf, &@as([4]u8, p.rgba)[0..3]);
    }

    /// Stores the red, green, blue, and transparency components of `p`, in
    /// this order, in `buf`.
    inline fn storeRgba(p: Pixel, buf: *[4]u8) void {
        @memcpy(buf, &@as([4]u8, p.rgba));
    }

    /// Performs either the functionality of `storeRgb` or `storeRgba`,
    /// depending on `want_alpha`.
    inline fn store(p: Pixel, buf: [*]u8, want_alpha: bool) void {
        if (want_alpha) p.storeRgba(buf[0..4]) else p.storeRgb(buf[0..3]);
    }

    /// Sets the red, green, blue values of `p` to the respective elements
    /// of `buf`.
    inline fn setRgb(p: *Pixel, buf: *const [3]u8) void {
        p.rgba = .{ buf[0], buf[1], buf[2], p.getA() };
    }

    /// Sets the red, green, blue, transparency values of `p` to the
    /// respective elements of `buf`.
    inline fn setRgba(p: *Pixel, buf: *const [4]u8) void {
        p.rgba = .{ buf[0], buf[1], buf[2], buf[3] };
    }

    /// Returns a `Pixel`, initialized with `r`, `g, `b`, `a` as its red,
    /// green, blue, and transparency components respectively.
    inline fn init(r: u8, g: u8, b: u8, a: u8) Pixel {
        return .{ .rgba = .{ r, g, b, a } };
    }

    /// Initializes the `Pixel` pointed to by `p` with `r`, `g`, `b`, `a` as
    /// its red, green, blue, and transparency components respectively.
    inline fn set(p: *Pixel, r: u8, g: u8, b: u8, a: u8) void {
        p.* = init(r, g, b, a);
    }

    /// Returns the red component of `p`.
    inline fn getR(p: Pixel) u8 {
        return p.rgba[0];
    }

    /// Returns the green component of `p`.
    inline fn getG(p: Pixel) u8 {
        return p.rgba[1];
    }

    /// Returns the blue component of `p`.
    inline fn getB(p: Pixel) u8 {
        return p.rgba[2];
    }

    /// Returns the transparency component of `p`.
    inline fn getA(p: Pixel) u8 {
        return p.rgba[3];
    }

    /// Returns the hash of `p`, ranging from 0 to 63. See the QOI specification.
    fn hash(p: Pixel) u6 {
        return @as(u6, @truncate(@reduce(.Add, p.rgba *% [4]u8{ 3, 5, 7, 11 })));
    }

    inline fn diffNV(p: *Pixel, dr: i7, dg: i7, db: i7) void {
        p.rgba = .{
            addSigned(p.rgba[0], dr),
            addSigned(p.rgba[1], dg),
            addSigned(p.rgba[2], db),
            p.rgba[3],
        };
    }

    /// Performs the "diff" operation on `p`, as defined in the QOI
    /// specification.
    inline fn diff(p: *Pixel, dr: i7, dg: i7, db: i7) void {
        // TODO
        return diffNV(p, dr, dg, db);
    }

    /// Returns whether `p1` and `p2` are equal.
    inline fn eql(p1: Pixel, p2: Pixel) bool {
        // oof
        return @as(u32, @bitCast(p1.rgba)) == @as(u32, @bitCast(p2.rgba));
    }
};

/// The implementation. Only struct and function declarations are important here.
const impl = struct {
    /// Errors that can occur while demuxing a QOI file.
    const ScanError = error{
        /// The magic number of the file was invalid.
        InvalidMagic,
        /// The colorspace of the file was invalid.
        InvalidColorspace,
        /// The number of channels of the file was invalid.
        InvalidChannels,
    };

    /// Errors that can occur while pushing an operation onto the decoder.
    const PushError = error{
        /// The file contains syntactically correct but semantically incorrect
        /// data, such as a header where there shouldn't be one, no header where
        /// there should be one, or more than one index operation in a row (TODO)
        InvalidStream,
    };

    /// Represents what the QOI specification calls a chunk.
    const Operation = union(enum) {
        /// The header, as defined in the specification.  This isn't technically
        /// an operation, but I use the scanner to read it purely for
        /// convenience.
        const Header = struct {
            /// The number of channels, as in the spec.
            const Channels = enum(u3) {
                rgb = 3,
                rgba = 4,

                /// Converts the integer `x` to a `Channels`, as defined in the
                /// spec.
                inline fn fromInt(x: anytype) ScanError!Channels {
                    return if (x == 3 or x == 4) return @enumFromInt(x) else return error.InvalidChannels;
                }

                /// Converts the `Channels` `x` to an integer, as defined in the
                /// spec.
                inline fn toInt(x: Channels) u8 {
                    return @intFromEnum(x);
                }

                /// Converts the `Channels` `self` to a `PixelFormat` in an
                /// `ImageeInfo`.
                inline fn toImageInfoPixelFormat(self: Channels) ImageInfo.PixelFormat {
                    return switch (self) {
                        .rgb => ImageInfo.PixelFormat.rgb,
                        .rgba => ImageInfo.PixelFormat.rgba,
                    };
                }
            };

            /// A colorspace, as defined in the specification.
            const Colorspace = enum(u1) {
                srgb = 0,
                linear = 1,

                /// Converts the integer `x` to a `Colorspace`, as defined by
                /// the spec.
                inline fn fromInt(x: anytype) ScanError!Colorspace {
                    return if (x < 2) @enumFromInt(x) else error.InvalidColorspace;
                }

                /// Converts the `Colorspace` `x` to an integer, as per the
                /// spec.
                inline fn toInt(x: Colorspace) u8 {
                    return @intFromEnum(x);
                }

                /// Converts the `Colorspace` `self` to a colorspace as used in
                /// an `ImageInfo`.
                inline fn toImageInfoColorspace(self: Colorspace) ImageInfo.Colorspace {
                    return switch (self) {
                        .srgb => ImageInfo.Colorspace.srgb,
                        .linear => ImageInfo.Colorspace.linear,
                    };
                }
            };

            /// How many rows the image represented by this file has (see spec)
            height: u32 = 0,

            /// How many columns the image represented by this file has (see
            /// spec)
            width: u32 = 0,

            /// How many channels the image represented by this file has (see spec)
            channels: Channels = .rgb,

            /// The colorspace the image represented by this file has (see spec)
            colorspace: Colorspace = .srgb,

            /// Converts the header to an `ImageInfo`. Used by the interfaces.
            inline fn toImageInfo(self: Header) ImageInfo {
                return .{
                    .rows = self.height,
                    .cols = self.width,
                    .pix_fmt = self.channels.toImageInfoPixelFormat(),
                    .colorspace = self.colorspace.toImageInfoColorspace(),
                };
            }
        };

        /// Red, green, and blue components. The transparency component is left untouched.
        const Rgb = struct {
            r: u8 = 0,
            g: u8 = 0,
            b: u8 = 0,
        };

        /// All components.
        const Rgba = struct {
            r: u8 = 0,
            g: u8 = 0,
            b: u8 = 0,
            a: u8 = 0,
        };

        /// An index into the `seen` array of the decoder.
        const Index = struct {
            idx: u6 = 0,
        };

        /// A difference to the previous pixel.
        const Diff = struct {
            /// The red difference.
            dr: i2 = 0,

            /// The green difference.
            dg: i2 = 0,

            /// The blue difference.
            db: i2 = 0,
        };

        /// A strong brightness difference to the previous pixel.
        // TODO coalesce this with Diff
        const Luma = struct {
            /// The red difference.
            dg: i6 = 0,

            /// The green difference.
            dr: i7 = 0,

            /// The blue difference.
            db: i7 = 0,
        };

        /// A run of the previous pixel.
        const Run = struct {
            length: u6 = 0,
        };

        rgb: Rgb,
        rgba: Rgba,
        index: Index,
        diff: Diff,
        luma: Luma,
        run: Run,
        header: Header,

        /// Sets `self` to `x`. Only for convenience.
        inline fn set(self: *Operation, x: anytype) void {
            self.* = switch (@TypeOf(x)) {
                Rgb => .{ .rgb = x },
                Rgba => .{ .rgba = x },
                Index => .{ .index = x },
                Diff => .{ .diff = x },
                Luma => .{ .luma = x },
                Run => .{ .run = x },
                Header => .{ .header = x },
                else => @compileError("idiot"),
            };
        }

        /// Returns an `Operation` initialized with `x`.
        inline fn init(x: anytype) Operation {
            var ret: Operation = undefined;
            ret.set(x);
            return ret;
        }
    };

    /// The implementation and state of the muxer. Turns `Operation`s into
    /// bytes.
    const Muxer = struct {
        /// The operation that is currently being read.
        current_op: ?Operation = null,

        /// The position inside the current operation.
        pos: u8 = 0,

        /// Whether we've already written a header.
        have_header: bool,

        /// Feeding an operation always produces bytes.
        fn feed(self: *Muxer, op: Operation) void {
            _ = self;
            switch (op) {
                .header => {},
            }
        }
    };

    /// The implementation and state of the scanner. Turns bytes into
    /// `Operation`s.
    const Scanner = struct {
        /// The operation that is currently being read.
        current_op: ?Operation = Operation.init(@as(Operation.Header, undefined)),

        /// The position inside the current operation.
        pos: u8 = 0,

        /// Feeds bytes from a reader into the scanner `self`, returning an
        /// `Operation`. If `null` is returned, the reader is exhausted.
        inline fn feedReader(self: *Scanner, reader: anytype) anyerror!?Operation {
            while (true) {
                const op = try self.feed(reader.readByte() catch |e| switch (e) {
                    error.EndOfStream => return null,
                    else => return e,
                }) orelse continue;
                return op;
            }
        }

        /// Feeds `byte` into the scanner `self`, returning `null` if the
        /// operation is incomplete.
        fn feed(self: *Scanner, byte: u8) ScanError!?Operation {
            // Detect the operation from the first byte.
            if (self.current_op == null) {
                self.current_op = util.parseFirstByte(byte);
                return switch (self.current_op.?) {
                    .rgb,
                    .rgba,
                    .luma,
                    => null,
                    else => b: {
                        defer self.current_op = null;
                        break :b self.current_op;
                    },
                };
            }

            // Now, we assume we have a non-null self.current_op.
            switch (self.current_op.?) {
                .rgba => {
                    switch (self.pos) {
                        0 => self.current_op.?.rgba.r = byte,
                        1 => self.current_op.?.rgba.g = byte,
                        2 => self.current_op.?.rgba.b = byte,
                        3 => {
                            self.current_op.?.rgba.a = byte;
                            self.pos = 0;
                            defer self.current_op = null;
                            return self.current_op;
                        },
                        else => unreachable,
                    }
                },
                .rgb => {
                    switch (self.pos) {
                        0 => self.current_op.?.rgb.r = byte,
                        1 => self.current_op.?.rgb.g = byte,
                        2 => {
                            self.current_op.?.rgb.b = byte;
                            self.pos = 0;
                            defer self.current_op = null;
                            return self.current_op;
                        },
                        else => unreachable,
                    }
                },
                .luma => {
                    self.current_op.?.set(util.lumaWithSecondByte(self.current_op.?.luma, byte));
                    self.pos = 0;
                    defer self.current_op = null;
                    return self.current_op;
                },
                .header => {
                    switch (self.pos) {
                        0 => if (byte != 'q') return error.InvalidMagic,
                        1 => if (byte != 'o') return error.InvalidMagic,
                        2 => if (byte != 'i') return error.InvalidMagic,
                        3 => if (byte != 'f') return error.InvalidMagic,
                        4 => self.current_op.?.header.width = @as(u32, byte) << 24,
                        5 => self.current_op.?.header.width |= @as(u32, byte) << 16,
                        6 => self.current_op.?.header.width |= @as(u32, byte) << 8,
                        7 => self.current_op.?.header.width |= @as(u32, byte),
                        8 => self.current_op.?.header.height = @as(u32, byte) << 24,
                        9 => self.current_op.?.header.height |= @as(u32, byte) << 16,
                        10 => self.current_op.?.header.height |= @as(u32, byte) << 8,
                        11 => self.current_op.?.header.height |= @as(u32, byte),
                        12 => self.current_op.?.header.channels = try Operation.Header.Channels.fromInt(byte),
                        13 => {
                            self.current_op.?.header.colorspace = try Operation.Header.Colorspace.fromInt(byte);
                            self.pos = 0;
                            defer self.current_op = null;
                            return self.current_op;
                        },
                        else => unreachable,
                    }
                },
                else => unreachable,
            }
            self.pos += 1;
            return null;
        }

        /// Various obvious utility functions.
        const util = struct {
            inline fn indexFromByte(byte: u8) Operation.Index {
                return .{ .idx = @as(u6, @truncate(byte)) };
            }

            inline fn byteFromIndex(x: Operation.Index) u8 {
                return x.idx;
            }

            inline fn diffFromByte(byte: u8) Operation.Diff {
                return .{
                    .dr = @as(i2, @intCast(@as(i3, @as(u2, @truncate(byte >> 4))) - 2)),
                    .dg = @as(i2, @intCast(@as(i3, @as(u2, @truncate(byte >> 2))) - 2)),
                    .db = @as(i2, @intCast(@as(i3, @as(u2, @truncate(byte))) - 2)),
                };
            }

            inline fn byteFromDiff(x: Operation.Diff) u8 {
                return 0b01000000 | @as(u8, x.dr) << 4 | @as(u8, x.dg) << 2 | x.db;
            }

            inline fn lumaFromFirstByte(byte: u8) Operation.Luma {
                return .{
                    .dg = @as(i6, @intCast(@as(i7, @as(u6, @truncate(byte))) - 32)),
                    .dr = undefined,
                    .db = undefined,
                };
            }

            inline fn firstByteFromLuma(luma: Operation.Luma) u8 {
                return 0b10000000 | (luma.dg + 32);
            }

            inline fn lumaWithSecondByte(old_luma: Operation.Luma, byte: u8) Operation.Luma {
                return .{
                    .dg = old_luma.dg,
                    .db = (@as(i7, @as(u4, @truncate(byte))) - 8) + old_luma.dg,
                    .dr = (@as(i7, @as(u4, @truncate(byte >> 4))) - 8) + old_luma.dg,
                };
            }

            inline fn secondByteFromLuma(luma: Operation.Luma) u8 {
                return luma.db - luma.dg + 8;
            }

            inline fn runFromByte(byte: u8) Operation.Run {
                return .{ .length = @as(u6, @truncate(byte)) + 1 };
            }

            inline fn byteFromRun(run: Operation.Run) u8 {
                if (run.length > 62 or run.length == 0) unreachable;
                return 0b11000000 | @as(u6, @truncate(run - 1));
            }

            inline fn parseFirstByte(byte: u8) Operation {
                return switch (@as(u2, @truncate(byte >> 6))) {
                    0b00 => Operation.init(indexFromByte(byte)),
                    0b01 => Operation.init(diffFromByte(byte)),
                    0b10 => Operation.init(lumaFromFirstByte(byte)),
                    0b11 => switch (@as(u6, @truncate(byte))) {
                        0b111111 => Operation.init(@as(Operation.Rgba, undefined)),
                        0b111110 => Operation.init(@as(Operation.Rgb, undefined)),
                        else => Operation.init(runFromByte(byte)),
                    },
                };
            }
            test "indexFromByte" {
                try std.testing.expectEqual(indexFromByte(0b00111111), Operation.Index{ .idx = 63 });
            }
            test "diffFromByte" {
                try std.testing.expectEqual(diffFromByte(0b01111111), Operation.Diff{ .dr = 1, .dg = 1, .db = 1 });
            }
            test "lumaFromFirstByte" {
                try std.testing.expectEqual(lumaFromFirstByte(0b10100000).dg, 0);
            }
            test "lumaWithSecondByte" {
                var x = lumaFromFirstByte(0b10100000);
                x = lumaWithSecondByte(x, 0b11111110);
                try std.testing.expectEqual(x, Operation.Luma{ .dg = 0, .dr = 7, .db = 6 });
            }
            test "runFromByte" {
                try std.testing.expectEqual(runFromByte(0b11111101), Operation.Run{ .length = 62 });
            }
            test "parseFirstByte" {
                try std.testing.expect(parseFirstByte(0b11111111) == .rgba);
                try std.testing.expect(parseFirstByte(0b11111110) == .rgb);
                try std.testing.expect(parseFirstByte(0b11000000) == .run);
                try std.testing.expect(parseFirstByte(0b10000000) == .luma);
                try std.testing.expect(parseFirstByte(0b00000000) == .index);
                try std.testing.expect(parseFirstByte(0b01000000) == .diff);
            }
        };
    };

    /// The implementation and state of the decoder. Turns `Operation`s into
    /// `Pixel`s.
    const Decoder = struct {
        /// Previosly seen pixels indexed by the result of the hash of the
        /// pixels.
        seen: [64]Pixel = undefined,

        /// The current pixel.
        current_pixel: Pixel = Pixel.init(0, 0, 0, 255),

        /// For popping, how many pixels are still left in the current run of
        /// pixels.
        run_left: u6 = 0,

        // Sometimes it's faster to check the previous pixel for equality,
        // sometimes it's faster not to. I'll leave this in for now. If I
        // replace `Pixel` with an array-based implementation, it will probably
        // be faster.
        /// The previous pixel. This is not necessary for the implementation to
        /// work, it is only an optimization that skips comstoreing the hash.
        prev_pixel: Pixel = Pixel.init(0, 0, 0, 255),

        /// The result returned by `pop`.
        const PopResult = struct {
            /// The pixel popped from the decoder.
            p: Pixel,
            /// `true` if there aren't any more pixels left to pop.
            is_end: bool,
        };

        /// Pushes the operation `op`, to the decoder `self`.
        fn push(self: *Decoder, op: Operation) PushError!void {
            switch (op) {
                // assume the user has already read the header from the scanner
                .header => return error.InvalidStream,

                .run => |r| self.run_left = r.length,

                // These get translated by the scanner to basically the same
                // thing. TODO coalesce these?
                .diff => |d| self.current_pixel.diff(d.dr, d.dg, d.db),
                .luma => |d| self.current_pixel.diff(d.dr, d.dg, d.db),

                .rgb => |r| self.current_pixel.set(r.r, r.g, r.b, self.current_pixel.getA()),
                .rgba => |r| self.current_pixel.set(r.r, r.g, r.b, r.a),
                .index => |i| self.current_pixel = self.seen[i.idx],
            }
        }

        /// Pops a pixel off the decoder.
        fn pop(self: *Decoder) PopResult {
            if (self.run_left > 1) {
                self.run_left -= 1;
                return PopResult{ .p = self.current_pixel, .is_end = false };
            }

            if (!self.current_pixel.eql(self.prev_pixel))
                self.seen[self.current_pixel.hash()] = self.current_pixel;

            self.prev_pixel = self.current_pixel;

            return PopResult{ .p = self.current_pixel, .is_end = true };
        }
    };
};

test "Operation" {
    var op: impl.Operation = undefined;
    op.set(impl.Operation.Rgb{ .r = 127, .g = 127, .b = 127 });
    try std.testing.expectEqual(op.rgb, impl.Operation.Rgb{ .r = 127, .g = 127, .b = 127 });
    op.set(impl.Operation.Index{ .idx = 10 });
    try std.testing.expectEqual(op.index, impl.Operation.Index{ .idx = 10 });
}

test "Scanner" {
    const expected_header = impl.Operation.init(impl.Operation.Header{
        .width = 1920,
        .colorspace = .srgb,
        .height = 1080,
        .channels = .rgba,
    });
    const expected_rgb = impl.Operation.init(impl.Operation.Rgb{ .r = 127, .g = 127, .b = 127 });
    const expected_rgba = impl.Operation.init(impl.Operation.Rgba{ .r = 127, .g = 127, .b = 127, .a = 127 });
    const expected_index = impl.Operation.init(impl.Operation.Index{ .idx = 24 });
    const expected_diff = impl.Operation.init(impl.Operation.Diff{ .dr = 1, .dg = -1, .db = -2 });
    const expected_luma = impl.Operation.init(impl.Operation.Luma{ .dg = 21, .dr = 24, .db = 19 });
    const expected_run = impl.Operation.init(impl.Operation.Run{ .length = 11 });

    var scanner = impl.Scanner{};
    try std.testing.expectEqual(try scanner.feed('q'), null);
    try std.testing.expectEqual(try scanner.feed('o'), null);
    try std.testing.expectEqual(try scanner.feed('i'), null);
    try std.testing.expectEqual(try scanner.feed('f'), null);
    try std.testing.expectEqual(try scanner.feed(0), null);
    try std.testing.expectEqual(try scanner.feed(0), null);
    try std.testing.expectEqual(try scanner.feed(0x07), null);
    try std.testing.expectEqual(try scanner.feed(0x80), null);
    try std.testing.expectEqual(try scanner.feed(0), null);
    try std.testing.expectEqual(try scanner.feed(0), null);
    try std.testing.expectEqual(try scanner.feed(0x04), null);
    try std.testing.expectEqual(try scanner.feed(0x38), null);
    try std.testing.expectEqual(try scanner.feed(4), null);
    try std.testing.expectEqual((try scanner.feed(0)).?, expected_header);

    try std.testing.expectEqual((try scanner.feed(0b11111110)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)).?, expected_rgb);

    try std.testing.expectEqual((try scanner.feed(0b11111111)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)), null);
    try std.testing.expectEqual((try scanner.feed(0x7f)).?, expected_rgba);

    try std.testing.expectEqual((try scanner.feed(0b00_011000)).?, expected_index);

    try std.testing.expectEqual((try scanner.feed(0b01_11_01_00)).?, expected_diff);

    try std.testing.expectEqual((try scanner.feed(0b10_110101)), null);
    try std.testing.expectEqual((try scanner.feed(0b1011_0110)).?, expected_luma);

    try std.testing.expectEqual((try scanner.feed(0b11_001010)).?, expected_run);
}

test "Pixel" {
    var test_pixel = Pixel.init(1, 3, 3, 7);
    const expected_rgb = [3]u8{ 1, 3, 3 };
    const expected_rgba = [4]u8{ 1, 3, 3, 7 };
    var actual: [4]u8 = undefined;
    try std.testing.expectEqual(test_pixel.getR(), 1);
    try std.testing.expectEqual(test_pixel.getG(), 3);
    try std.testing.expectEqual(test_pixel.getB(), 3);
    try std.testing.expectEqual(test_pixel.getA(), 7);
    test_pixel.store(&actual, false);
    try std.testing.expectEqual(expected_rgb, actual[0..3].*);
    test_pixel.store(&actual, true);
    try std.testing.expectEqual(expected_rgba, actual);
    try std.testing.expect(test_pixel.eql(test_pixel));
    test_pixel.set(1, 2, 3, 4);
    try std.testing.expect(Pixel.init(1, 2, 3, 4).eql(test_pixel));
    test_pixel.setRgb(&expected_rgb);
    try std.testing.expect(Pixel.init(1, 3, 3, 4).eql(test_pixel));
    test_pixel.setRgba(&expected_rgba);
    try std.testing.expect(Pixel.init(1, 3, 3, 7).eql(test_pixel));
}

test "Pixel.hash" {
    const tmp = struct {
        // This cursedness translated from C
        fn hash(arg_r: u8, arg_g: u8, arg_b: u8, arg_a: u8) u8 {
            const r = @as(c_int, @bitCast(@as(c_uint, arg_r)));
            const g = @as(c_int, @bitCast(@as(c_uint, arg_g)));
            const b = @as(c_int, @bitCast(@as(c_uint, arg_b)));
            const a = @as(c_int, @bitCast(@as(c_uint, arg_a)));
            return @as(u8, @bitCast(@as(i8, @truncate(@import("std").zig.c_translation.signedRemainder((((r * @as(c_int, 3)) + (g * @as(c_int, 5))) + (b * @as(c_int, 7))) + (a * @as(c_int, 11)), @as(c_int, 64))))));
        }
    };
    try std.testing.expectEqual(Pixel.init(0, 0, 0, 0).hash(), @as(u6, @truncate(tmp.hash(0, 0, 0, 0))));
    try std.testing.expectEqual(Pixel.init(1, 2, 3, 4).hash(), @as(u6, @truncate(tmp.hash(1, 2, 3, 4))));
    try std.testing.expectEqual(Pixel.init(1, 3, 3, 7).hash(), @as(u6, @truncate(tmp.hash(1, 3, 3, 7))));
    try std.testing.expectEqual(Pixel.init(127, 127, 127, 127).hash(), @as(u6, @truncate(tmp.hash(127, 127, 127, 127))));
    try std.testing.expectEqual(Pixel.init(255, 255, 255, 255).hash(), @as(u6, @truncate(tmp.hash(255, 255, 255, 255))));
}

test "Decoder" {
    const instore = [_]impl.Operation{
        .{ .rgba = .{ .r = 1, .g = 3, .b = 3, .a = 7 } },
        .{ .run = .{ .length = 4 } },
        .{ .rgb = .{ .r = 4, .g = 2, .b = 0 } },
    };
    const expected_outstore = [_]impl.Decoder.PopResult{
        .{ .p = Pixel.init(1, 3, 3, 7), .is_end = true },
        .{ .p = Pixel.init(1, 3, 3, 7), .is_end = false },
        .{ .p = Pixel.init(1, 3, 3, 7), .is_end = false },
        .{ .p = Pixel.init(1, 3, 3, 7), .is_end = false },
        .{ .p = Pixel.init(1, 3, 3, 7), .is_end = true },
        .{ .p = Pixel.init(4, 2, 0, 7), .is_end = true },
    };
    var actual_outstore: [expected_outstore.len]impl.Decoder.PopResult = undefined;
    var dec = impl.Decoder{};
    try dec.push(instore[0]);
    actual_outstore[0] = dec.pop();
    try dec.push(instore[1]);
    actual_outstore[1] = dec.pop();
    actual_outstore[2] = dec.pop();
    actual_outstore[3] = dec.pop();
    actual_outstore[4] = dec.pop();
    try dec.push(instore[2]);
    actual_outstore[5] = dec.pop();
    try std.testing.expectEqual(expected_outstore, actual_outstore);
}

/// Contains all the information about an image, excluding the actual data.
const ImageInfo = struct {
    /// A pixel format.
    const PixelFormat = enum(u8) {
        rgb = 3,
        rgba = 4,

        fn getDepth(x: PixelFormat) usize {
            return @intFromEnum(x);
        }
    };

    /// A colorspace.
    const Colorspace = enum(u1) { srgb = 0, linear = 1 };

    /// The number of rows in the image.
    rows: u32,

    /// The number of columns in the image.
    cols: u32,

    /// The pixel format of the image.
    pix_fmt: PixelFormat,

    /// The colorspace of the image.
    colorspace: Colorspace,

    pub fn getMeta(info: ImageInfo) zenit.FrameMetadata {
        const layout: zenit.image.Layout = switch (info.pix_fmt) {
            .rgb => .xyz,
            .rgba => .xyzw,
        };

        const interpretation: zenit.image.Interpretation = switch (info.colorspace) {
            .linear => if (layout == .xyz) zenit.image.Interpretation.s_rgb_linear else zenit.image.Interpretation.s_rgb_linear,
            .srgb => if (layout == .xyz) zenit.image.Interpretation.s_rgb else zenit.image.Interpretation.s_rgb,
        };

        return .{
            .medium_data = .{
                .image = .{ .rows = info.rows, .columns = info.cols, .vfr_frame_duration = .{ .nsecs = 0 } },
            },
            .media_type = .image,
            .layout = .{ .image = layout },
            .interpretation = .{ .image = interpretation },
            .pts = undefined,
            .dts = undefined,
            .flags = .{ .is_key_frame = true },
        };
    }
};

const Image = struct {
    ii: ImageInfo,
    data: [*]u8,

    fn initAlloc(x: ImageInfo, a: std.mem.Allocator) !Image {
        return Image{
            .ii = x,
            .data = (try a.alloc(u8, x.cols * x.rows * x.pix_fmt.getDepth())).ptr,
        };
    }

    fn alloc(self: *Image, x: ImageInfo, a: std.mem.Allocator) !void {
        self.* = .{
            .ii = x,
            .data = (try a.alloc(u8, self.ii.cols * self.ii.rows * self.ii.pix_fmt.getDepth())).ptr,
        };
    }

    fn free(self: Image, a: std.mem.Allocator) void {
        a.free(self.data[0 .. self.ii.cols * self.ii.rows * self.ii.pix_fmt.getDepth()]);
    }

    inline fn getAt(self: Image, row: u32, col: u32) Pixel {
        const p = self.data + row * self.ii.cols + col;
        return if (self.ii.colorspace == .rgba) {
            .{ .rgba = @as(PixelVector, p[0..4]) };
        } else {
            .{ .rgba = .{ p[0], p[1], p[2], undefined } };
        };
    }

    inline fn setAt(self: Image, row: u32, col: u32, x: Pixel) void {
        const p = self.data + row * self.ii.cols + col;
        if (self.ii.colorspace == .rgba) {
            x.setRgb(p[0..3]);
        } else {
            x.setRgba(p[0..4]);
        }
    }
};

/// Any error that can occur in decoding, where decoding is meant as the
/// combination of demuxing and actual decoding plus API sugar.
const Error = impl.ScanError || impl.PushError || error{ OutOfMemory, UnknownError };

/// First, call `pushForInfo` until you get an `ImageInfo`. Then, call `push`, then `pop`.
const PixelDecoder = struct {
    scanner: impl.Scanner = .{},
    dec: impl.Decoder = .{},
    pixels_left: usize = 0,

    fn pushForInfo(self: *PixelDecoder, byte: u8) Error!?ImageInfo {
        const op = (try self.scanner.feed(byte)) orelse return null;
        if (op != .header) return error.InvalidStream;
        const ii = op.header.toImageInfo();
        self.pixels_left = ii.cols * ii.rows;
        return ii;
    }

    fn pushForInfoReader(self: *PixelDecoder, reader: anytype) !ImageInfo {
        return while (true) break (try self.pushForInfo(try reader.readByte())) orelse continue;
    }

    fn push(self: *PixelDecoder, byte: u8) Error!bool {
        if (self.pixels_left == 0) return true;
        const op = (try self.scanner.feed(byte)) orelse return false;

        try self.dec.push(op);
        return true;
    }

    fn pushReader(self: *PixelDecoder, reader: anytype) !bool {
        while (true) if (try self.push(reader.readByte() catch |e| switch (e) {
            error.EndOfStream => return false,
            else => return e,
        })) break;
        return true;
    }

    const PopResult = struct {
        p: Pixel,
        is_end: bool,
    };

    fn pop(self: *PixelDecoder) ?PopResult {
        const pr = self.dec.pop();
        if (self.pixels_left == 0)
            return null;

        self.pixels_left -= 1;
        return .{ .p = pr.p, .is_end = pr.is_end };
    }
};
