const std = @import("std");
const math = std.math;
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "nektro_jpg",
        .v_table = &.{
            .probeFn = &@import("jpg.zig").probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    _ = z;
    _ = data;
    _ = in;
    return .{};
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    _ = z;
    _ = data;
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    _ = data;
    return .{ .frame = try parse(z, in) };
}

// original implementation by nektro
// modified for zenit by cancername

// Parser for JPEG File Interchange Format
// https://en.wikipedia.org/wiki/JPEG
// https://en.wikipedia.org/wiki/JPEG_File_Interchange_Format
// https://www.w3.org/Graphics/JPEG/
// https://jpeg.org/jpeg/
// https://www.loc.gov/preservation/digital/formats/fdd/fdd000018.shtml
// https://github.com/corkami/formats/blob/master/image/jpeg.md
// https://www.youtube.com/watch?v=Q2aEzeMDHMA
// https://www.youtube.com/watch?v=0me3guauqOU
// http://www.opennet.ru/docs/formats/jpeg.txt

// TODO: handle progressive jpeg
// TODO: read and attach thumbnail to frame metadata
// TODO: unknown unknowns
// TODO: use init properly

pub fn parse(z: *zenit.Context, in: *zenit.InputStream) anyerror!zenit.Frame {
    // This code is pretty, but I don't trust it yet. TODO rewrite.
    @setRuntimeSafety(true);

    if (!try in.readExpected("\xff\xd8")) try z.diag(
        .err,
        .decoder,
        error.InvalidMagic,
        "SOS marker not found.",
        .{},
    );

    var alloc_s = z.alloc.acquireTemp();
    defer z.alloc.releaseTemp(alloc_s);
    const alloc = alloc_s.allocator();

    var factor_tables: [3]FactorTable = undefined;
    var quantization_table_luminance: [64]u8 = .{0} ** 64;
    var quantization_table_chrominance: [64]u8 = .{0} ** 64;
    var huffcodes: [2][2][]const struct { u8, u16, u8 } = undefined;
    var nb_components: usize = 0;

    var meta: zenit.FrameMetadata = .{
        .format = .{ .image = .{ .color_space = .bt_709, .pixel_format = .yuv444p } },
        .medium_data = undefined,
        .pts = undefined,
        .dts = undefined,
        .flags = .{ .is_key_frame = true },
    };

    // SOS marker breaks this loop
    while (true) {
        if (!try in.readExpected("\xff")) {
            try z.diag(.err, .decoder, error.NoMarker, "ff marker not found.", .{});
        }

        const marker: MarkerCode = @enumFromInt(try in.readByte());

        if (marker == .SOI) try z.diag(.err, .decoder, error.MisplacedSoiMarker, "Misplaced SOI marker at position {d}.", .{in.pos});

        const segment_length = try math.sub(u16, try in.readInt(u16, .big), 2);

        switch (marker) {
            .SOI => unreachable,

            .APP0 => {
                if (!try in.readExpected("JFIF\x00")) try z.diag(.err, .decoder, error.NoJfifMagic, "Invalid JFIF magic at position {d}.", .{in.pos});
                const version_major = try in.readByte();
                const version_minor = try in.readByte();
                const density_unit: DensityUnit = @enumFromInt(try in.readByte());
                if (zenit.util.isElseValue(density_unit)) try z.diag(
                    .err,
                    .decoder,
                    error.InvalidDensityUnit,
                    "Invalid density unit {}.",
                    .{density_unit},
                );
                const density_x = try in.readInt(u16, .big);
                const density_y = try in.readInt(u16, .big);

                const thumbnail_width = try in.readByte();
                const thumbnail_height = try in.readByte();

                if (version_major != 1) try z.diag(.err, .decoder, error.IncompatibleMajorVersion, "Incompatible JFIF major version {d}", .{version_major});

                try meta.extra_data.putDupe("density_unit", .{ .string = @constCast(@tagName(density_unit)) }, z);
                try meta.extra_data.putDupeKey("density_x", .{ .unsigned = density_x }, z);
                try meta.extra_data.putDupeKey("density_y", .{ .unsigned = density_y }, z);
                try meta.extra_data.putDupeKey("jfif_minor_version", .{ .unsigned = version_minor }, z);

                try in.seek(.{ .cur = @intCast(@as(usize, 3) * thumbnail_width * thumbnail_height) });
            },

            .DQT => {
                const destination: Destination = @enumFromInt(try in.readByte());
                const dstp = switch (destination) {
                    .luminance => &quantization_table_luminance,
                    .chrominance => &quantization_table_chrominance,
                    _ => try z.diag(.err, .decoder, error.InvalidQTDest, "Invalid quantization table destination {}.", .{destination}),
                };
                try in.readNoEof(dstp);
            },

            .SOF0 => {
                const bit_depth = try in.readByte();
                if (bit_depth != 8) try z.diag(.err, .decoder, error.UnsupportedBitDepth, "The bit depth {d} is not yet supported.", .{bit_depth});
                meta.medium_data = .{ .image = .{ .rows = try in.readInt(u16, .big), .columns = try in.readInt(u16, .big) } };
                nb_components = try in.readInt(u8, .big);
                switch (nb_components) {
                    1, 3 => {},
                    else => try z.diag(.err, .decoder, error.UnsupportedComponentCt, "Component count {d} not supported.", .{nb_components}),
                }
                for (0..nb_components) |i| {
                    factor_tables[i] = @bitCast(try in.readInt(u24, .little));
                }
            },

            .DHT => {
                const thtc = @as(ThTc, @bitCast(try in.readByte()));
                if (thtc.class > 1 or thtc.destination > 1) try z.diag(.err, .decoder, error.InvalidThtc, "Invalid ThTc {}.", .{thtc});
                const huffsize = try in.readBytesNoEof(16);

                var huffcode = std.ArrayList(struct { u8, u16, u8 }).init(alloc);

                var code: u16 = 0;
                for (0..16) |i| {
                    for (0..huffsize[i]) |_| {
                        try huffcode.append(.{
                            @as(u8, @intCast(i + 1)),
                            code,
                            try in.readByte(),
                        });
                        code += 1;
                    }
                    code <<= 1;
                }

                huffcodes[thtc.class][thtc.destination] = try huffcode.toOwnedSlice();
            },

            .SOS => {
                if (try in.readByte() != nb_components) try z.diag(
                    .err,
                    .decoder,
                    error.NonmatchingProgressiveNbComponents,
                    "The number of components in the scan does not match {d}.",
                    .{nb_components},
                );
                for (0..nb_components) |_| {
                    const scan_component: ScanComponent = @bitCast(try in.readInt(u16, .little));
                    _ = scan_component;
                }
                const spectral_selection_start = try in.readByte();
                const spectral_selection_end = try in.readByte();
                const successive_approximation: SuccessiveApproximation = @bitCast(try in.readByte());

                _ = spectral_selection_start;
                _ = spectral_selection_end;
                _ = successive_approximation;

                break;
            },

            .APP1,
            .APP2,
            .APP3,
            .APP4,
            .APP5,
            .APP6,
            .APP7,
            .APP8,
            .APP9,
            .APPA,
            .APPB,
            .APPC,
            .APPD,
            .APPE,
            .APPF,
            .COM,
            => |m| {
                z.diag(.err, .decoder, {}, "TODO: Unsupported marker {}.", .{m});
                try in.seek(.{ .cur = segment_length });
            },

            else => |v| {
                z.diag(.err, .decoder, {}, "Unknown marker {d}.", .{@intFromEnum(v)});
                try in.seek(.{ .cur = segment_length });
            },
        }
    }
    const q_tables = [3]*[64]u8{ &quantization_table_luminance, &quantization_table_chrominance, &quantization_table_chrominance };

    var bitreader = try Biterator.init(in);

    var data4 = std.ArrayList([8][8]f32).init(alloc);
    var mcu_count: usize = 0;

    // read_mcu
    var dc_prev: [3]i16 = .{ 0, 0, 0 };
    while (!bitreader.eoi) {
        for (0..nb_components) |i| {
            if (bitreader.eoi) continue;
            const component = factor_tables[i];
            // log.debug("component: {d}: {d}x{d}", .{ i + 1, component.sampling_factor_x, component.sampling_factor_y });

            for (0..(component.sampling_factor_x * component.sampling_factor_y)) |_| {
                // read_data_unit
                var huff_tbl = huffcodes[0][@intFromEnum(component.destination)];
                var data = std.BoundedArray(i16, 64){};
                while (data.len < 64) {
                    var key: u16 = 0;
                    var key_len: ?u8 = null;

                    var bits: u8 = 0;
                    for (1..17) |wbits| {
                        key_len = null;
                        bits = @intCast(wbits);
                        key = @shlExact(key, 1);
                        const val = try bitreader.next() orelse break;
                        key |= val;
                        key_len = hufftableGet(huff_tbl, .{ bits, key });
                        if (key_len != null) break;
                    }
                    if (bitreader.eoi) break;

                    huff_tbl = huffcodes[1][@intFromEnum(component.destination)];

                    if (key_len == null) {
                        z.diag(.err, .decoder, {}, "key not found: {d}, {d}", .{ bits, key });
                        break;
                    }
                    if (key_len.? == 0xF0) {
                        try data.appendNTimes(0, 16);
                        continue;
                    }
                    if (data.len > 0) {
                        if (key_len.? == 0x00) {
                            data.appendNTimesAssumeCapacity(0, data.capacity() - data.len);
                            break;
                        }
                        for (0..(key_len.? >> 4)) |_| {
                            if (data.len < 64) {
                                data.appendAssumeCapacity(0);
                            }
                        }
                        key_len.? &= 0x0F;
                    }
                    if (data.len >= 64) {
                        break;
                    }
                    if (key_len.? != 0) {
                        const val = try bitreader.nextBits(u16, key_len.?) orelse break;
                        const num = calcAddBits(@intCast(key_len.?), @intCast(val));
                        data.appendAssumeCapacity(num);
                    } else {
                        data.appendAssumeCapacity(0);
                    }
                }
                if (bitreader.eoi) break;
                if (data.len != 64) try z.diag(.err, .decoder, error.BadDataLen, "Wrong data langth: {d}.", .{data.len});

                data.buffer[0] += dc_prev[i];
                dc_prev[i] = data.buffer[0];

                const q_table = q_tables[i];
                for (&data.buffer, q_table) |*d, q| {
                    d.* *= q;
                }

                var data2: [64]i16 = undefined;
                for (0.., zigzag) |a, b| {
                    data2[a] = data.buffer[b];
                }
                var data2f: [64]f32 = undefined;
                for (data2, 0..) |a, j| {
                    data2f[j] = @floatFromInt(a);
                }

                var data3: [8][8]f32 = undefined;
                for (0..8) |x| {
                    for (0..8) |y| {
                        var sum: f32 = 0;
                        // I looked into vectorizing this but suggestVectorSize(f32) was 8 so doing so made this a lot slower.
                        for (0..8) |u| {
                            for (0..8) |v| {
                                const matrix_value = data2f[(v * 8) + u];
                                sum += matrix_value * idct_table[u][x] * idct_table[v][y];
                            }
                        }
                        data3[y][x] = @divFloor(sum, 4);
                    }
                }
                // log.debug("inverse dct :: {d}", .{@as([64]f32, @bitCast(data3))});

                try data4.append(data3);
                // std.debug.print("\n", .{});
            }
        }
        mcu_count += 1;
    }
    mcu_count -= 1;

    const sfx = [3]u4{ factor_tables[0].sampling_factor_x, factor_tables[1].sampling_factor_x, factor_tables[2].sampling_factor_x };
    const sfy = [3]u4{ factor_tables[0].sampling_factor_y, factor_tables[1].sampling_factor_y, factor_tables[2].sampling_factor_y };
    // log.debug("scaling factor: x: {d}", .{sfx});
    // log.debug("scaling factor: y: {d}", .{sfy});

    var data5 = std.ArrayList([]const [3]f32).init(alloc);
    try data5.ensureUnusedCapacity(mcu_count);

    const sfx_max = std.mem.max(u4, &sfx);
    const sfy_max = std.mem.max(u4, &sfy);
    const d5w = @as(u16, sfx_max) * 8;
    const d5h = @as(u16, sfy_max) * 8;
    var compidx: usize = 0;
    for (0..mcu_count) |_| {
        var data6 = std.ArrayList([3]f32).init(alloc);
        try data6.appendNTimes(.{ 0, 0, 0 }, d5w * d5h);
        for (0..nb_components) |i| {
            for (0..sfy[i]) |j| {
                for (0..sfx[i]) |k| {
                    const hin = sfx[i];
                    const vin = sfy[i];
                    const hs = @divFloor(sfx_max, hin);
                    const vs = @divFloor(sfy_max, vin);
                    const comp = data4.items[compidx];
                    compidx += 1;
                    for (0..8) |uy| {
                        for (0..8) |ux| {
                            for (0..vs) |v| {
                                for (0..hs) |h| {
                                    const x = @as(u32, @intCast(ux)) * hs + h + (k * 8);
                                    const y = @as(u32, @intCast(uy)) * vs + v + (j * 8);
                                    data6.items[(y * d5w) + x][i] = comp[uy][ux];
                                }
                            }
                        }
                    }
                }
            }
        }
        // log.debug("combine mcu :: {d}", .{data6.items});
        data5.appendAssumeCapacity(try data6.toOwnedSlice());
    }

    const ww: usize = @intCast(meta.medium_data.image.columns);
    const wh: usize = @intCast(meta.medium_data.image.rows);

    var frame = try zenit.Frame.alloc(&z.alloc, meta);
    errdefer frame.free(&z.alloc);

    var offsetx: usize = 0;
    var offsety: usize = 0;

    switch (nb_components) {
        1 => try z.diag(.err, .decoder, error.TodoComponent, "TODO: 1 component", .{}),
        3 => {},
        else => unreachable,
    }

    for (data5.items) |block| {
        for (0..d5h) |yb| {
            const y = offsety + yb;
            if (y >= wh) break;

            for (0..d5w) |xb| {
                const x = offsetx + xb;
                if (x >= ww) break;

                const comp = block[(yb * d5w) + xb];
                const plane_len = ww * wh;
                frame.data[(y * ww) + x] = @intFromFloat(comp[0] * 128);
                frame.data[plane_len + (y * ww) + x] = @intFromFloat(comp[1] * 128);
                frame.data[plane_len * 2 + (y * ww) + x] = @intFromFloat(comp[2] * 128);
            }
        }
        offsetx += d5w;
        if (offsetx >= ww) {
            offsetx = 0;
            offsety += d5h;
        }
    }

    return frame;
}

fn hufftableGet(haystack: []const struct { u8, u16, u8 }, needle: struct { u8, u16 }) ?u8 {
    for (haystack) |item| {
        if (item[0] == needle[0] and item[1] == needle[1]) {
            return item[2];
        }
    }
    return null;
}

fn calcAddBits(len: u4, val: i16) i16 {
    const one: i16 = 1;
    if (val & (one << (len - 1)) > 0) {
        return val;
    } else {
        return val - ((one << len) - 1);
    }
}

const idct_table = blk: {
    var out: [8][8]f32 = undefined;
    for (0..8) |y| {
        for (0..8) |x| {
            const fy: f32 = @floatFromInt(y);
            const fx: f32 = @floatFromInt(x);
            out[y][x] = (C(y) * @cos(((2.0 * fx + 1.0) * fy * std.math.pi) / 16.0));
        }
    }
    break :blk out;
};

fn C(x: f32) f32 {
    if (x == 0) return 1.0 / @sqrt(2.0);
    return 1.0;
}

fn clamp(a: f32) u8 {
    const b: i16 = @intFromFloat(a);
    const c = @divFloor(((std.math.absInt(b) catch unreachable) + b), 2);
    if (c > 255) return 255;
    return @intCast(c);
}

/// Table B.1 – Marker code assignments
const MarkerCode = enum(u8) {
    stuffing = 0x00, // Reserved

    TEM = 0x01, // For temporary private use in arithmetic coding

    // _ = 0x02...0xBF, // Reserved

    SOF0 = 0xC0, // Baseline DCT
    SOF1 = 0xC1, // Extended sequential DCT
    SOF2 = 0xC2, // Progressive DCT
    SOF3 = 0xC3, // Lossless (sequential)

    DHT = 0xC4, // Define Huffman table(s)

    SOF5 = 0xC5, // Differential sequential DCT
    SOF6 = 0xC6, // Differential progressive DCT
    SOF7 = 0xC7, // Differential lossless (sequential)

    JPG = 0xC8, // Reserved for JPEG extensions
    SOF9 = 0xC9, // Extended sequential DCT
    SOF10 = 0xCA, // Progressive DCT
    SOF11 = 0xCB, // Lossless (sequential)

    SOF13 = 0xCD, // Differential sequential DCT
    SOF14 = 0xCE, // Differential progressive DCT
    SOF15 = 0xCF, // Differential lossless (sequential)

    DAC = 0xCC, // Define arithmetic coding conditioning(s)

    RST0 = 0xD0, // Restart with modulo 8 count 0
    RST1 = 0xD1, // Restart with modulo 8 count 1
    RST2 = 0xD2, // Restart with modulo 8 count 2
    RST3 = 0xD3, // Restart with modulo 8 count 3
    RST4 = 0xD4, // Restart with modulo 8 count 4
    RST5 = 0xD5, // Restart with modulo 8 count 5
    RST6 = 0xD6, // Restart with modulo 8 count 6
    RST7 = 0xD7, // Restart with modulo 8 count 7

    SOI = 0xD8, // Start of image
    EOI = 0xD9, // End of image
    SOS = 0xDA, // Start of scan
    DQT = 0xDB, // Define quantization table(s)
    DNL = 0xDC, // Define number of lines
    DRI = 0xDD, // Define restart interval
    DHP = 0xDE, // Define hierarchical progression
    EXP = 0xDF, // Expand reference component(s)
    APP0 = 0xE0, // Reserved for application segments
    APP1 = 0xE1, // Reserved for application segments
    APP2 = 0xE2, // Reserved for application segments
    APP3 = 0xE3, // Reserved for application segments
    APP4 = 0xE4, // Reserved for application segments
    APP5 = 0xE5, // Reserved for application segments
    APP6 = 0xE6, // Reserved for application segments
    APP7 = 0xE7, // Reserved for application segments
    APP8 = 0xE8, // Reserved for application segments
    APP9 = 0xE9, // Reserved for application segments
    APPA = 0xEA, // Reserved for application segments
    APPB = 0xEB, // Reserved for application segments
    APPC = 0xEC, // Reserved for application segments
    APPD = 0xED, // Reserved for application segments
    APPE = 0xEE, // Reserved for application segments
    APPF = 0xEF, // Reserved for application segments

    JPG0 = 0xF0, // Reserved for JPEG extensions
    JPG1 = 0xF1, // Reserved for JPEG extensions
    JPG2 = 0xF2, // Reserved for JPEG extensions
    JPG3 = 0xF3, // Reserved for JPEG extensions
    JPG4 = 0xF4, // Reserved for JPEG extensions
    JPG5 = 0xF5, // Reserved for JPEG extensions
    JPG6 = 0xF6, // Reserved for JPEG extensions
    JPG7 = 0xF7, // Reserved for JPEG extensions
    JPG8 = 0xF8, // Reserved for JPEG extensions
    JPG9 = 0xF9, // Reserved for JPEG extensions
    JPGA = 0xFA, // Reserved for JPEG extensions
    JPGB = 0xFB, // Reserved for JPEG extensions
    JPGC = 0xFC, // Reserved for JPEG extensions
    JPGD = 0xFD, // Reserved for JPEG extensions
    COM = 0xFE, // Comment

    // _ = 0xFF, // Reserved

    _,
};

const Destination = enum(u8) {
    luminance,
    chrominance,
    _,
};

const DensityUnit = enum(u8) {
    none,
    px_per_in,
    px_per_cm,
    _,
};

const FactorTable = packed struct {
    id: u8,
    sampling_factor_x: u4,
    sampling_factor_y: u4,
    destination: Destination,
};

const ScanComponent = packed struct {
    selector: u8,
    dc_destination: u4,
    ac_destination: u4,
};

const SuccessiveApproximation = packed struct {
    high: u4,
    low: u4,
};

const ThTc = packed struct {
    destination: u4, //enum { luminance, chrominance },
    class: u4, //enum { DC, AC },
};

const Biterator = struct {
    reader: *zenit.InputStream,
    bits: std.bit_set.IntegerBitSet(8),
    idx: u8,
    eoi: bool,

    pub fn init(reader: *zenit.InputStream) !Biterator {
        return .{
            .reader = reader,
            .bits = .{ .mask = try reader.readByte() },
            .idx = 0,
            .eoi = false,
        };
    }

    pub fn next(self: *Biterator) !?u1 {
        if (self.eoi) return null;
        if (self.idx == 8) try self.reset();
        if (self.idx == 0) try self.check();
        if (self.eoi) return null;
        const result = self.bits.isSet(7 - self.idx);
        self.idx += 1;
        return @intFromBool(result);
    }

    pub fn nextBits(self: *Biterator, comptime T: type, amt: usize) !?T {
        if (self.eoi) return null;
        var out: T = 0;
        for (0..amt) |_| {
            out <<= 1;
            const val = try self.next() orelse return null;
            out += val;
        }
        return out;
    }

    fn reset(self: *Biterator) !void {
        self.bits.mask = try self.reader.readByte();
        self.idx = 0;
    }

    fn check(self: *Biterator) !void {
        if (self.bits.mask != 0xFF) return;
        try self.reset();
        switch (@as(MarkerCode, @enumFromInt(self.bits.mask))) {
            .stuffing => {
                self.bits.mask = 0xFF;
            },
            .EOI => {
                self.eoi = true;
            },
            else => |v| @panic(@tagName(v)),
        }
    }
};

const zigzag = [64]u6{
    0,  1,  5,  6,  14, 15, 27, 28,
    2,  4,  7,  13, 16, 26, 29, 42,
    3,  8,  12, 17, 25, 30, 41, 43,
    9,  11, 18, 24, 31, 40, 44, 53,
    10, 19, 23, 32, 39, 45, 52, 54,
    20, 22, 33, 38, 46, 51, 55, 60,
    21, 34, 37, 47, 50, 56, 59, 61,
    35, 36, 48, 49, 57, 58, 62, 63,
};
const zagzig = [64]u6{
    0,  1,  8,  16, 9,  2,  3,  10,
    17, 24, 32, 25, 18, 11, 4,  5,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13, 6,  7,  14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63,
};
