const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    if (bytes.len < 4) return 0;
    if (!std.mem.eql(u8, "\xff\xd8\xff", bytes[0..3])) return 0;

    switch (bytes[3]) {
        0xee, 0xdb => return zenit.probe.score_high,
        0xe0 => {
            if (std.mem.startsWith(u8, "\x00\x10JFIF\x00\x01", bytes[4..])) return zenit.probe.score_high;
            return zenit.probe.score_low;
        },
        0xe1 => {
            if (bytes.len > 6 and std.mem.startsWith(u8, "Exif\x00\x00", bytes[6..])) return zenit.probe.score_high;
            return zenit.probe.score_low;
        },
        else => return zenit.probe.score_low,
    }
}
