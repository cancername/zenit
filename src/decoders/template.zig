const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = @compileError("name here"),
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    _ = bytes; // autofix

}

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    _ = data; // autofix
    _ = z; // autofix
    _ = in; // autofix
    return .{};
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    _ = data; // autofix
    _ = z; // autofix
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    _ = z; // autofix
    _ = data; // autofix
    _ = in; // autofix
}
