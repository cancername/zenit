//! A bitstream decoder for Portable Arbitrary Map, a lossless uncompressed intermediate format for
//! arbitrary 2D data. Format documentation can be found at <https://netpbm.sourceforge.net/doc/pam.html>.
const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "pam",
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    return if (std.mem.startsWith(u8, bytes, "P7\n")) zenit.probe.high else if (std.mem.startsWith(u8, bytes, "P7")) zenit.probe.low else 0;
}

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    _ = data;
    _ = z;
    _ = in;
    return .{};
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    _ = data;
    _ = z;
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    _ = data;
    var tmp_buf: [512]u8 = undefined;

    if ((try in.peek(1)).len == 0) return .{ .end = {} };

    // Verify the magic matches.
    {
        const buf = tmp_buf[0 .. in.readUntilDelimiter(&tmp_buf, '\n') catch 0];
        if (!std.mem.eql(u8, buf, "P7\n")) {
            const displayed_buf = buf[0..@min(4, buf.len)];
            try z.diag(
                .err,
                .decoder,
                error.InvalidMagic,
                "The PAM magic did not match (expected P7, got \"{s}\" (possibly truncated)).",
                .{std.fmt.fmtSliceEscapeUpper(displayed_buf)},
            );
        }
    }

    // Parse the header.

    var width: ?usize = null;
    var height: ?usize = null;
    var depth: ?u16 = null;
    var tupltype: std.BoundedArray(u8, tmp_buf.len) = .{};
    var maxval: ?u16 = null;
    var comment_buf: std.ArrayListUnmanaged(u8) = .{};
    defer comment_buf.deinit(z.alloc.general);

    while (true) {
        const line_len = in.readUntilDelimiter(&tmp_buf, '\n') catch |e| switch (e) {
            error.NoDelimiterFound => {
                try z.diag(
                    .err,
                    .decoder,
                    error.NoDelimiterFound,
                    "There was no '\\n' in the first {d} bytes of the read buffer. The file is likely corrupted or has a very long line in it.",
                    .{tmp_buf.len},
                );
            },
            else => |err| return err,
        };
        const raw_line = tmp_buf[0..line_len];
        if (raw_line.len == 0) try z.diag(
            .err,
            .decoder,
            error.EndOfStream,
            "Reached EOF while parsing header.",
            .{},
        );

        if (raw_line.len == 1) continue;

        if (raw_line[0] == '#') {
            const comment = raw_line[1..];
            comment_buf.appendSlice(z.alloc.general, comment) catch {
                const displayed_comment = comment[0..@min(64, comment.len)];
                z.diag(
                    .err,
                    .decoder,
                    {},
                    "Comment \"{s}\" (possibly truncated) lost due to OOM.",
                    .{std.fmt.fmtSliceEscapeLower(displayed_comment)},
                );
            };
            continue;
        }

        const line = std.mem.trim(u8, raw_line, &.{ '\r', '\t', std.ascii.control_code.vt, std.ascii.control_code.ff });
        if (line.len == 0) continue;

        const pos = std.mem.indexOfAny(u8, line, " ") orelse line.len - 1;
        const tok1 = line[0..pos];
        const tok2 = std.mem.trimRight(u8, line[pos + 1 .. line.len], "\n");

        if (std.mem.eql(u8, tok1, "WIDTH")) {
            if (width != null) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"width"});
            width = try zenit.util.parseUnsignedDiag(usize, tok2, 10, z);
        } else if (std.mem.eql(u8, tok1, "HEIGHT")) {
            if (height != null) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"height"});
            height = try zenit.util.parseUnsignedDiag(usize, tok2, 10, z);
        } else if (std.mem.eql(u8, tok1, "DEPTH")) {
            if (depth != null) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"depth"});
            depth = try zenit.util.parseUnsignedDiag(u16, tok2, 10, z);
        } else if (std.mem.eql(u8, tok1, "MAXVAL")) {
            if (maxval != null) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"maxval"});
            maxval = try zenit.util.parseUnsignedDiag(u16, tok2, 10, z);
        } else if (std.mem.eql(u8, tok1, "TUPLTYPE")) {
            if (tupltype.len != 0) z.diag(.warn, .decoder, {}, "The {s} field was duplicated.", .{"tupltype"});
            tupltype.len = 0;
            tupltype.appendSlice(tok2) catch unreachable;
        } else if (std.mem.eql(u8, tok1, "ENDHDR")) {
            break;
        } else {
            const displayed_tok1 = tok1[0..@min(64, tok1.len)];
            try z.diag(
                .err,
                .decoder,
                error.InvalidLine,
                "The token \"{s}\" (possibly truncated) was not recognized.",
                .{std.fmt.fmtSliceEscapeLower(displayed_tok1)},
            );
        }
    }

    // Validate the values from the header.

    if (width == null or height == null or depth == null or maxval == null or width.? == 0 or height.? == 0 or depth.? == 0 or maxval.? == 0) {
        try z.diag(
            .err,
            .decoder,
            error.InvalidHeader,
            "One of width, height, depth, or maxval was missing or invalid.",
            .{},
        );
    }

    // Figure out which, if any, sample format to use.

    const tt = tupltype.constSlice();
    const maybe_format_nomaxval: ?enum { monob_bytes, gray, ya, rgb, rgba } = switch (depth.?) {
        1 => if (std.mem.eql(u8, tt, "BLACKANDWHITE")) .monob_bytes else if (std.mem.eql(u8, tt, "GRAYSCALE")) .gray else null,
        2 => if (std.mem.eql(u8, tt, "GRAYSCALE_ALPHA")) .ya else null,
        3 => if (std.mem.eql(u8, tt, "RGB")) .rgb else null,
        4 => if (std.mem.eql(u8, tt, "RGB_ALPHA")) .rgba else null,
        else => |d| try z.diag(
            .err,
            .decoder,
            error.UnsupportedDepth,
            "The depth {d} is not (yet) supported.",
            .{d},
        ),
    };

    const format_nomaxval = maybe_format_nomaxval orelse try z.diag(
        .err,
        .decoder,
        error.UnsupportedTupleType,
        "The tuple type \"{s}\" is not (yet) supported.",
        .{std.fmt.fmtSliceEscapeLower(tt)},
    );

    const bit_depth: u8 = switch (maxval.?) {
        0 => unreachable,
        1...(1 << 8) - 1 => 8,
        (1 << 8)...(1 << 10) - 1 => 10,
        (1 << 10)...(1 << 12) - 1 => 12,
        (1 << 12)...(1 << 16) - 1 => 16,
    };

    // PPM specifies BT.709 RGB, and PAM refers back to PPM:
    //
    // > A color image, such as would alternatively be represented by a PPM image, has a tuple type
    // > of "RGB".
    //
    // PAM specifies the _ALPHA tuple types as unassociated:
    //
    // > A sample in the opacity plane tells how opaque the pixel is, by telling what fraction of
    // > the pixel’s light comes from the foreground color. The rest of the pixel’s light comes from
    // > the (unspecified) background color.
    //
    // As opposed to PBM, 1 means "light on" in PAM:
    //
    // > Note that in the PBM format, a sample value of zero means white, but in PAM, zero means
    // > black.

    const format: zenit.image.Format = switch (format_nomaxval) {
        .rgb => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_rgb_8,
            10 => zenit.image.Format.bt_709_rgb_10,
            12 => zenit.image.Format.bt_709_rgb_12,
            16 => zenit.image.Format.bt_709_rgb_16,
            else => unreachable,
        },
        .rgba => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_rgba_ua_8,
            10 => zenit.image.Format.bt_709_rgba_ua_10,
            12 => zenit.image.Format.bt_709_rgba_ua_12,
            16 => zenit.image.Format.bt_709_rgba_ua_16,
            else => unreachable,
        },
        .monob_bytes => switch (bit_depth) {
            8 => zenit.image.Format.mono_light_on_bytes,
            10, 12, 16 => try z.diag(
                .err,
                .decoder,
                error.UnsupportedMaxval,
                "Unsupported maxval {d} for tuple type \"{s}\".",
                .{ maxval.?, std.fmt.fmtSliceEscapeLower(tt) },
            ),
            else => unreachable,
        },
        .gray => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_gray_8,
            10 => zenit.image.Format.bt_709_gray_10,
            12 => zenit.image.Format.bt_709_gray_12,
            16 => zenit.image.Format.bt_709_gray_16,
            else => unreachable,
        },
        .ya => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_graya_ua_8,
            10 => zenit.image.Format.bt_709_graya_ua_10,
            12 => zenit.image.Format.bt_709_graya_ua_12,
            16 => zenit.image.Format.bt_709_graya_ua_16,
            else => unreachable,
        },
    };

    // Finally, allocate an appropriate frame and read the data in.

    var frame = try zenit.Frame.allocDiag(z, .{
        .pts = undefined,
        .dts = undefined,
        .flags = .{ .is_key_frame = true },
        .layout = .{.image = format.layout },
        .interpretation = .{.image = format.interpretation },
        .medium_data = .{ .image = .{ .rows = height.?, .columns = width.?, .vfr_frame_duration = undefined } },
        .media_type = .image,
    });
    errdefer frame.free(&z.alloc);

    const nb_read = try in.readAll(frame.data);
    if (nb_read != frame.data.len) {
        try z.diag(
            .err,
            .decoder,
            error.EndOfStream,
            "The end of file was reached too early (needed {d} bytes, got {d}).",
            .{ frame.data.len, nb_read },
        );
    }

    // Store comments.
    if (comment_buf.items.len != 0)
        try frame.meta.extra_data.putDupeKey("comment", .{ .string = try comment_buf.toOwnedSlice(z.alloc.general) }, z);

    // Convert to host endianness if required.

    if (comptime @import("builtin").cpu.arch.endian() != .big) {
        switch (bit_depth) {
            8 => {},
            10, 12, 16 => {
                // TODO: replace with a simple ptrCast when it is implemented.
                const data_int_p: [*]align(zenit.Frame.alignment) u16 = @ptrCast(frame.data.ptr);
                const data_len = @divExact(frame.data.len, @sizeOf(u16));
                const data_ints = data_int_p[0..data_len];
                for (data_ints) |*p| p.* = @byteSwap(p.*);
            },
            else => unreachable,
        }
    }

    // Scale samples from non-power-of-2 maxvals up to native.

    switch (maxval.?) {
        std.math.maxInt(u8), std.math.maxInt(u10), std.math.maxInt(u12), std.math.maxInt(u16) => {},
        else => |m| if (m != 1 and !format.eql(zenit.image.Format.mono_light_on_bytes)) {
            z.diag(
                .warn,
                .decoder,
                {},
                "The maxval {d} does not have a native corresponding pixel format and automatic conversion is unimplemented. The output frame is corrupt.",
                .{m},
            );
            frame.meta.flags.is_corrupt = true;
        },
    }

    return .{ .frame = frame };
}
