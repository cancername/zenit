//! A bitstream decoder for the Netpbm formats, a family of lossless uncompressed intermediate
//! formats:
//! - Portable Bitmap <https://netpbm.sourceforge.net/doc/pbm.html>.
//! - Portable Graymap <https://netpbm.sourceforge.net/doc/pgm.html>.
//! - Portable Pixmap <https://netpbm.sourceforge.net/doc/ppm.html>.
const std = @import("std");
const zenit = @import("../zenit.zig");

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "pnm",
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .deinitFn = &deinitImpl,
            .decodeFn = &decodeImpl,
        },
    };
}

const PnmFormat = enum(u8) {
    plain_pbm = '1',
    plain_pgm = '2',
    plain_ppm = '3',
    pbm = '4',
    pgm = '5',
    ppm = '6',

    fn fromVersion(byte: u8) ?PnmFormat {
        return std.meta.intToEnum(PnmFormat, byte) catch null;
    }

    fn isPlain(pf: PnmFormat) bool {
        return @intFromEnum(pf) <= '3';
    }

    fn hasMaxval(pf: PnmFormat) bool {
        return switch (pf) {
            .plain_pbm, .pbm => false,
            .plain_pgm, .plain_ppm, .pgm, .ppm => true,
        };
    }
};

pub fn probeImpl(bytes: []const u8) zenit.probe.Score {
    return if (bytes.len >= 3 and bytes[0] == 'P' and bytes[2] == '\n' and bytes[1] >= '1' and bytes[1] <= '6') zenit.probe.high else 0;
}

pub fn initImpl(z: *zenit.Context, data: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    _ = z;
    _ = data;
    _ = in;
    return .{};
}

pub fn deinitImpl(z: *zenit.Context, data: ?*anyopaque) anyerror!void {
    _ = z;
    _ = data;
}

pub fn decodeImpl(z: *zenit.Context, data: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
    _ = data;

    if ((try in.peek(1)).len == 0) return .{ .end = {} };

    // Get the specific format from the magic.

    const format: PnmFormat = get_format: {
        const magic = try in.readBytesNoEof(3);
        const m_format = PnmFormat.fromVersion(magic[1]);
        if (magic[0] != 'P' or magic[2] != '\n' or m_format == null) try z.diag(
            .err,
            .decoder,
            error.InvalidMagic,
            "The magic bytes \"{s}\" are not \"P[1-6]\n\".",
            .{std.fmt.fmtSliceEscapeLower(&magic)},
        );
        break :get_format m_format.?;
    };

    var tmp_buf: [512]u8 = undefined;

    var comment_buf: std.ArrayListUnmanaged(u8) = .{};
    defer comment_buf.deinit(z.alloc.general);

    // Parse the header.

    var width: usize = undefined;
    var height: usize = undefined;
    var maxval: u16 = 1;

    var state: enum {
        width,
        height,
        maxval,
    } = .width;

    outer: while (true) {
        const line_len = in.readUntilDelimiter(&tmp_buf, '\n') catch |e| switch (e) {
            error.NoDelimiterFound => {
                try z.diag(
                    .err,
                    .decoder,
                    error.NoDelimiterFound,
                    "There was no '\\n' in the first {d} bytes of the read buffer. The file is likely corrupted or has a very long line in it.",
                    .{tmp_buf.len},
                );
            },
            else => |err| return err,
        };
        const line = tmp_buf[0..line_len];

        if (line.len == 0) try z.diag(
            .err,
            .decoder,
            error.EndOfStream,
            "Reached EOF while parsing header.",
            .{},
        );

        if (line[0] == '#') {
            const comment = line[1..];
            comment_buf.appendSlice(z.alloc.general, comment) catch {
                const displayed_comment = comment[0..@min(64, comment.len)];
                z.diag(
                    .err,
                    .decoder,
                    {},
                    "Comment \"{s}\" (possibly truncated) lost due to OOM.",
                    .{std.fmt.fmtSliceEscapeLower(displayed_comment)},
                );
            };
            continue;
        }

        var si = std.mem.tokenizeAny(u8, line, &std.ascii.whitespace);

        while (si.next()) |token| {
            switch (state) {
                .width => {
                    width = try zenit.util.parseUnsignedDiag(usize, token, 10, z);
                    state = .height;
                },
                .height => {
                    height = try zenit.util.parseUnsignedDiag(usize, token, 10, z);
                    state = if (format.hasMaxval()) .maxval else break :outer;
                },
                .maxval => {
                    maxval = try zenit.util.parseUnsignedDiag(u16, token, 10, z);
                    break :outer;
                },
            }
        }
    }

    // Determine correct sample format.

    const bit_depth: u8 = switch (maxval) {
        0 => unreachable,
        1 => 1,
        2...(1 << 8) - 1 => 8,
        (1 << 8)...(1 << 10) - 1 => 10,
        (1 << 10)...(1 << 12) - 1 => 12,
        (1 << 12)...(1 << 16) - 1 => 16,
    };

    const img_format: zenit.image.Format = switch (format) {
        .pbm => zenit.image.Format.mono_ink_on,
        .plain_pbm => zenit.image.Format.mono_ink_on_bytes,
        .pgm, .plain_pgm => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_gray_8,
            10 => zenit.image.Format.bt_709_gray_10,
            12 => zenit.image.Format.bt_709_gray_12,
            16 => zenit.image.Format.bt_709_gray_16,
            else => unreachable,
        },
        .ppm, .plain_ppm => switch (bit_depth) {
            8 => zenit.image.Format.bt_709_rgb_8,
            10 => zenit.image.Format.bt_709_rgb_10,
            12 => zenit.image.Format.bt_709_rgb_12,
            16 => zenit.image.Format.bt_709_rgb_16,
            else => unreachable,
        },
    };

    // Make frame.

    var frame = try zenit.Frame.allocDiag(z, .{
        .pts = undefined,
        .dts = undefined,
        .flags = .{ .is_key_frame = true, .is_dts_accurate = false, .is_pts_accurate = false },
        .medium_data = .{ .image = .{ .rows = height, .columns = width, .vfr_frame_duration = .{ .nsecs = 0 } } },
        .media_type = .image,
        .layout = .{ .image = img_format.layout },
        .interpretation = .{ .image = img_format.interpretation },
    });

    // Store comment.

    if (comment_buf.items.len != 0)
        try frame.meta.extra_data.putDupeKey("comment", .{ .string = try comment_buf.toOwnedSlice(z.alloc.general) }, z);

    // Read frame.

    if (format.isPlain()) {
        // Parse plaintext integers.
        const frame_len = frame.data.len;
        const nb_samples = if (bit_depth > 8) @divExact(frame_len, @sizeOf(u16)) else frame_len;
        var i: usize = 0;
        while (i < nb_samples) : (i += 1) {
            const sample_len = try in.readUntilDelimiters(tmp_buf[0..8], &std.ascii.whitespace);
            const sample_buf = tmp_buf[0..sample_len];
            const u = try zenit.util.parseUnsignedDiag(u16, sample_buf, 10, z);
            if (u > maxval) {
                z.diag(.warn, .decoder, {}, "Sample {d} ({d}) exceeeded maxval {d}.", .{ i, u, maxval });
                frame.meta.flags.is_corrupt = true;
            }

            if (bit_depth <= 8) {
                frame.data[i] = @truncate(u);
            } else {
                const u16_data: [*]align(zenit.Frame.alignment) u16 = @ptrCast(frame.data);
                u16_data[i] = u;
            }
        }
    } else {
        in.readNoEof(frame.data) catch |err| switch (err) {
            error.EndOfStream => |e| try z.diag(.err, .decoder, e, "Premature EOF.", .{}),
            else => |e| return e,
        };

        // Convert to host endianness if required.

        if (comptime @import("builtin").cpu.arch.endian() != .big) {
            switch (bit_depth) {
                8, 1 => {},
                10, 12, 16 => {
                    // TODO: replace with a simple ptrCast when it is implemented.
                    const data_int_p: [*]align(zenit.Frame.alignment) u16 = @ptrCast(frame.data.ptr);
                    const data_len = @divExact(frame.data.len, @sizeOf(u16));
                    const data_ints = data_int_p[0..data_len];
                    for (data_ints) |*p| p.* = @byteSwap(p.*);
                },
                else => unreachable,
            }
        }
    }

    return .{ .frame = frame };
}
