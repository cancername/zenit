const std = @import("std");
const zenit = @import("../zenit.zig");
const shared = @import("../shared/png.zig");
const Ihdr = shared.Ihdr;
const Sbit = shared.Sbit;
const Srgb = shared.Srgb;
const Chunk = shared.ReadChunk;

pub fn decoder() zenit.BitstreamDecoder {
    return .{
        .name = "png",
        .v_table = &.{
            .probeFn = &probeImpl,
            .initFn = &initImpl,
            .decodeFn = &decodeImpl,
            .deinitFn = &deinitImpl,
        },
    };
}

const State = enum {
    ihdr,
    meta,
    idat,
    done,
};

const Context = struct {
    state: State = .ihdr,
    ihdr: Ihdr = undefined,
    plte: std.BoundedArray([3]u8, 256) = .{},
    sbit: Sbit = undefined,
    color_space: struct {} = .{},
    first_idat: Chunk,
    tmp_ally: std.heap.ArenaAllocator,

    inline fn ally(ctx: *Context) std.mem.Allocator {
        return @call(.always_inline, std.heap.ArenaAllocator.allocator, .{&ctx.tmp_ally});
    }

    fn metadata(ctx: Context) zenit.FrameMetadata {
       _ = ctx;
        return .{
            .medium_data = .{},
        };
    }
};

fn probeImpl(bytes: []const u8) zenit.probe.Score {
    return if (std.mem.startsWith(u8, bytes, "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A")) zenit.probe.high else 0;
}

fn deinitImpl(z: *zenit.Context, context: ?*anyopaque) anyerror!void {
    const ctx: *Context = @ptrCast(@alignCast(context));

    // copy allocator out of the context so it doesn't try to free itself
    const tmp_ally = ctx.tmp_ally;

    ctx.* = undefined;

    z.alloc.releaseTemp(tmp_ally);
}

fn initImpl(z: *zenit.Context, context: *?*anyopaque, in: *zenit.InputStream) anyerror!zenit.FrameStreamMetadata {
    var tmp_ally = z.alloc.acquireTemp();
    errdefer z.alloc.releaseTemp(tmp_ally);

    const ctx = try tmp_ally.allocator().create(Context);
    ctx.* = .{
        .tmp_ally = tmp_ally,
        .first_idat = undefined,
    };

    if (!try in.readExpected("\x89\x50\x4E\x47\x0D\x0A\x1A\x0A")) return error.BadMagic;

    {
        const ihdr_chunk = try Chunk.read(z, ctx.ally(), in, @sizeOf(Ihdr));
        defer ihdr_chunk.dealloc(ctx.ally());

        try ihdr_chunk.validate(z);

        if (ihdr_chunk.chunkType() != .IHDR) try z.diag(.err, .decoder, error.NoIhdr, "First chunk was {s} instead of IHDR.", .{@tagName(ihdr_chunk.chunkType())});
        ctx.ihdr = try Ihdr.fromReadChunk(ihdr_chunk);
        try ctx.ihdr.validate(z);
    }

//    while (true) {
//        const len, const chunk_type: shared.ChunkType = Chunk.peek(in) orelse return error.PrematureEof;

//    }

    // TODO: flesh this out
    while (Chunk.read(z, ctx.ally(), in, std.math.maxInt(u31))) |chunk| {
        chunk.validate(z) catch {};

        switch (chunk.chunkType()) {
            .IHDR => return error.DuplicateIhdr,

            .IDAT => {
                ctx.first_idat = chunk;
                break;
            },

            .PLTE => {
                if (ctx.plte.len != 0) return error.DuplicatePlte;
                const data_bytes = chunk.data();
                const data_p: [*]const [3]u8 = @ptrCast(data_bytes.ptr);
                const data_len = try std.math.divExact(usize, data_bytes.len, 3);
                const data = data_p[0..data_len];
                ctx.plte.appendSlice(data) catch try z.diag(
                    .err,
                    .decoder,
                    error.PlteTooLarge,
                    "The PLTE chunk was too large ({d} bytes, {d} colors, max 256).",
                    .{ data_bytes.len, data_len },
                );
                chunk.dealloc(ctx.ally());
            },

            .sBIT => {
                const data = chunk.data();
                const sbit_slice = ctx.sbit.slice(ctx.ihdr.color_type);

                if (data.len != sbit_slice.len) {
                    z.diag(.warn, .decoder, {}, "sBIT length was {d}, should be {d}", .{ data.len, sbit_slice.len });
                    continue;
                }

                @memcpy(sbit_slice, data);

                chunk.dealloc(ctx.ally());
            },

            .IEND => return error.PrematureIend,

            else => {
                const d = chunk.data();
                const trunc_data = chunk.data()[0..@min(512, std.mem.indexOfScalar(u8, d, '\n') orelse d.len)];
                std.debug.print("unhandled PNG chunk in header: {} data \"{s}\" (len {d}) crc {x} ?= {x}\n", .{ chunk.chunkType(), std.fmt.fmtSliceEscapeLower(trunc_data), d.len, chunk.crc(), chunk.computeCrc() });
                chunk.dealloc(ctx.ally());
            },
        }
    } else |e| return e;

    context.* = @ptrCast(ctx);

    return .{};
}

fn decodeImpl(z: *zenit.Context, context: ?*anyopaque, in: *zenit.InputStream) anyerror!zenit.DecodeUpdate {
      _ = z;
      _ = context;
      _ = in;
//    const ctx: *Context = @ptrCast(@alignCast(context));

//    while (Chunk.peek(in)) |info| {
//        const len, const chunk_type = info;
//        try shared.validatePngInt(z, len, "chunk", "length");



//    }

    return .{.end = {}};
}
