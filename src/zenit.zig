pub usingnamespace @import("context.zig");
pub usingnamespace @import("types.zig");
pub usingnamespace @import("stream.zig");
pub usingnamespace @import("info.zig");
pub usingnamespace @import("decoder.zig");
pub usingnamespace @import("demuxer.zig");
pub usingnamespace @import("helper.zig");
pub usingnamespace @import("transfer_fn.zig").TransferFns(@This().FastFloat);
pub usingnamespace @import("convert.zig");

pub const TransferFn = @import("transfer_fn.zig").TransferFn;
pub const probe = @import("probe.zig");
pub const util = @import("util.zig");


// Bitstream decoders
pub const decoders = struct {
    pub const pam = @import("decoders/pam.zig");
    pub const pnm = @import("decoders/pnm.zig");
    pub const y4m = @import("decoders/y4m.zig");
    pub const qoi = @import("decoders/qoi.zig");
    pub const png = @import("decoders/png.zig");
    // pub const jpg = @import("decoders/nektro_jpg.zig");
};

test {
    @import("std").testing.refAllDeclsRecursive(@This());
}
