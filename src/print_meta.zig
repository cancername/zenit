const std = @import("std");
const zenit = @import("zenit.zig");
const pam = @import("decoders/pam.zig");
const y4m = @import("decoders/y4m.zig");

pub fn main() !void {
    var ally_s = @import("binned_allocator.zig").BinnedAllocator(.{}){};
    defer ally_s.deinit();
    const ally = ally_s.allocator();

    var ctx = zenit.Context.initSingleAlly(ally);
    defer ctx.deinit();
    defer for (ctx.diagnostics.items) |diagnostic| std.debug.print("{}\n", .{diagnostic});

    try ctx.registerBitstreamDecoder(pam.decoder());
    try ctx.registerBitstreamDecoder(y4m.decoder());

    var stream = zenit.InputStream.initOwnedFd((try std.fs.cwd().openFile(std.mem.span(std.os.argv[1]), .{})).handle);
    defer stream.deinit();

    const bd_i = b: {
        const sel = ctx.probe(&stream) orelse return error.NoFormatFound;
        if (sel != .bitstream_decoder) return error.WrongSelection;
        break :b sel.bitstream_decoder;
    };

    const bd = ctx.bitstream_decoders.items[bd_i];

    const decoder_data, const stream_metadata = try bd.init(&ctx, &stream);
    _ = stream_metadata;

    while (bd.decode(&ctx, decoder_data, &stream)) |update| switch (update) {
        .end => break,
        .none => {},
        .frame => |cf| {
            var f = cf;
            std.debug.print("{}\n", .{f.meta});
            f.free(&ctx);
        },
    } else |e| return e;
}
