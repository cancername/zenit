const std = @import("std");
const types = @import("types.zig");

// TODO: Surround is cursed and I have no clue how I will deal with it Yet™

pub const Interpretation = struct {
    pub const Channels = enum {
        lr,
        wxyz,
    };
    channels: Channels,
};

pub const Format = struct {
    layout: Layout,
    interpretation: Interpretation,
};

pub const Layout = struct {
    pub const Kind = enum {
        u_8,
        s_16,
        s_32,
        f_32,
    };
    is_planar: bool,
    kind: Kind,
    nb_channels: u8,
};
